package qa.qcri.prepocressor.ui;

import org.opencv.core.Core;

import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.operations.Operation;
import qa.qcri.prepocressor.operations.OperationFactory;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class is the main class. It contains the main() method which starts the
 * main program workflow (load resources, cluster, output results).
 * 
 * @see #main(String[])
 * @author Felix Stahlberg
 */
public class Main {
	
	/**
	 * This method is the main runner method.
	 * @param args
	 */
	public static void main(String[] args) {
		Logger log = Logger.getSingleton();
		// Load configuration
		Configuration conf = Configuration.getGlobalConfiguration();
		if (args.length >= 1 && args[0].equals("-help")) {
			if (args.length == 1) {
				conf.printHelp();
			} else {
				for (int i = 1; i < args.length; i++) {
					System.out.println("OPERATION " + args[i] + ":");
					Operation op = OperationFactory.createOperation(args[i]);
					if (op != null) {
						op.getConfiguration().printHelp();
					}
				}
			}
			return;
		}
		conf.processCommandLineArguments(args);
		if (log.isFatalErrorRegistered()) {
			log.fatalError("Fatal errors occurred. Use -help to show "
					+ "information about each available parameter");
			return;
		}
		conf.dumpToFilesystem();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME); // Load OpenCV native library
		// TODO: Find a better place to load the OpenCV library
		log.notice("Configuration loaded...");
		Pipeline pipeline = new Pipeline();
		pipeline.run();
	}
}
