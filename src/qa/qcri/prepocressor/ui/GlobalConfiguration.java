package qa.qcri.prepocressor.ui;

import java.text.SimpleDateFormat;
import java.util.Date;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This is the configuration for prepocressor.
 *
 * @see Configuration#getGlobalConfiguration()
 * @author Felix Stahlberg
 */
public class GlobalConfiguration extends Configuration {
	@Override
	protected void addDefaultParameters() {
		SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd.HHmmss.");
		addParameter("outputPath",
			formatter.format(new Date()) + System.getProperty("user.name")
			+ "-%base%idx%ext",
			"This parameter controls where the output files are stored. The "
			+ "file format is determined by the file extension defined by this"
			+ " template. For available file formats, see the -inputFile "
			+ "parameter. Following placeholders can be used:\n+\n"
			+ "* '%dir': Name of the directory containing the input image.\n"
			+ "* '%base': Base name of the input image file name (without "
				+ "file extension).\n"
			+ "* '%-base': Same as %base, but cut after first minus hyphn ('-')\n"
			+ "* '%idx': Some operators split up input images into smaller pieces"
				+ ". The pieces are stored subsituting %idx with '_1', '_2'...\n"
			+ "* '%unqatip': Assume base name of the input image file name (without "
				+ "file extension) is a QATIP style ID. Then the %unqatip placeholder"
				+ " stands for the original corpus id. Breaks if original speaker or"
				+ " utterance id starts with 'x'\n"
			+ "* '%ext': File extension (given by the input file).\n-");
		addParameter("inputFile", "imageList.txt",
				"Text file containing paths to the input images. The paths should "
				+ "be separated by line breaks. This parameter can also point "
				+ "directly to an image file if only one image is to be "
				+ "processed. Following input formats are supported (provided "
				+ "by OpenCVs imread function):\n+\n"
				+ "* Windows bitmaps - *.bmp, *.dib\n"
				+ "* JPEG files - *.jpeg, *.jpg, *.jpe\n"
				+ "* JPEG 2000 files - *.jp2\n"
				+ "* Portable Network Graphics - *.png\n"
        		+ "* Portable image format - *.pbm, *.pgm, *.ppm\n"
        		+ "* Sun rasters - *.sr, *.ras\n"
        		+ "* TIFF files - *.tiff, *.tif\n"
        		+ "* Additionally, the CSV file format is supported by prepocressor.\n-"
		);
		addParameter("logLevel", "INFO",
			"Controls the amount of output. \n+\n"
			+ "* FATAL: Only fatal errors, \n"
			+ "* ERROR: All errors, \n"
			+ "* WARN: Warnings and errors, \n"
			+ "* INFO: Notices, warnings and errors, \n"
			+ "* DEBUG: Debug mode\n-");
		addParameter("silentOverwrite", 1,
			"No files are overridden if this is set to 0.");
		addParameter("singlePopulation", 0,
			"If this parameter is set to 1, only a single population is used "
			+ "for all input images. Otherwise, a population is created for "
			+ "each of the input files separately. If this option is set "
			+ "the -nThreads parameter is not used. Note that the file "
			+ "name of the population is set to the first input file");
		addParameter("idLength", 3,
			"Length of the numerical IDs that are inserted when one image in "
			+ "the pipeline produces multiple children. Fill up with trailing "
			+ "0s if the number is shorter. Set to 0 to switch off trailing "
			+ "0s.");
		addParameter("pipeline", "",
			"This parameter defines the operations to be executed on the input"
			+ " images. The syntax is similar to the linux shell pipeline: "
			+ "Operations are separated by '|' und parameterized with the common"
			+ "-<arg> <val> syntax. For details regarding the operations, try "
			+ "-help <operation-name>. Available operations are:\n+\n"
			+ "* adaptiveThreshold\n* ascendersTextLine\n* axisAlignedHough\n* bbq\n* blur\n* col2graph\n* "
			+ "componentDensity\n* concat\n* convertToFloat\n* cutWithAltecXml\n* cutWithHadaraXml\n* cutWithPageXml\n* "
			+ "devNull\n* drawChildren\n* drawKaldiAlignment\n* drawTextLines\n* "
			+ "exactOrientationCorrection\n* extractConstantRegions\n* extend\n* extendForHoughsquare\n* "
			+ "featExtract\n* fillTransparency\n* filter\n* flip\n* grayscale\n* hough\n* "
			+ "houghTextLine\n* invert\n* log\n* morph\n* multiChannelOtsu\n* "
			+ "normalize\n* normalizeText\n* normalizeUpperBaseline\n* "
			+ "orientationCorrection\n* outlierRemove\n* polynomialTextLine\n* potatoPeelingTextLine\n* "
			+ "printMax\n* projectionLineSegmentation\n* rectSum\n* reduce\n* "
			+ "reducedAlcmTransform\n* renderPageXmlTranscriptions\n* removeDiacritics\n* removeLargeComponents\n* "
			+ "removeSmallComponents\n* removeUnderline\n* removeVertTextMargin\n* scale\n* sobel\n* "
			+ "splitTextLines\n* subtractMean\n* tee\n* textSkewCorrection\n* "
			+ "thinning\n* threshold\n* transpose\n* vertTextSegmentation\n* visualizePageXml\n* writeRects\n-");
		addParameter("nThreads", 1,
			"Number of threads.");
	}

	@Override
	protected String getDescription() {
		return "PREPOCRESSOR " + Configuration.VERSION + " is a tool for preprocessing "
			+ "images and feature extraction for OCR developed at the Qatar "
			+ "Computing Research Institute.";
	}
}
