package qa.qcri.prepocressor.ui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import qa.qcri.prepocressor.io.Logger;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This singleton class stores the configuration, which is normally
 * set by command line arguments or configuration file.
 * 
 * @see #processCommandLineArguments(String[])
 * @author Felix Stahlberg
 */
abstract public class Configuration {
	
	final static public String VERSION = "0.2.1";
	
	/*
	 * Singleton instance for the global configuration.
	 */
	static private Configuration globalConf;
	
	/*
	 * Parameter lists by their datatypes
	 */
	private Map<String, Integer> intParams;
	private Map<String, Float> floatParams;
	private Map<String, String> stringParams;
	private Map<String, String> descriptions;
	
	/*
	 * List of already included configuration files 
	 */
	private Set<String> alreadyIncluded;
	
	/*
	 * Add an integer parameter.
	 */
	protected void addParameter(String key, int defaultValue, String desc) {
		intParams.put(key, defaultValue);
		descriptions.put(key, desc);
	}
	
	/*
	 * Add a floating point parameter.
	 */
	protected void addParameter(String key, float defaultValue, String desc) {
		floatParams.put(key, defaultValue);
		descriptions.put(key, desc);
	}
	
	/*
	 * Add a string parameter.
	 */
	protected void addParameter(String key, String defaultValue, String desc) {
		stringParams.put(key, defaultValue);
		descriptions.put(key, desc);
	}
	
	/**
	 * Sole constructor. Creates an empty configuration instance.
	 * 
	 * @see #restore()
	 */
	public Configuration() {
		descriptions = new HashMap<String, String>();
		intParams = new HashMap<String, Integer>();
		floatParams = new HashMap<String, Float>();
		stringParams = new HashMap<String, String>();
		alreadyIncluded = new HashSet<String>();
	}
	
	/**
	 * Restores the default configuration.
	 */
	public void restore() {
		descriptions.clear();
		intParams.clear();
		floatParams.clear();
		stringParams.clear();
		addParameter("configFile", "", // Always add configFile
				"Configuration file (format: <key> <val>). Parameters "
				+ "in this file override values in the default configuration "
				+ "file. Command line arguments override all other settings. "
				+ "If this parameter is used within a config file, it has "
				+ "'include once' semantics.");
		addParameter("configDumpFileName", "", // Always add configFile
				"This can be used to write a file containing all parameters of"
				+ " the used configuration.");
		addDefaultParameters();
	}
	
	/**
	 * Add all parameters available for this configuration.
	 */
	abstract protected void addDefaultParameters();	
	/**
	 * Get configuration singleton. If it does not exist yet, create it and 
	 * load default configuration file.
	 * 
	 * @return configuration singleton
	 */
	static public Configuration getGlobalConfiguration() {
		if (globalConf == null) {
			globalConf = new GlobalConfiguration();
			globalConf.restore();
		}
		return globalConf;
	}
	
	/**
	 * Set the value of a parameter.
	 * 
	 * @param key
	 * @param value
	 */
	public void set(String key, int value) {
		if (!intParams.containsKey(key)) {
			Logger.getSingleton().fatalError(
					"Unknown integer parameter " + key);
		} else {
			intParams.put(key, value);
		}
	}
	
	/**
	 * Set the value of a parameter.
	 * 
	 * @param key
	 * @param value
	 */
	public void set(String key, float value) {
		if (!floatParams.containsKey(key)) {
			Logger.getSingleton().fatalError(
					"Unknown floating point parameter " + key);
		} else {
			floatParams.put(key, value);
		}
	}
	
	/**
	 * Get a integer parameter. If the key is unknown, return null
	 * 
	 * @param key the name of the parameter
	 * @return the value of the parameter or null
	 */
	public Integer getInteger(String key) {
		return intParams.get(key);
	}
	
	/**
	 * Get a floating point parameter. If the key is unknown, return null
	 * 
	 * @param key the name of the parameter
	 * @return the value of the parameter or null
	 */
	public Float getFloat(String key) {
		return floatParams.get(key);
	}
	
	/**
	 * Get a string parameter. If the key is unknown, return null
	 * 
	 * @param key the name of the parameter
	 * @return the value of the parameter or null
	 */
	public String getString(String key) {
		return stringParams.get(key);
	}
	
	/**
	 * Set the value of a parameter.
	 * 
	 * @param key
	 * @param value
	 */
	public void set(String key, String value) {
		try {
			if (intParams.containsKey(key)) {
				set(key, Integer.parseInt(value));
			} else  if (floatParams.containsKey(key)){
				set(key, new Float(value));
			} else if (stringParams.containsKey(key)) {
				stringParams.put(key, value);
				if (key.equals("logLevel")) {
					Logger.getSingleton().setLevel(value);
				}
			} else {
				Logger.getSingleton().fatalError(
						"Unknown parameter " + key);
			}
		} catch(NumberFormatException e) {
			Logger.getSingleton().fatalError(
					"Parameter " + key + " should be numeric");
		}
	}
	
	/**
	 * Parse the command line arguments and overrides parameters if they
	 * appear in them.
	 * 
	 * @param argv string array with command line arguments
	 */
	public void processCommandLineArguments(String[] args) {
		String curParam = null;
		for (String arg : args) {
			if (arg.isEmpty()) {
				; // skip this arg
			} else if (arg.charAt(0) == '-') {
				if (curParam != null) {
					set(curParam, 1);
				}
				curParam = arg.substring(1);
			} else if (curParam != null) {
				set(curParam, arg);
				if (curParam.equals("configFile") 
						&& loadConfigurationFile(arg)) {
					// Process command line arguments again
					processCommandLineArguments(args);
					return;
				}
				curParam = null;
			} else {
				Logger.getSingleton().fatalError(
						"Malformed command line argument " + arg);
			}
		}
		if (curParam != null) {
			set(curParam, 1);
		}
	}
	
	/**
	 * Load parameters from a configuration file. The file should be in GIZA++
	 * format (i.e. one parameter per line, key/value separated by a blank).
	 * Lines starting with '#' are ignored.
	 * 
	 * @param fileName path to the file to load
	 * @param boolean TRUE if the file could be loaded, FALSE otherwise
	 */
	public boolean loadConfigurationFile(String fileName) {
		if (alreadyIncluded.contains(fileName)) {
			return false;
		}
		Logger log = Logger.getSingleton();
		alreadyIncluded.add(fileName);
		try {
			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				if (strLine.charAt(0) == '#') { // Comment
					continue;
				}
				String[] split = strLine.split(" ", 2);
				if (split.length != 2) {
					log.error("Malformed line " + strLine + " in " + fileName);
				}
				set(split[0], split[1]);
			}
			in.close();
		} catch (Exception e) {// Catch exception if any
			log.error("Error while reading config file "
					+ fileName + ": " + e.getMessage());
			return false;
		}
		return true;
	}
	
	/**
	 * Prints a help text to stdout.
	 */
	public void printHelp() {
		System.out.println(getDescription());
		System.out.println(
				"Configuration via command line arguments: -<name> <value>");
		System.out.println(
				"Configuration via file (per line): <name> <value>");
		System.out.println("Following parameters are available:\n");
		SortedMap<String, String> params = new TreeMap<String, String>();
		for (Entry<String, String> e : stringParams.entrySet()) {
			String key = e.getKey();
			params.put(key, getParameterHelpText(key, e.getValue(),
					"String", descriptions.get(key)));
		}
		for (Entry<String, Integer> e : intParams.entrySet()) {
			String key = e.getKey();
			params.put(key, getParameterHelpText(key, "" + e.getValue(), 
					"Integer", descriptions.get(key)));
		}
		for (Entry<String, Float> e : floatParams.entrySet()) {
			String key = e.getKey();
			params.put(key, getParameterHelpText(key, "" + e.getValue(), 
					"Float", descriptions.get(key)));
		}
		for (Entry<String, String> e : params.entrySet()) {
			System.out.println(e.getValue());
		}
	}
	
	/**
	 * Get the description of this configuration.
	 * 
	 * @return short sentence describing the subject of this configuration.
	 */
	abstract protected String getDescription();
	
	/**
	 * Builds the content of a config file for the current configuration.
	 */
	public void dumpToFilesystem() {
		String fileName = getString("configDumpFileName");
		if (fileName.isEmpty()) {
			return;
		}
		try {
			FileWriter fs = new FileWriter(fileName);
			BufferedWriter out = new BufferedWriter(fs);
			SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd HH:mm:ss.");
			out.write("# PREPOCRESSOR-Version: " + VERSION + "\n");
			out.write("# Start: " + formatter.format(new Date()) + "\n");
			out.write("# Working Directory: " + System.getProperty("user.dir")
					+ "\n");
			out.write("\n# String Parameters\n");
			SortedSet<String> keys = new TreeSet<String>(stringParams.keySet());
			for (String key : keys) {
				out.write(key + " " + stringParams.get(key) + "\n");
			}
			out.write("\n# Integer Parameters\n");
			keys = new TreeSet<String>(intParams.keySet());
			for (String key : keys) {
				out.write(key + " " + intParams.get(key) + "\n");
			}
			out.write("\n# Floating Point Parameters\n");
			keys = new TreeSet<String>(floatParams.keySet());
			for (String key : keys) {
				out.write(key + " " + floatParams.get(key) + "\n");
			}
			out.close();
		} catch (IOException e) {
			Logger.getSingleton().error("Error while dumping corpus to "
					+ fileName + ": " + e.getMessage());
		}
	}
	
	/*
	 * Prints a single parameter to stdout.
	 */
	private String getParameterHelpText(String key, String value, String type, 
			String description) {
		if (value.equals("")) {
			value = "<not set>";
		}
		StringBuilder sb = new StringBuilder();
		sb.append(key + " (" + type + ", Default: " + value + ")\n");
		sb.append(description + "\n");
		return sb.toString();
	}
}
