package qa.qcri.prepocressor.ui;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.datastructures.Population;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.operations.Operation;
import qa.qcri.prepocressor.operations.OperationFactory;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class is responsible for processing the pipeline.
 * 
 * @author Felix Stahlberg
 */
public class Pipeline {
	protected static List<Operation> ops;
	private static List<String> inputFileNames;
	private static Iterator<String> inputFileIterator;
	
	private class PipelineWorker implements Runnable {
		@Override
		public void run() {
			String inputFile = getNextInputFile();
			String outputPath = Configuration.getGlobalConfiguration()
					.getString("outputPath");
			Logger log = Logger.getSingleton();
			while (inputFile != null) {
				log.debug("Process input file '" + inputFile + "'");
				Individual indy = new Individual();
				if (indy.loadContent(inputFile)) {
					Population pop = new Population(inputFile);
					pop.add(indy);
					for (Operation op : ops) {
						pop = op.processPopulation(pop);
					}
					pop.store(outputPath);
				}
				inputFile = getNextInputFile();
			}
		}
	}
	
	/* (non-Javadoc)
	 * Worker threads call this method to obtain a new input file name.
	 * If all files are processed, return null.
	 * 
	 * @throws NullPointerException if the iterator is not initialized
	 */
	synchronized private String getNextInputFile() {
		if (inputFileIterator.hasNext()) {
			return inputFileIterator.next();
		}
		return null;
	}

	/**
	 * Processes the pipeline.
	 */
	public void run() {
		Configuration conf = Configuration.getGlobalConfiguration();
		Logger log = Logger.getSingleton();
		inputFileNames = getInputFileNames();
		inputFileIterator = inputFileNames.iterator();
		ops = getOperations();
		if (!log.isFatalErrorRegistered()) {
			if (conf.getInteger("singlePopulation")	> 0) {
				runSinglePopulation();
			} else {
				runMultiplePopulations();
			}
			log.notice("Done.");
		}
	}
	
	/* (non-Javadoc)
	 * Process pipeline in single population mode
	 */
	private void runSinglePopulation() {
		String inputFile = getNextInputFile();
		String outputPath = Configuration.getGlobalConfiguration()
				.getString("outputPath");
		Population pop = new Population(inputFile);
		int idx = 0;
		while (inputFile != null) {
			Individual indy = new Individual();
			if (indy.loadContent(inputFile)) {
				indy.setIdxPrefix("_" + Operation.generateId(idx++));
				pop.add(indy);
			}
			inputFile = getNextInputFile();
		}
		for (Operation op : ops) {
			pop = op.processPopulation(pop);
		}
		pop.store(outputPath);
	}
	
	/* (non-Javadoc)
	 * Process pipeline in multiple population mode (normal mode)
	 */
	private void runMultiplePopulations() {
		Configuration conf = Configuration.getGlobalConfiguration();
		Logger log = Logger.getSingleton();
		final int nThreads = conf.getInteger("nThreads");
		Thread[] workers = new Thread[nThreads];
		for (int i = 0; i < nThreads; i++) {
			Thread t = new Thread(new PipelineWorker());
			t.start();
			workers[i] = t;
		}
		for (int i = 0; i < nThreads; i++) {
			try {
				workers[i].join();
			} catch (InterruptedException e) {
				log.error("Join of worker threads interrupted: " 
						+ e.getMessage());
			}
		}
	}
	
	
	/*
	 * Get the list of fully configured operations from the pipeline argument.
	 */
	private List<Operation> getOperations() {
		String pipeline = Configuration.getGlobalConfiguration()
			.getString("pipeline").trim();
		List<Operation> ops = new LinkedList<Operation>();
		for (String cmd : pipeline.split("\\|")) {
			Operation op = OperationFactory.createOperation(cmd);
			if (op != null) {
				ops.add(op);
			}
		}
		return ops;
	}
	
	/*
	 * Get the list of input file names from the inputFile parameter.
	 */
	private List<String> getInputFileNames() {
		Logger log = Logger.getSingleton();
		List<String> fileNames = new LinkedList<String>();
		String inputFileParam = Configuration.getGlobalConfiguration()
				.getString("inputFile");
		Individual soleIndy = new Individual();
		int oldLevel = log.getLevel();
		log.setLevel(Logger.FATAL_ERROR + 1);
		if (soleIndy.loadContent(inputFileParam)) { // Loading successful
			fileNames.add(inputFileParam);
			log.setLevel(oldLevel);
			return fileNames;
		}
		log.setLevel(oldLevel);
		// Otherwise: inputFileParam points to a list of paths
		int nSkipped = 0;
		try {
			FileInputStream fstream = new FileInputStream(inputFileParam);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				String trimmed = strLine.trim();
				if (trimmed.isEmpty() || strLine.charAt(0) == '#') { // Comment
					continue;
				}
				File f = new File(trimmed);
				if(f.exists() && !f.isDirectory()) {
					fileNames.add(trimmed);
				} else {
					nSkipped++;
				}
			}
			in.close();
		} catch (Exception e) {// Catch exception if any
			log.fatalError("Input file '" + inputFileParam + "' reading error: "
					+ e.getMessage());
		}
		if (nSkipped > 0) {
			log.warn(nSkipped + " input paths skipped (file not exists)");
		}
		return fileNames;
	}
}
