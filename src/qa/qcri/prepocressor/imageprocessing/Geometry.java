package qa.qcri.prepocressor.imageprocessing;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Provides geometric functions.
 * 
 * @author Felix Stahlberg
 */
final public class Geometry {
	private Geometry() {
		;
	}
	
	/**
	 * Calculates the sum of elements within rectangles in the image. The 
	 * rectangles have a common edge point (fix point) but their width and 
	 * height vary. Produces a maxHeight times maxWidth matrix storing the sum
	 * within corresponding rectangles.
	 * 
	 * @param input input image
	 * @param fixRow row number of the fix point
	 * @param fixCol column number of the fix point
	 * @param maxHeight maximum height of the rectangles
	 * @param maxWidth maximum width of the rectangles
	 * @return maxHeight times maxWidth matrix with rectangle sums
	 */
	public static Mat rectSum(Mat input, int fixRow, int fixCol, 
			int maxHeight, int maxWidth) {
		Mat cropped = input.submat(
				Math.min(fixRow, fixRow + maxHeight),
				Math.max(fixRow, fixRow + maxHeight),
				Math.min(fixCol, fixCol + maxWidth),
				Math.max(fixCol, fixCol + maxWidth));
		Mat norm = cropped; // Normalized image width correct width
							// and fix point at (0,0)
		if (maxHeight < 0 || maxWidth < 0) { // Flip image
			norm = new Mat(input.rows(), input.cols(), input.type());
			int flipType = 0;
			if (maxWidth < 0) {
				flipType = maxHeight < 0 ? -1 : 1;
			}
			Core.flip(cropped, norm, flipType);
		}
		int height = norm.rows();
		int width = norm.cols();
		Mat acc = new Mat(height, width, CvType.CV_32FC1);
		double[] lastAccRow = new double[width];
		for (int rectHeight = 0; rectHeight < height; rectHeight++) {
			double[] newAccRow = new double[width];
			double lineAcc = 0.0;
			for (int rectWidth = 0; rectWidth < width; rectWidth++) {
				lineAcc += norm.get(rectHeight, rectWidth)[0];
				double val = lineAcc + lastAccRow[rectWidth];
				newAccRow[rectWidth] = val;
				acc.put(rectHeight, rectWidth, new double[]{val});
			}
			lastAccRow = newAccRow;
		}
		if (maxHeight < 0 || maxWidth < 0) { // image flipped
			norm.release();
		}
		return acc;
	}
	
	/**
	 * Calculates a map of connected component density. Each connected 
	 * component adds 1/area to all pixels in its bounding box where area
	 * is the size of the bounding box.
	 * 
	 * @param input input image
	 * @param minSize bounding boxes below this size are ignored
	 * @return density map
	 */
	public static Mat componentDensity(Mat input, int minSize) {
		Mat dst = new Mat(input.rows(), input.cols(), CvType.CV_32FC1);
		List<MatOfPoint> contours = new LinkedList<MatOfPoint>();
		Mat hierarchy = new Mat(input.rows(), input.cols(), input.type());
		Imgproc.findContours(input, contours, hierarchy, 
				Imgproc.RETR_EXTERNAL,
				Imgproc.CHAIN_APPROX_SIMPLE);
		for (MatOfPoint contour : contours) {
			Rect rect = Imgproc.boundingRect(contour);
			if (rect.width*rect.height >= minSize) {
				double val = 1.0/(rect.width*rect.height);
				for (int x = rect.x; x <= rect.x + rect.width; x++) {
					for (int y = rect.y; y <= rect.y + rect.height; y++) {
						dst.put(y, x, new double[]{dst.get(y, x)[0] + val});
					}
				}
			}
		}
		return dst;
	}
	
	/**
	 * Remove connected components in the image that are either too large
	 * (width > maxWidth and height > maxHeight) or too small (width < minWidth
	 * and height < minHeight). The method operates in-place on the input image.
	 * 
	 * @param input input image
	 * @param minWidth minimum width of connected components
	 * @param maxWidth maximum width of connected components
	 * @param minHeight minimum height of connected components
	 * @param maxHeight maximum height of connected components
	 * @param mode Retrieval mode for findContours. See the Imgproc.RETR_* constants
	 * @return input image without large connected components
	 */
	public static Mat removeConnectedComponents(Mat input, 
			int minWidth, int maxWidth, int minHeight, int maxHeight,
			int mode) {
		Mat working = new Mat(input.rows(), input.cols(), input.type());
		input.copyTo(working);
		List<MatOfPoint> contours = new LinkedList<MatOfPoint>();
		Mat drawed = Mat.ones(new Size(input.cols(),input.rows()), CvType.CV_8UC1);
		Mat hierarchy = new Mat(input.rows(), input.cols(), input.type());
		Imgproc.findContours(working, contours, hierarchy, 
				mode,
				Imgproc.CHAIN_APPROX_NONE);
		int idx = 0;
		for (MatOfPoint contour : contours) {
			Rect rect = Imgproc.boundingRect(contour);
			if ((rect.width > maxWidth && rect.height > maxHeight) || // too large
					(rect.width < minWidth && rect.height < minHeight)) { // too small
				Imgproc.drawContours(drawed, contours, idx, Scalar.all(0),
					Core.FILLED);
			}
			idx++;
		}
		Core.multiply(input, drawed, input);
		working.release();
		drawed.release();
		hierarchy.release();
		return input;
	}
	
	/**
	 * Discrete Fourier transform.
	 * 
	 * @param input input image
	 * @return Fourier transformed image
	 */
	public static Mat dft(Mat input) {
	    Mat padded = new Mat(input.rows(), input.cols(), CvType.CV_32FC1);
	    int m = Core.getOptimalDFTSize(input.rows());
	    int n = Core.getOptimalDFTSize(input.cols() );
	    Imgproc.copyMakeBorder(input, padded,
	    	0, m - input.rows(), 0, n - input.cols(),
	    	Imgproc.BORDER_CONSTANT, Scalar.all(0));
	    List<Mat> planes = new LinkedList<Mat>();
	    planes.add(padded);
	    planes.add(Mat.zeros(padded.size(), CvType.CV_32F));
	    Mat complexI = new Mat(input.rows(), input.cols(), CvType.CV_32FC2);;
	    Core.merge(planes, complexI);
	    Core.dft(complexI, complexI);
	    return complexI;
	}
	
	/**
	 * Discrete Cosine transform.
	 * 
	 * @param input input image
	 * @return Cosine transformed image
	 */
	public static Mat dct(Mat input) {
	    Mat padded = input;
	    int m = input.rows() % 2;
	    int n = input.cols() % 2;
	    if (m + n > 0) {
	    	padded = new Mat(input.rows(), input.cols(), CvType.CV_32FC1);
	    	Imgproc.copyMakeBorder(input, padded,
	    			0, m, 0, n,
	    			Imgproc.BORDER_CONSTANT, Scalar.all(0));
	    }
	    Mat dst = new Mat(padded.rows(), padded.cols(), CvType.CV_32FC1);;
	    Core.dct(padded, dst);
	    Core.log(dst, dst);
	    return dst;
	}
	
	
	/**
	 * Perform one thinning iteration.
	 * Normally you wouldn't call this function directly from your code.
	 *
	 * @param  im    Binary image with range = 0-1
	 * @param  iter  0=even, 1=odd
	 * @see http://opencv-code.com/quick-tips/implementation-of-thinning-algorithm-in-opencv/
	 */
	private static Mat thinningIteration(Mat im, int iter) {
	    Mat marker = Mat.ones(im.size(), CvType.CV_8UC1);
	    for (int i = 1; i < im.rows()-1; i++) {
	        for (int j = 1; j < im.cols()-1; j++) {
	            double p2 = im.get(i-1, j)[0];
	            double p3 = im.get(i-1, j+1)[0];
	            double p4 = im.get(i, j+1)[0];
	            double p5 = im.get(i+1, j+1)[0];
	            double p6 = im.get(i+1, j)[0];
	            double p7 = im.get(i+1, j-1)[0];
	            double p8 = im.get(i, j-1)[0];
	            double p9 = im.get(i-1, j-1)[0];

	            int A  = ((p2 < 0.5 && p3 > 0.5) ? 1 : 0) + ((p3 < 0.5 && p4 > 0.5) ? 1 : 0) + 
	                     ((p4 < 0.5 && p5 > 0.5) ? 1 : 0) + ((p5 < 0.5 && p6 > 0.5) ? 1 : 0) + 
	                     ((p6 < 0.5 && p7 > 0.5) ? 1 : 0) + ((p7 < 0.5 && p8 > 0.5) ? 1 : 0) +
	                     ((p8 < 0.5 && p9 > 0.5) ? 1 : 0) + ((p9 < 0.5 && p2 > 0.5) ? 1 : 0);
	            int B  = (int) (p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9);
	            int m1 = (int) (iter == 0 ? (p2 * p4 * p6) : (p2 * p4 * p8));
	            int m2 = (int) (iter == 0 ? (p4 * p6 * p8) : (p2 * p6 * p8));

	            if (A == 1 && (B >= 2 && B <= 6) && m1 == 0 && m2 == 0)
	                marker.put(i,j, new double[]{0.0});
	        }
	    }
	    
	    Core.bitwise_and(im, marker, im);
	    marker.release();
	    return im;
	}

	/**
	 * Function for thinning the given binary image
	 *
	 * @param  im  Binary image with range = 0-255
	 * @see http://opencv-code.com/quick-tips/implementation-of-thinning-algorithm-in-opencv/
	 */
	public static Mat thinning(Mat imOrg) {
		Mat im = Mat.zeros(
			imOrg.rows()+2, imOrg.cols()+2, imOrg.type());
		Imgproc.copyMakeBorder(imOrg, im, 1, 1, 1, 1, Imgproc.BORDER_CONSTANT);
		Core.normalize(im, im, 0, 1, Core.NORM_MINMAX);
	    Mat prev = Mat.zeros(im.size(), CvType.CV_8UC1);
	    Mat diff = Mat.zeros(im.size(), CvType.CV_8UC1);
	    do {
	        im = thinningIteration(im, 0);
	        im = thinningIteration(im, 1);
	        Core.absdiff(im, prev, diff);
	        im.copyTo(prev);
	    } while (Core.countNonZero(diff) > 4);
	    im.submat(1, im.rows() - 1, 1, im.cols() - 1).copyTo(imOrg);
	    prev.release();
	    diff.release();
	    im.release();
	    return imOrg;
	}
	
	/**
	 * Applies a steerable ellipsoid filter to create an adaptive local
	 * connectivity map. The ALCM of each direction is reduced horizontally
	 * to a single col. The i-th col of the resulting image corresponds to
	 * the direction i. i encodes the angle 180*i/resolution.
	 * If resolution=2, i=0 is the horizontal, i=1 the vertical ALCM.
	 * The resulting image has the width {@code resolution}.
	 * 
	 * @param input input image
	 * @param kSize size of the kernel. Should have width>height
	 * @param resolution angle resolution and width of the output image. >0.
	 * @return Matrix consisting of cols corresponding to horizontally reduced
	 * 		ALCMs in a specific direction
	 * @throws IllegalArgumentException if resolution < 1 or height > width
	 * @throws NullPointerException if {@code input} is null
	 */
	public static Mat reducedALCM(Mat input, Size kSize, int resolution) 
			throws IllegalArgumentException {
		if (resolution < 1) {
			throw new IllegalArgumentException("Resolution must be >= 1");
		}
		if (kSize.width < kSize.height) {
			throw new IllegalArgumentException("Width must be larger than height");
		}
		Mat dst = Mat.zeros(input.rows(), resolution, CvType.CV_32FC1);
		Mat reduced = Mat.zeros(input.rows(), input.cols(), CvType.CV_32FC1);
		Mat ellipse = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, kSize);
		Point center = new Point(ellipse.cols()/2, ellipse.rows()/2);
		Size kernelSize = new Size(kSize.width, kSize.width);
		for (int x = 0; x < resolution; x++) {
			Mat alcm = Mat.zeros(input.rows(), input.cols(), CvType.CV_32FC1);
			Mat kernel = Mat.zeros(kernelSize, ellipse.type());
			Mat r = Imgproc.getRotationMatrix2D(center,
				180 * x / resolution, 1.0);
			r.put(1, 2, new double[]{
					r.get(1, 2)[0] +
					kSize.width/2 -kSize.height/2}); // Translation into center
			Imgproc.warpAffine(ellipse, kernel, r, kernelSize);
			Imgproc.filter2D(input, alcm, CvType.CV_32FC1, kernel);
			Core.reduce(alcm, reduced, 1, Core.REDUCE_SUM);
			Core.add(reduced, reduced, dst.col(x));
			alcm.release();
			kernel.release();
			r.release();
		}
		ellipse.release();
		return dst;
	}
	
	/**
	 * Computes a connectivity map based on run length from pixels in specific
	 * directions. The i-th col of the resulting image corresponds to
	 * the direction i. i encodes the angle 180*i/resolution.
	 * If resolution=2, i=0 is the horizontal, i=1 the vertical ALCM.
	 * The resulting image has the width {@code resolution}.
	 * 
	 * @param input input image
	 * @param resolution angle resolution and width of the output image. >0.
	 * @return Matrix consisting of cols corresponding to horizontally reduced
	 * 		run length information
	 * @throws IllegalArgumentException if resolution < 1
	 * @throws NullPointerException if {@code input} is null
	 */
	public static Mat reducedRunlength(Mat input, int resolution) 
			throws IllegalArgumentException {
		if (resolution < 1) {
			throw new IllegalArgumentException("Resolution must be >= 1");
		}
		Mat dst = Mat.zeros(input.rows(), resolution, CvType.CV_32FC1);
		Mat norm = Mat.zeros(input.rows(), input.cols(), CvType.CV_32FC1);
		Mat reduced = Mat.zeros(input.rows(), input.cols(), CvType.CV_32FC1);

		Mat[] maps = new Mat[resolution];
		for (int x = 0; x < resolution; x++) {
			double theta = Math.PI * x / resolution;
			maps[x] = runlengthMap(input, theta);
		}
		for (int row = 0; row < input.rows(); row++) {
			for (int col = 0; col < input.cols(); col++) {
				double acc = 0.0;
				for (Mat map : maps) {
					acc += map.get(row, col)[0];
				}
				if (acc > 0.0001) {
					for (Mat map : maps) {
						map.put(row, col, new double[]{map.get(row, col)[0]/acc});
					}
				}
			}
		}
		for (int x = 0; x < resolution; x++) {
			Core.reduce(maps[x], reduced, 1, Core.REDUCE_SUM);
			Core.normalize(reduced, norm, 0, 0.5, Core.NORM_MINMAX);
			Core.add(reduced, reduced, dst.col(x));
			maps[x].release();
		}
		/*for (int row = 0; row < input.rows(); row++) {
			Core.normalize(dst.row(row), dst.row(row), 0.0, 1.0, Core.NORM_MINMAX);
		}*/
		reduced.release();
		norm.release();
		return dst;
	}
	
	public static Mat runlengthMap(Mat input, double theta) {
		LinkedList<Point> maskPoints = new LinkedList<Point>();
		// Generate list of relative point position from near to far
		double a = Math.cos(theta);
		double b = Math.sin(theta);
		int cols = input.cols();
		int rows = input.rows();
		int oldMaskX = 0, oldMaskY = 0;
		for (double scale = 1.0; true; scale++) {
			int maskX = (int) (a*scale);
			int maskY = (int) (b*scale);
			if (Math.abs(maskX)  > cols 
					|| Math.abs(maskY)  > rows) {
				break;
			}
			if (maskX != oldMaskX || maskY != oldMaskY) {
				maskPoints.add(new Point(maskX, maskY));
				oldMaskX = maskX;
				oldMaskY = maskY;
			}
		}
		Mat dst = Mat.zeros(input.rows(), input.cols(), CvType.CV_32FC1);
		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				if (input.get(row, col)[0] > 0.5) {
					// Furthest point in positive direction
					int posX = col;
					int posY = row;
					for (Point p : maskPoints) {
						int x = (int) (col + p.x);
						int y = (int) (row + p.y);
						if (x < 0 || y < 0 || x >= cols || y >= rows
								|| input.get(y, x)[0] < 0.5) {
							posX = x; // May break on completely white images
							posY = y;
							break;
						}
					}
					// Furthest point in negative direction
					int negX = col;
					int negY = row;
					for (Point p : maskPoints) {
						int x = (int) (col - p.x);
						int y = (int) (row - p.y);
						if (x < 0 || y < 0 || x >= cols || y >= rows
								|| input.get(y, x)[0] < 0.5) {
							negX = x; // May break on completely white images
							negY = y;
							break;
						}
					}
					int xDist = Math.abs(posX - negX) + 1;
					int yDist = Math.abs(posY - negY) + 1;
					dst.put(row, col,
						new double[]{Math.sqrt(xDist*xDist + yDist*yDist)});
				}
			}
		}
		return dst;
	}
	
	/**
	 * OpenCV does not support median filtering with non-quadratic kernels.
	 * Therefore, this is a slower but more flexible medianBlur implementation
	 * 
	 * @param input input image
	 * @param xSize kernel size in x direction
	 * @param ySize kernel size in y direction
	 * @return blurred image
	 */
	public static Mat myMedianBlur(Mat input, int xSize, int ySize) {
		if (xSize == ySize && xSize > 1 && xSize % 2 == 1) {
			Imgproc.medianBlur(input, input, xSize);
			return input;
		}
		Point topLeft = new Point(-xSize/2, -ySize/2);
		Point bottomRight = new Point(topLeft.x + xSize, topLeft.y + ySize);
		Mat dst = new Mat(input.rows(), input.cols(), input.type());
		for (int row = 0; row < input.rows(); row++) {
			Point submatTopLeft = new Point(0,
				Math.max(0, row + topLeft.y));
			Point submatBottomRight = new Point(0,
				Math.min(input.rows(), row + bottomRight.y));
			for (int col = 0; col < input.cols(); col++) {
				submatTopLeft.x = Math.max(0, col+topLeft.x);
				submatBottomRight.x = Math.min(input.cols(), col+bottomRight.x);
				Rect submat = new Rect(submatTopLeft, submatBottomRight);
				int l = submat.height*submat.width;
				float[] vals = new float[l];
				input.submat(submat).get(0, 0, vals);
				Arrays.sort(vals);
				float med;
				if (l % 2 == 0) {
					med = (vals[l/2] + vals[l/2-1])/2;
				} else {
					med = vals[l/2];
				}
				dst.put(row, col, new double[]{med});
			}
		}
		input.release();
		return dst;
	}
}
