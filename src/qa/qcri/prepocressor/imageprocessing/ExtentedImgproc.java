package qa.qcri.prepocressor.imageprocessing;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Core.MinMaxLocResult;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Extended methods in context of the Hough transform.
 * 
 * @author Felix Stahlberg
 */
final public class ExtentedImgproc {
	
	/**
	 * Bresenham method for lines (fast)
	 */
	public final static int BRESENHAM = 1;
	
	/**
	 * Exactly tracking fraction of pixels
	 */
	public final static int EXACT = 2;
	
	/**
	 * Original method based on increasing scaling factors
	 */
	public final static int SCALING = 3;
	
	/* (non-Javadoc)
	 * Internal class for representing weighted points
	 */
	static private class WeightedPoint {
		int x,y;
		double weight;
		
		public WeightedPoint(int x, int y, double weight) {
			this.x = x;
			this.y = y;
			this.weight = weight;
		}
		/**
		 * @return the x
		 */
		public int getX() {
			return x;
		}
		/**
		 * @return the y
		 */
		public int getY() {
			return y;
		}
		/**
		 * @return the weight
		 */
		public double getWeight() {
			return weight;
		}
		
	}
	
	
	private ExtentedImgproc() {
		;
	}

	/**
	 * This is a specialized and modified version of the Hough transformation.
	 * In contrast to the Hough space, rho is always on the x-axis. Rho ranges
	 * from 0 to image width. The range and resolution for theta can be 
	 * specified. The advantage of this implementation is that there are no
	 * quantization errors for rho since the resolution is exactly one pixel. 
	 * The disadvantage is that only lines crossing the x axis between 0 and 
	 * image width are considered. The returned image contains the counts
	 * where the y coordinate represents theta. The {@link #SCALING} line
	 * representation is used.
	 * 
	 * @param input input image
	 * @param fromTheta minimal value for theta in radians
	 * @param toTheta maximal value for theta in radians
	 * @param thetaRes number of quantization steps for theta
	 * @return hough transformed image
	 */
	static public Mat axisAlignedHoughTransform(Mat input, double fromTheta,
			double toTheta, int thetaRes) {
		return axisAlignedHoughTransform(input, fromTheta, toTheta, thetaRes,
			SCALING);
	}
	
	/**
	 * Normalize all channels of the input image to the given range
	 * 
	 * @param input input image
	 * @param newMin new minimum
	 * @param newMax new maximum
	 */
	static public void normalize(Mat input, double newMin, double newMax) {
		List<Mat> channels = new LinkedList<Mat>();
		Core.split(input, channels);
		for (Mat channel : channels) {
			Mat shifted = new Mat(channel.size(), channel.type());
			Mat dst = null;
			MinMaxLocResult minMax = Core.minMaxLoc(channel);
			Core.add(channel, new Scalar(-minMax.minVal + newMin), shifted);
			if (minMax.maxVal - minMax.minVal > 0.0000001) {
				Mat ones = Mat.ones(channel.size(), channel.type());
				dst = shifted.mul(ones,
					newMax / (minMax.maxVal - minMax.minVal));
				shifted.release();
				ones.release();
			} else {
				dst = shifted;
			}
			dst.copyTo(channel);
			dst.release();
		}
		Core.merge(channels, input);
		for (Mat channel : channels) {
			channel.release();
		}
	}
	
	/**
	 * This is a specialized and modified version of the Hough transformation.
	 * In contrast to the Hough space, rho is always on the x-axis. Rho ranges
	 * from 0 to image width. The range and resolution for theta can be 
	 * specified. The advantage of this implementation is that there are no
	 * quantization errors for rho since the resolution is exactly one pixel. 
	 * The disadvantage is that only lines crossing the x axis between 0 and 
	 * image width are considered. The returned image contains the counts
	 * where the y coordinate represents theta.
	 * 
	 * @param input input image
	 * @param fromTheta minimal value for theta in radians
	 * @param toTheta maximal value for theta in radians
	 * @param thetaRes number of quantization steps for theta
	 * @param mode specify the line representation mode. See {@link #BRESENHAM},
	 * 		{@link #EXACT}, and {@link #SCALING}
	 * @return hough transformed image
	 */
	static public Mat axisAlignedHoughTransform(Mat input, double fromTheta,
			double toTheta, int thetaRes, int mode) {
		int rows = input.rows();
		int cols = input.cols();
		Mat dst = Mat.zeros(thetaRes, cols, CvType.CV_32FC1);
		double thetaScale = (toTheta - fromTheta) /
				(thetaRes <= 1 ? 1 : (thetaRes-1));
		for (int y = 0; y < thetaRes; y++) {
			double theta = fromTheta + y * thetaScale;
			double alpha = Math.PI/2.0 - theta;
			for (int x = 0; x < cols; x++) {
				double acc = 0.0;
				List<WeightedPoint> points = (mode == BRESENHAM)
					? getLineWithBresenham(input, 0, 0,
							(int) Math.round(Math.tan(alpha) * rows), rows)
					: (mode == EXACT ? getExactLine(rows, alpha)
							: getLineWithScaling(theta, rows));
				for (WeightedPoint p : points) {
					int px = x + p.getX();
					int py = p.getY();
					if (px < 0 || px >= cols 
							|| py < 0 || py >= rows) {
						break;
					}
					acc += p.getWeight() * input.get(py, px)[0];
				}
				dst.put(y, x, new double[]{acc});
			}
		}
		return dst;
	}
	
	/* (non-Javadoc)
	 * Find line with increasing scaling factor gradually
	 */
	private static List<WeightedPoint> getLineWithScaling(double theta, 
			int rows) {
		List<WeightedPoint> points = new LinkedList<WeightedPoint>();
		double a = Math.cos(theta);
		double b = Math.sin(theta);
		int oldMaskX = -1, oldMaskY = -1;
		for (double scale = 0.0; true; scale++) {
			int maskX = (int) (a * scale);
			int maskY = (int) (b * scale);
			if (maskY < 0 || maskY >= rows) {
				break;
			}
			if (maskX != oldMaskX || maskY != oldMaskY) {
				points.add(new WeightedPoint(maskX, maskY, 1.0));
				oldMaskX = maskX;
				oldMaskY = maskY;
			}
		}
		return points;
	}
	
	/* (non-Javadoc)
	 * Returns list of exactly weighted points that comprise the line
	 */
	private static List<WeightedPoint> getExactLine(int rows, double alpha) {
		List<WeightedPoint> points = new LinkedList<WeightedPoint>();
		for (int r = 1; r < rows; r++) {
			double upper = Math.tan(alpha) * (r-0.5);
			double lower = Math.tan(alpha) * (r+0.5);
			double left = Math.min(upper, lower);
			double right = Math.max(upper, lower);
			double width = right - left;
			double length = Math.sqrt(1 + width*width);
			for (int c = (int) left - 1; c <= right + 1; c++) {
				double fraction = (Math.min(c+0.5, right) - Math.max(c-0.5, left))/width;
				if (fraction > 0.0) {
					double factor = fraction * length;
					points.add(new WeightedPoint(c, r, factor));
				}
			}
		}
		return points;
	}
	
	/**
	 * Returns the list of array elements that comprise the line. 
	 * 
	 * @param grid the 2d array
	 * @param x0 the starting point x
	 * @param y0 the starting point y
	 * @param x1 the finishing point x
	 * @param y1 the finishing point y
	 * @return the line as a list of array elements
	 * @see https://github.com/fragkakis/bresenham
	 */
	private static List<WeightedPoint> getLineWithBresenham(Mat input, 
			int x0, int y0, int x1, int y1) {
		List<WeightedPoint> points = new LinkedList<WeightedPoint>();
		int dx = Math.abs(x1 - x0);
		int dy = Math.abs(y1 - y0);
		
		int sx = x0 < x1 ? 1 : -1; 
		int sy = y0 < y1 ? 1 : -1; 
		
		int err = dx-dy;
		int e2;
		int currentX = x0;
		int currentY = y0;
		
		while(true) {
			points.add(new WeightedPoint(currentX, currentY, 1.0));
			if(currentX == x1 && currentY == y1) {
				break;
			}
			
			e2 = 2*err;
			if(e2 > -1 * dy) {
				err = err - dy;
				currentX = currentX + sx;
			}
			
			if(e2 < dx) {
				err = err + dx;
				currentY = currentY + sy;
			}
		}
				
		return points;
	}
}
