package qa.qcri.prepocressor.datastructures;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.highgui.Highgui;

import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * An individual represents an entity that can be passed through the pipeline.
 * The entity is usually associated with a file name and an OpenCV Mat file.
 * 
 * @see Population
 * @author Felix Stahlberg
 */
public class Individual {

	protected String idxPrefix;
	protected Mat content;
	protected Population pop;
	protected Individual parent;
	protected Rect parentRect;
	
	/**
	 * Builds an individual instance without content.
	 */
	public Individual() {
		content = null;
		idxPrefix = "";
		pop = null;
		parent = null;
		parentRect = null;
	}
	
	/**
	 * Set the population this individual belongs to.
	 * 
	 * @param pop new population instance
	 */
	public void setPopulation(Population pop) {
		this.pop = pop;
	}
	
	/**
	 * @return true iff parent is not null
	 */
	public boolean hasParent() {
		return parent != null;
	}
	
	/**
	 * @return parent individual or null if this individual has no parent
	 */
	public Individual getParent() {
		return parent;
	}
	
	/**
	 * @return region of the parent individual in which this individual is
	 * 		placed or null if this individual has no parent
	 */
	public Rect getParentRect() {
		return parentRect;
	}
	
	/**
	 * Set a new parent.
	 * 
	 * @param parent parent individual
	 * @param parentRect region of the children within the parent
	 */
	public void setParent(Individual parent, Rect parentRect) {
		this.parent = parent;
		this.parentRect = parentRect;
	}
	
	/**
	 * @return population this individual belongs to
	 */
	public Population getPopulation() {
		return pop;
	}
	
	/**
	 * Builds an individual instance without content.
	 * 
	 * @param idxPrefix prefix to be prepend when substituting %idx
	 */
	public Individual(String idxPrefix) {
		content = null;
		this.idxPrefix = idxPrefix;
	}

	/**
	 * Load the content of this individual from a file. The file name is passed
	 * through to OpenCV's imread function. If this function fails, we try to
	 * parse it as csv file. If this fails, we return false.
	 * 
	 * @param fileName path to the file
	 * @return true if the content was loaded successfully, false otherwise
	 */
	public boolean loadContent(String fileName) {
		content = Highgui.imread(fileName);
		if (content == null || content.rows() * content.cols() == 0) {
			loadCsv(fileName);
		}
		return content != null;
	}
	
	/**
	 * @return the index prefix. The index prefix is a string
	 * 		that is inserted when %idx is substituted
	 */
	public String getIdxPrefix() {
		return idxPrefix;
	}
	
	/**
	 * @return the content of this individual which is an OpenCV Mat (image)
	 */
	public Mat getContent() {
		return content;
	}

	/**
	 * @param idxPrefix the index prefix to set. The index prefix is a string
	 * 		that is inserted when %idx is substituted
	 */
	public void setIdxPrefix(String idxPrefix) {
		this.idxPrefix = idxPrefix;
	}
	
	/**
	 * Write the content of this individual into a file.
	 * 
	 * @param path Path with placeholders to the target files. See the
	 * 	-outputPath parameter
	 * @return true if the file was written successfully, false otherwise
	 * @throws NullPointerException if fileNamePattern is null or the content
	 * 		of this individual is null or if the population was not set
	 * @see #setPopulation(Population)
	 */
	public boolean store(String path) {
		String fileName = pop.getFullIndividualFileName(path, idxPrefix);
		File f = new File(fileName);
		if(f.exists() && Configuration.getGlobalConfiguration()
				.getInteger("silentOverwrite") == 0) {
			Logger.getSingleton().warn("Did not write file " + fileName
				+ " (already exists and -silentOverwrite=0)");
			return false;
		}
		Logger.getSingleton().debug("Create file " + fileName);
		int lastDot = fileName.lastIndexOf(".");
		if (lastDot >= 0 && fileName.substring(lastDot)
				.toLowerCase().equals(".csv")) {
			return storeCsv(fileName);
		}
		return Highgui.imwrite(fileName, content);
	}
	
	/**
	 * Get the full target file name.
	 * 
	 * @param path Path with placeholders to the target files. See the
	 * 	-outputPath parameter
	 * @return target file name
	 */
	public String getTargetFileName(String path) {
		return pop.getFullIndividualFileName(path, idxPrefix);
	}
	
	/**
	 * Store the content as csv file (only the first channel).
	 * 
	 * @param fileName path to the file to write (without placeholders)
	 * @return false if an error occurred, true otherwise
	 * @throws NullPointerException if fileName or content is null
	 */
	public boolean storeCsv(String fileName) {
		try {
			FileWriter fs = new FileWriter(fileName);
			BufferedWriter out = new BufferedWriter(fs);
			for (int row = 0; row < content.rows(); row++) {
				StringBuilder sb = new StringBuilder();
				for (int col = 0; col < content.cols(); col++) {
					sb.append("\t" + content.get(row, col)[0]);
				}
				out.write(sb.toString().substring(1) + "\n");
			}
			out.close();
		} catch (IOException e) {
			Logger.getSingleton().warn("Error while writing "
					+ fileName + ": " + e.getMessage());
			return false;
		}
		return true;
	}
	

	/**
	 * Load the content from a CSV file. The length of the first row defines 
	 * the number of columns. If consecutive lines contain a different number
	 * of elements, output a warning and fill up with zeros / cut.
	 * 
	 * @param fileName path to the CSV file (without placeholders)
	 * @return true if the content was loaded successfully, false otherwise
	 */
	public boolean loadCsv(String fileName) {
		content = null;
		Logger log = Logger.getSingleton();
		List<String> rows = new LinkedList<String>();
		try {
			FileInputStream fstream = new FileInputStream(fileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				rows.add(strLine.trim());
			}
			in.close();
		} catch (Exception e) {// Catch exception if any
			log.warn("Input file '" + fileName + "' reading error: "
					+ e.getMessage());
			return false;
		}
		if (rows.isEmpty()) {
			log.warn("Input file '" + fileName + "' is empty.");
			return false;
		}
		int cols = rows.get(0).split(" +").length;
		boolean nColsMismatch = false;
		int row = 0;
		content = Mat.zeros(rows.size(), cols, CvType.CV_8UC1);
		try {
			for (String strRow : rows) {
				String[] vals = strRow.split(" +");
				if (vals.length != cols) {
					nColsMismatch = true;
				}
				for (int col = 0; col < Math.min(cols, vals.length); col++) {
					content.put(row, col, Integer.parseInt(vals[col]));
				}
				row++;
			}
		} catch (NumberFormatException e) {
			log.error("Input file '" + fileName + "' is not in CSV format: Non"
					+ "-numeric and non-whitespace characters found");
			content = null;
			return false;
		}
		if (nColsMismatch) {
			log.warn("Input file '" + fileName + "' lines in CSV file do not "
				+ "have the same lengths. Cropped or filled up with zeros.");
		}
		return true;
	}

	/**
	 * Set individual content.
	 * 
	 * @param content new content
	 */
	public void setContent(Mat content) {
		this.content = content;
	}
	
	/**
	 * Produce a child from a submatrix of the content.
	 * 
	 * @param roi region in content of this child
	 * @return child individual instance
	 * @throws NullPointerException if content or roi is null
	 */
	public Individual produceChild(Rect roi) {
		return produceChild(roi, true);
	}
	
	/**
	 * Produce a child from a submatrix of the content.
	 * 
	 * @param roi region in content of this child
	 * @param setContent set child content to submatrix
	 * @return child individual instance
	 * @throws NullPointerException if content or roi is null
	 */
	public Individual produceChild(Rect roi, boolean setContent) {
		Individual child = new Individual();
		child.content = setContent ? content.submat(roi) : null;
		child.idxPrefix = idxPrefix + "_child"; // Should be changed later
		child.pop = pop;
		child.parent = this;
		child.parentRect = roi;
		return child;
	}
}
