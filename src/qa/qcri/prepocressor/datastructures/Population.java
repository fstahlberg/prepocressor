package qa.qcri.prepocressor.datastructures;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * A population is a set of individuals. Normally, all individuals originate
 * from the same input file.
 * 
 * @see Individual
 * @author Felix Stahlberg
 */
public class Population implements Iterable<Individual> {
	
	protected List<Individual> individuals;
	protected String inputFileName;

	/**
	 * Creates a new and empty population instance.
	 * 
	 * @param inputFileName name of the original file from which all individuals
	 * 		in this population are derived
	 */
	public Population(String inputFileName) {
		individuals = new LinkedList<Individual>();
		this.inputFileName = inputFileName;
	}
	
	/**
	 * Get name of the original file.
	 * 
	 * @return the input file name
	 */
	public String getInputFileName() {
		return inputFileName;
	}
	
	/**
	 * Add an individual to this population.
	 * 
	 * @param individual to add
	 * @throws NullPointerException if individual is null
	 */
	public void add(Individual individual) {
		if (individual == null) {
			throw new NullPointerException();
		}
		individuals.add(individual);
		individual.setPopulation(this);
	}
	
	/**
	 * Add a collection of individuals to this population.
	 * 
	 * @param individuals to add
	 * @throws NullPointerException if individuals is null
	 */
	public void addAll(Collection<? extends Individual> individuals) {
		this.individuals.addAll(individuals);
		for (Individual indy : individuals) {
			indy.setPopulation(this);
		}
	}

	@Override
	public Iterator<Individual> iterator() {
		return individuals.iterator();
	}
	
	/**
	 * Get the number of individuals in the population.
	 * 
	 * @return number of individuals
	 */
	public int size() {
		return individuals.size();
	}
	
	/**
	 * Get the full name of the target file. 
	 * 
	 * @param path Path with placeholders to the target files. See the
	 * 	-outputPath parameter. This string must 
	 * 	contain the %idx placeholder. If it does not contain it, the 
	 * 	placeholder is added at the end of the file name
	 * @param idxPrefix index prefix of the current individual
	 */
	public String getFullIndividualFileName(String path, String idxPrefix) {
		int lastDot = inputFileName.lastIndexOf(".");
		int lastSlash = inputFileName.lastIndexOf("/");
		int secLastSlash = inputFileName.lastIndexOf("/", lastSlash - 1);
		String dir = "", base = "", minusBase = "", unqatip = "", ext = "";
		if (lastDot >= 0) {
			ext = inputFileName.substring(lastDot);
		}
		if (lastSlash - secLastSlash > 0) {
			dir = inputFileName.substring(secLastSlash + 1, lastSlash);
		}
		base = inputFileName.substring(lastSlash + 1,
				inputFileName.length() - ext.length());
		minusBase = base;
		unqatip = base;
		int firstMinusInBase = base.indexOf("-");
		if (firstMinusInBase >= 0) {
			minusBase = base.substring(0, firstMinusInBase);
			unqatip = base.substring(firstMinusInBase + 1);
			int firstUscore= unqatip.indexOf("_");
			if (firstUscore >= 0) {
				unqatip = removeLeading("x", unqatip.substring(0, firstUscore))
					+ "_" 
					+ removeLeading("x", unqatip.substring(firstUscore + 1));
			}
		}
		String fileNamePattern = path.replaceAll("%dir", dir)
				.replaceAll("%base", base)
				.replaceAll("%unqatip", unqatip)
				.replaceAll("%-base", minusBase)
				.replaceAll("%ext", ext);
		String fileName = fileNamePattern + idxPrefix;
		if (fileNamePattern.indexOf("%idx") >= 0) {
			fileName = fileNamePattern.replaceAll("%idx", idxPrefix);
		}
		return fileName;
	}
	
	/* (non-Javadoc)
	 * Remove any number
	 */
	private String removeLeading(String filler, String subject) {
		return subject.replaceFirst("^" + filler + "+","");
	}

	/**
	 * Writes the population to the file system.
	 * 
	 * @param path Path with placeholders to the target files. See the
	 * 	-outputPath parameter
	 */
	public void store(String path) {
		for (Individual indy : individuals) {
			indy.store(path);
		}
	}
}
