package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.Geometry;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the removeLargeComponents operation.
 * 
 * @author Felix Stahlberg
 */
public class RemoveLargeComponentsOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public RemoveLargeComponentsOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("maxWidth", 0.2f,
					"Maximum width of a connected component relative to the "
					+ "image width.");
				addParameter("maxHeight", 0.3f,
					"Maximum height of a connected component relative to the "
					+ "image height.");
			}
			@Override
			protected String getDescription() {
				return "Remove large connected components in the image -- i.e. "
					+ "components which exceed either the maximum width or the "
					+ "maximum height.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		int w = content.cols();
		int h = content.rows();
		content = Geometry.removeConnectedComponents( // Components too wide
			content, 0, (int) (conf.getFloat("maxWidth")*w), 0, h+1,
			Imgproc.RETR_EXTERNAL);
		content = Geometry.removeConnectedComponents( // Components too high
			content, 0, w+1, 0, (int) (conf.getFloat("maxHeight")*h),
			Imgproc.RETR_EXTERNAL);
		indy.setContent(content);
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		return children;
	}
}
