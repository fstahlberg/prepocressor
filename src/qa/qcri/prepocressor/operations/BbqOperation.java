package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the bbq operation.
 * 
 * @author Felix Stahlberg
 */
public class BbqOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public BbqOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("threshold", 0.5f,
					"Threshold for detecting the lowest point");
			}
			@Override
			protected String getDescription() {
				return "Keep only the lowest point in each column. The lowest"
					+ " point is detected by comparing the first channel "
					+ "with the threshold parameter.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		
		Mat dst = Mat.zeros(content.rows(), content.cols(), content.type());
		double eps = conf.getFloat("threshold");
		for (int col = 0; col < content.cols(); col++) {
			for (int row = content.rows() - 1; row >= 0; row--) {
				if (content.get(row, col)[0] >= eps) {
					dst.put(row, col, 1);//content.get(row, col));
					break;
				}
			}
		}
		Individual child = new Individual();
		child.setContent(dst);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		content.release();
		return children;
	}
}
