package qa.qcri.prepocressor.operations;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoint;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.ExtentedImgproc;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
 This file is part of the prepocressor toolkit.

 Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
 Felix Stahlberg

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

/**
 * This class implements the ascendersTextLine operation.
 * 
 * @author Felix Stahlberg
 */
public class AscendersTextLineOperation extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see Operation#Operation(String)
	 */
	public AscendersTextLineOperation(String name) {
		super(name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("descenderArea", 0.6f,
					"Bottom part of the image is discarded for ascender "
					+ "recognition. Set this to 0.5 if you've already estimated "
					+ "a linear baseline with houghTextLine to reduce the chance "
					+ "of descenders wrongly being recognized as ascenders. Set "
					+ "to 0 to disable this feature. Must be between 0 and 1.");
				addParameter("minScore", 0.4f,
					"Minimal score for ascender recognition. Between 0 and 1 (the "
					+ "higher the more restrictive). Score is composed of the "
					+ "skyline value and the vertical projection profile value.");
				addParameter("flatBorders", 1,
					"If this is set to 1, the baseline left and right from all "
					+ "data points is set to a straight horizontal line at "
					+ "the height of the curve at the last data point. This "
					+ "usually reduces artefacts due to large slopes in the "
					+ "curves outside the data point range.");
				addParameter("minScoreOffset", 0.2f,
					"Minimal score for ascender recognition. Between 0 and 1 (the "
					+ "higher the more restrictive). Score is composed of the "
					+ "skyline value and the vertical projection profile value.");
				addParameter("skylineWeight", 0.33f,
					"Weight of the skyline criterion. Weight of the projection "
					+ "profile criterion is 1 minus this value");
				addParameter("outlierFactor", -1.0f,
					"Remove data points this factor times stdDev from median. Set"
					+ " to negative value to disable outlier detection");
				addParameter("blurSize", 5,
					"Size of blurring kernel for the projection profile.");
				addParameter("minDataPoints", 8,
					"Minimum number of data points.");
				addParameter("order", 4,
					"Order of the polynom");
				addParameter("operation", "align",
					"This parameter decides what is passed through the pipeline."
					+ " Available values are:\n+\n"
					+ "* 'none': Leave the images as they are.\n"
					+ "* 'draw': Draw baseline.\n"
					+ "* 'align': Create a new image where the baseline is "
					+ "horizontal and at image height/2\n-");
			}

			@Override
			protected String getDescription() {
				return "ascendersTextLine aims to rectify baselines in Arabic script based "
					+ "on the height of ascenders: We fit a polynomial to the "
					+ "top ends of the ascenders end shift each column according "
					+ "the value of the estimated polynomial. Note that this "
					+ "operation might shift the baseline vertically, so it "
					+ "is recommended to apply a houghTextLine operation with "
					+ "low resolution afterwards.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		final int cols = content.cols();
		final float skylineWeight = conf.getFloat("skylineWeight");
		int order = conf.getInteger("order");
		Mat upper = new Mat(content.size(), content.type());
		content.submat(
				0, (int) ((1.0 - conf.getFloat("descenderArea")) * content.rows()),
				0, cols).copyTo(upper);
 
		Mat profile = getProjectionProfile(upper);
		Mat skyline = getSkyline(upper);
		float[] rawSkyline = new float[cols];
		skyline.get(0, 0, rawSkyline);
		ExtentedImgproc.normalize(profile, 0.0, 1.0 - skylineWeight);
		ExtentedImgproc.normalize(skyline, 0.0, skylineWeight);
		
		Mat scoresMat = new Mat(profile.size(), profile.type());
		Core.add(profile, skyline, scoresMat);
		float[] scores = new float[cols];
		scoresMat.get(0, 0, scores);
		
		List<WeightedObservedPoint> estPoints = getDataPoints(scores, rawSkyline,
				upper.rows());
		List<Individual> children = new LinkedList<Individual>();
		if (estPoints.size() < conf.getInteger("minDataPoints")) {
			Logger.getSingleton().error("Not enough estimation points! Pass thru "
				+ "original");
			children.add(indy);
			return children;
		}
		if (estPoints.size() <= order) {
			order = estPoints.size() - 1;
			Logger.getSingleton().warn("Decreasing order of polynomial to "
				+ order);
		}
		PolynomialCurveFitter fitter = PolynomialCurveFitter.create(order);
		final double[] coeff = fitter.fit(estPoints);
		String op = conf.getString("operation");
		if (op.equals("draw")) {
			PolynomialTextLineOperation.draw(content, coeff, estPoints);
		} else {
			int minCol = 0;
			int maxCol = cols;
			if (conf.getInteger("flatBorders") > 0) {
				minCol = (int) estPoints.get(0).getX();
				maxCol = (int) estPoints.get(estPoints.size() - 1).getX();
				coeff[0] += content.rows()/2 
						- PolynomialTextLineOperation.eval(coeff, minCol);
			} else {
				coeff[0] = content.rows()/2;
			}
			Mat dst = PolynomialTextLineOperation.align(content, coeff,
					minCol, maxCol);
			content.release();
			indy.setContent(dst);
		}
		children.add(indy);
		return children;
	}
	
	/* (non-Javadoc)
	 * Get projection profile scores (unnormalized)
	 */
	private Mat getProjectionProfile(Mat upper) { 
		final int blurSize = conf.getInteger("blurSize");
		Mat profile = new Mat(upper.size(), CvType.CV_32FC1);
		Core.reduce(upper, profile, 0, Core.REDUCE_SUM, CvType.CV_32FC1);
		if (blurSize > 1) {
			Imgproc.blur(profile, profile, new Size(blurSize, 1));
		}
		return profile;
	}
	
	/* (non-Javadoc)
	 * Get skyline scores (unnormalized)
	 */
	private Mat getSkyline(Mat upper) {
		final int rows = upper.rows();
		final int cols = upper.cols();
		Mat skyline = Mat.zeros(new Size(cols, 1), CvType.CV_32FC1);
		byte[] col = new byte[rows];
		for (int colIdx = 0; colIdx < cols; colIdx++) {
			upper.col(colIdx).get(0, 0, col);
			int rowIdx = 0;
			while (rowIdx < rows && col[rowIdx] == 0) {
				rowIdx++;
			}
			skyline.put(0, colIdx, new float[]{rows - rowIdx});
		}
		return skyline;
	}
	
	/* (non-Javadoc)
	 * Get datapoints for polynom estimation
	 */
	private List<WeightedObservedPoint> getDataPoints(float[] scores,
			float[] rawSkyline, int rows) {
		final float minScore = conf.getFloat("minScore");
		final float minOffset = conf.getFloat("minScoreOffset");
		List<WeightedObservedPoint> estPoints = 
				new ArrayList<WeightedObservedPoint>();
		for (int colIdx = 1; colIdx < scores.length - 1; colIdx++) {
			float score = scores[colIdx];
			if (score >= minScore && scores[colIdx-1] < score 
					&& scores[colIdx+1] < score) { // maximum
				int leftValley = colIdx - 1;
				int rightValley = colIdx + 1;
				while (leftValley > 0 
						&& scores[leftValley-1] < scores[leftValley]) {
					leftValley--;
				}
				while (rightValley < scores.length-1 
						&& scores[rightValley+1] < scores[rightValley]) {
					rightValley++;
				}
				if (score - Math.max(scores[leftValley], scores[rightValley])
						> minOffset) {
					estPoints.add(new WeightedObservedPoint(1.0,
						colIdx, rows - rawSkyline[colIdx]));
				}
			}
		}
		estPoints = PolynomialTextLineOperation.removeOutliers(
				conf.getFloat("outlierFactor"), estPoints);
		return estPoints;
	}
}
