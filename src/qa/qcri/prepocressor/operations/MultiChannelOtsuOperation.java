package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.primaresearch.dla.page.Page;
import org.primaresearch.dla.page.io.xml.PageXmlInputOutput;
import org.primaresearch.dla.page.layout.PageLayout;
import org.primaresearch.dla.page.layout.physical.Region;
import org.primaresearch.dla.page.layout.physical.text.LowLevelTextObject;
import org.primaresearch.dla.page.layout.physical.text.impl.TextRegion;
import org.primaresearch.io.UnsupportedFormatVersionException;
import org.primaresearch.maths.geometry.Rect;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.ExtentedImgproc;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the multiChannelOtsu operation.
 * 
 * @author Felix Stahlberg
 */
public class MultiChannelOtsuOperation extends Operation {

	/*
	 * (non-Javadoc)
	 * 
	 * @see Operation#Operation(String)
	 */
	public MultiChannelOtsuOperation(String name) {
		super(name);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("maxIter", 10, "Number of k-means iterations");
				addParameter("blackDiscount", 0.5f, "Increase this to make "
					+ "more pixels classified as white. Between 0 and 1");
				addParameter("maxForegroundFraction", 0.2f, "Maximum fraction"
					+ "of foreground pixels. If exceeded, increase blackDiscount"
					+ " parameter to produce more background");
				addParameter("minLevel", 0,
					"Minimum level in layout of region to be extracted.");
				addParameter("extractRegions", 0,
					"Extract regions.");
				addParameter("normalizeRegionChannels", 0,
					"Normalize channels in top level regions before "
					+ "binarization.");
				addParameter("extractTextObjects", 1,
					"Extract text lines.");
				addParameter("xmlPath", "",
					"Path to the xml files in PAGE format. The same place"
					+ "holders as in the global outputPath can be used. If this"
					+ "parameter is provided, binarization is done for each "
					+ "region separately. Otherwise, the algorithm is applied "
					+ "to the whole image. Note: The used PAGE library may "
					+ "break when using multiple threads");
			}

			@Override
			protected String getDescription() {
				return "This is Otsu thrsholding adapted for multichannel "
						+ "images. It uses greyscale standard otsu "
						+ "binarization for initial labeling, and then applies "
						+ "the k-means algorithm (k=2) for final binarization. "
						+ "Note: Channels > 3 (e.g. alpha channel) are not "
						+ "considered.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		Mat labels = Mat.zeros(content.size(), CvType.CV_8UC1);
		String xmlPath = conf.getString("xmlPath");
		if (!xmlPath.isEmpty()) {
			String fileName = indy.getPopulation().getFullIndividualFileName(
				xmlPath, indy.getIdxPrefix());
			try {
				Page page = PageXmlInputOutput.readPage(fileName);
				PageLayout layout = page.getLayout();
				for (Region region : layout.getRegionsSorted()) {
					processRegion(content, labels, region, 0);
				}
			} catch (UnsupportedFormatVersionException e) {
				Logger.getSingleton().error(
					"PAGE format error in " + fileName + ": " + e.getMessage());
			}
		} else { // Apply to whole image
			binarize(content, labels);
		}
		content.release();
		List<Individual> children = new LinkedList<Individual>();
		indy.setContent(labels);
		children.add(indy);
		return children;
	}
	
	/* (non-Javadoc)
	 * Recursive helper function
	 */
	private void processRegion(Mat content, Mat labels, Region region, int level) {
		if (region.hasRegions()) { // recursive descend
			int n = region.getRegionCount();
			for (int i = 0; i < n; i++) {
				processRegion(content, labels, region.getRegion(i), level+1);
			}
		}
		if (conf.getInteger("minLevel") > level) {
			return;
		}
		Mat work = new Mat(content.size(), content.type());
		boolean norm = conf.getInteger("normalizeRegionChannels") > 0;
		if (conf.getInteger("extractTextObjects") > 0
				&& region.getType().getName().equals("TextRegion")) {
			TextRegion txtRegion = (TextRegion) region;
			for (LowLevelTextObject txtObj : txtRegion.getTextObjectsSorted()) {
				content.copyTo(work);
				Rect bBox = txtObj.getCoords().getBoundingBox();
				Mat subWork = work.submat(bBox.top, bBox.bottom, bBox.left, bBox.right);
				if (norm) {
					ExtentedImgproc.normalize(subWork, 0, 255);
				}
				binarize(subWork, labels.submat(
						bBox.top, bBox.bottom, bBox.left, bBox.right));
				subWork.release();
			}
		}
		if (conf.getInteger("extractRegions") > 0) {
			content.copyTo(work);
			Rect bBox = region.getCoords().getBoundingBox();
			Mat subWork = work.submat(bBox.top, bBox.bottom, bBox.left, bBox.right);
			if (norm) {
				ExtentedImgproc.normalize(subWork, 0, 255);
			}
			binarize(subWork, labels.submat(
					bBox.top, bBox.bottom, bBox.left, bBox.right));
			subWork.release();
		}
		work.release();
	}
	
	/* (non-Javadoc)
	 * Binarization
	 */
	private void binarize(Mat img, Mat labels) {
		Mat greyscale = new Mat(img.size(), CvType.CV_8UC1);
		Imgproc.cvtColor(img, greyscale, Imgproc.COLOR_RGB2GRAY);
		Imgproc.threshold(greyscale, labels, 0, 1, Imgproc.THRESH_OTSU
				+ Imgproc.THRESH_BINARY);
		Mat labelBackup = new Mat(labels.size(), labels.type());
		labels.copyTo(labelBackup);
		double blackDiscount = conf.getFloat("blackDiscount");
		double maxForeground = conf.getFloat("maxForegroundFraction");
		double size = labels.cols() * labels.rows();
		double sum = 0.0;
		do {
			labelBackup.copyTo(labels);
			kmeans(img, labels, conf.getInteger("maxIter"), blackDiscount);
			blackDiscount += 0.1;
			sum = Core.sumElems(labels).val[0];
		} while (blackDiscount < 1.0 && sum/size > maxForeground);
		greyscale.release();
		labelBackup.release();
	}

	/* (non-Javadoc) 
	 * k-means algorithm. Note that the opencv implementation is
	 * pretty stupid as it can only handle 2-dimensional data - we have to
	 * implement it by our own. Early cancellation if clusters do not change
	 * 
	 * @param data data points
	 * @param labels input/output labels
	 * @param maxIter maximum number of iteration
	 */
	private void kmeans(Mat data, Mat labels, int maxIter, double w1) {
		Mat invLabels = new Mat(labels.size(), labels.type());
		double w0 = 1.0 - w1;
		boolean changed = true;
		for (int iter = 0; changed && iter < maxIter; iter++) {
			// Mean update
			Core.bitwise_not(labels, invLabels);
			Scalar m1 = Core.mean(data, labels);
			Scalar m0 = Core.mean(data, invLabels);
			changed = false;
			// Label update
			for (int row = 0; row < data.rows(); row++) {
				for (int col = 0; col < data.cols(); col++) {
					double[] vals = data.get(row, col);
					int newVal = w0 * d2(m0.val, vals) < w1 * d2(m1.val, vals) 
							? 0 : 1;
					if (newVal != labels.get(row, col)[0]) {
						changed = true;
						labels.put(row, col, new byte[]{(byte) newVal});
					}
				}
			}
		}
		invLabels.release();
	}
	
	/* (non-Javadoc)
	 * 3 dim squared euclidean distance,
	 */
	private double d2(double[] a1, double[] a2) {
		double acc = 0.0;
		for (int i = 0; i < 3; i++) {
			double diff = a1[i] - a2[i];
			acc += diff*diff;
		}
		return acc;
	}
}
