package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.Geometry;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the blur operation.
 * 
 * @author Felix Stahlberg
 */
public class BlurOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public BlurOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("xSize", 5,
					"Kernel size in x direction.");
				addParameter("ySize", 5,
					"Kernel size in y direction.");
				addParameter("mode", "mean",
					"Blur mode. Available: mean, gaussian, median. For the "
					+ "median filter, -xSize is used for both dimensions.");
			}
			@Override
			protected String getDescription() {
				return "Blurs the images.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		Mat dst = new Mat(content.rows(), content.cols(), content.type());
		int xSize = conf.getInteger("xSize");
		int ySize = conf.getInteger("ySize");
		String mode = conf.getString("mode");
		if (mode.equals("mean")) {
			Imgproc.blur(content, dst, new Size(xSize, ySize),
				new Point(xSize/2, ySize/2), Imgproc.BORDER_REPLICATE);
			content.release();
		} else if (mode.equals("median")) {
			dst = Geometry.myMedianBlur(content, xSize, ySize);
		} else if (mode.equals("gaussian") || mode.equals("gauss")) {
			Imgproc.GaussianBlur(content, dst, new Size(xSize, ySize), 0, 0, 
					Imgproc.BORDER_REPLICATE);
			content.release();
		} else {
			dst = content;
		}
		Individual child = new Individual();
		child.setContent(dst);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		return children;
	}
}
