package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the col2graph operation.
 * 
 * @author Felix Stahlberg
 */
public class Col2graphOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public Col2graphOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("graphWidth", 100,
					"Width of the generated image.");
			}
			@Override
			protected String getDescription() {
				return "Converts the first column of the image to a graph.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		MinMaxLocResult minMax = Core.minMaxLoc(content);
		double max = minMax.maxVal;
		int width = conf.getInteger("graphWidth");
		int height = content.rows();
		Mat dst = Mat.zeros(height, width, CvType.CV_8UC1);
		for (int row = 0; row < height; row++) {
			Core.line(dst, new Point(0, row), new Point(
				content.get(row, 0)[0]/max*width, row), Scalar.all(255), 1);
		}
		Individual child = new Individual();
		child.setContent(dst);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		content.release();
		return children;
	}
}
