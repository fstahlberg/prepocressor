package qa.qcri.prepocressor.operations;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.primaresearch.dla.page.Page;
import org.primaresearch.dla.page.io.xml.PageXmlInputOutput;
import org.primaresearch.dla.page.layout.PageLayout;
import org.primaresearch.dla.page.layout.physical.Region;
import org.primaresearch.dla.page.layout.physical.text.LowLevelTextObject;
import org.primaresearch.dla.page.layout.physical.text.impl.TextRegion;
import org.primaresearch.io.UnsupportedFormatVersionException;
import org.primaresearch.maths.geometry.Polygon;
import org.primaresearch.maths.geometry.Rect;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the renderPageXmlTranscriptions operation.
 * 
 * @author Felix Stahlberg
 */
public class RenderPageXmlTranscriptionsOperation extends Operation {

	/* (non-Javadoc)
	 * An instance of this class specifies a text to be written at a certain
	 * position in the image. We do not use getter/setters here as this is a
	 * private class..
	 */
	private class TextInImage {
		public String txt;
		public Rect bBox;
		public Scalar color;
		public TextInImage(String txt, Rect bBox, Scalar color) {
			this.txt = txt;
			this.bBox = bBox;
			this.color = color;
		}
	}
	
	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public RenderPageXmlTranscriptionsOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("xmlPath", "%base%idx.xml",
					"Path to the xml files in PAGE format. The same place"
					+ "holders as in the global outputPath can be used.");
				addParameter("renderWithOpenCV", 0,
					"Set to 1 to use OpenCV's putText function for rendering "
					+ " the text. This is faster but does only support "
					+ "ASCII characters.");
				addParameter("minFontSize", 12,
					"Minimum font size. Only used if renderWithOpenCV is not set");
				addParameter("maxFontSize", 100,
					"Maximum font size. Only used if renderWithOpenCV is not set");
				addParameter("minLevel", 0,
					"Minimum level in layout of region to be extracted.");
				addParameter("fontFamily", "Arial",
					"Font Family. Only used if renderWithOpenCV is not set");
				addParameter("align", "center",
					"Text alignment, 'right', 'left', or 'center'");
				addParameter("bgColorEstimateBorder", 2,
					"Controls the way the background color for text areas is "
					+ "estimated. The color is the average of the pixel colors "
					+ "at the border of the text area. This is the thickness "
					+ "of that border.");
			}
			@Override
			protected String getDescription() {
				return "This operation scans a page xml file for text regions."
					+ " The text regions are written to the document images "
					+ "in the pipeline by inserting solid rectangles with the "
					+ "text of the xml files in it. For example, this can be "
					+ "used to generate a translated version of the document "
					+ "image after the text has been ocred and translated.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		String fileName = indy.getPopulation().getFullIndividualFileName(
				conf.getString("xmlPath"), indy.getIdxPrefix());
		Mat content = indy.getContent();
		try {
			List<TextInImage> textToWrite = new LinkedList<TextInImage>();
			Page page = PageXmlInputOutput.readPage(fileName);
			PageLayout layout = page.getLayout();
			for (Region region : layout.getRegionsSorted()) {
				addRegion(textToWrite, content, region, 0);
			}
			if (conf.getInteger("renderWithOpenCV") > 0) {
				writeTextsWithOpenCV(textToWrite, content);
			} else {
				writeTextsWithBufferedImage(textToWrite, content);
			}
		} catch (UnsupportedFormatVersionException e) {
			Logger.getSingleton().error("PAGE format error in " 
					+ fileName + ": " + e.getMessage());
			
		} catch (Exception e) {
			Logger.getSingleton().error("Could not read PAGE file " 
					+ fileName + ": " + e.getMessage());
		}
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		return children;
	}
	
	/* (non-Javadoc)
	 * Write text with Java's native graphics functions 
	 */
	private void writeTextsWithBufferedImage (List<TextInImage> textToWrite, Mat content) {
		MatOfByte bMat = new MatOfByte();
		Highgui.imencode(".png", content, bMat);
		byte[] bArray = bMat.toArray();
		InputStream in = new ByteArrayInputStream(bArray);
		BufferedImage img = null;
		int minSize = conf.getInteger("minFontSize");
		int maxSize = conf.getInteger("maxFontSize");
		String family = conf.getString("fontFamily");
		try {
			img = ImageIO.read(in);
			Graphics g = img.getGraphics();
			for (TextInImage txt : textToWrite) {
				g.setColor(new Color((float) txt.color.val[2]/255.0f,
					(float) txt.color.val[1]/255.0f, (float) txt.color.val[0]/255.0f));
				int size = minSize;
				Rectangle2D strRect = null;
				FontMetrics m = null;
				do {
					size += 2;
					g.setFont(new Font(family, Font.PLAIN, size));
					m = g.getFontMetrics();
					strRect = m.getStringBounds(txt.txt, g);
				} while (strRect.getWidth() < txt.bBox.getWidth() 
						&& m.getAscent() + m.getDescent() < txt.bBox.getHeight()
						&& size <= maxSize);
				g.setFont(new Font(family, Font.PLAIN, size - 2));
				m = g.getFontMetrics();
				strRect = m.getStringBounds(txt.txt, g);
				g.drawString(txt.txt,
					txt.bBox.left + getLeftPadding(txt.bBox.getWidth(),
						(int) strRect.getWidth()),
					txt.bBox.bottom - m.getDescent());
			}
			content.put(0, 0,
				((DataBufferByte) img.getRaster().getDataBuffer()).getData());
		} catch (IOException e) {
			Logger.getSingleton().warn("Error during internal PNG encoding: "
				+ e.getMessage());
		}
	}
	
	/* (non-Javadoc)
	 * Write text with OpenCV's putText. Does only support ASCII.
	 */
	private void writeTextsWithOpenCV (List<TextInImage> textToWrite, Mat content) {
		for (TextInImage txt : textToWrite) {
			double scale = getBestFontScale(txt.txt,
					txt.bBox.getWidth(), txt.bBox.getHeight());
			Size txtSize = Core.getTextSize(txt.txt,
					Core.FONT_HERSHEY_SIMPLEX, scale, 1, null);
			
			Core.putText(content, txt.txt, new Point(
				txt.bBox.left + 
					getLeftPadding(txt.bBox.getWidth(), (int) txtSize.width),
				txt.bBox.bottom - (txt.bBox.getHeight() - txtSize.height) / 2.0),
				Core.FONT_HERSHEY_SIMPLEX, scale, txt.color);
		}
	}
	
	/* (non-Javadoc)
	 * Get the number of left padding pixels according the -align parameter.
	 */
	private int getLeftPadding(int boxWidth, int txtWidth) {
		String align = conf.getString("align");
		if (align.equals("center")) {
			return (boxWidth - txtWidth)/2;
		} else if (align.equals("right")) {
			return boxWidth - txtWidth;
		}
		return 0;
	}
	
	/* (non-Javadoc)
	 * Recursive helper function for traversing xml file
	 */
	private void addRegion(List<TextInImage> textToWrite, Mat content, 
			Region region, int level) {
		if (region.hasRegions()) { // recursive descend
			int n = region.getRegionCount();
			for (int i = 0; i < n; i++) {
				addRegion(textToWrite, content, region.getRegion(i), level+1);
			}
		}
		if (conf.getInteger("minLevel") > level) {
			return;
		}
		Logger log = Logger.getSingleton();
		if (region.getType().getName().equals("TextRegion")) {
			TextRegion txtRegion = (TextRegion) region;
			List<LowLevelTextObject> objs = txtRegion.getTextObjectsSorted();
			if (objs.isEmpty()) {
				String transcript = txtRegion.getText();
				writeText(textToWrite, content, txtRegion.getCoords(), transcript);
				log.debug("Write region transcription '" + transcript + "'");
			}
			for (LowLevelTextObject txtObj : objs) {
				String transcript = txtObj.getText();
				writeText(textToWrite, content, txtObj.getCoords(), transcript);
				log.debug("Write transcription '" + transcript + "'");
			}
		}
	}

	/* (non-Javadoc)
	 * Write text into image at specific coordinates
	 */
	private void writeText(List<TextInImage> textToWrite, Mat content, 
			Polygon coords, String transcript) {
		if (transcript == null) {
			return;
		}
		Rect bBox = coords.getBoundingBox();
		int border = conf.getInteger("bgColorEstimateBorder");
		Mat bg = content.submat(bBox.top, bBox.bottom, bBox.left, bBox.right);
		Mat mask = Mat.ones(bg.size(), CvType.CV_8UC1);
		if (bBox.bottom - bBox.top > 2*border 
				&& bBox.right - bBox.left > 2*border) {
			Mat cut = mask.submat(border, bg.rows() - border,
					border, bg.cols() - border);
			Mat zeros = Mat.zeros(cut.size(), CvType.CV_8UC1);
			zeros.copyTo(cut);
			zeros.release();
		}
		Scalar bgColor = Core.mean(bg, mask);
		mask.release();
		Core.rectangle(content, new Point(bBox.left, bBox.top),
			new Point(bBox.right, bBox.bottom), bgColor, Core.FILLED);
		if (!transcript.isEmpty()) {
			textToWrite.add(new TextInImage(transcript, bBox,
					new Scalar(255 - bgColor.val[0],
						255 - bgColor.val[1],
						255 - bgColor.val[2])));
		}
	}
	
	/* (non-Javadoc)
	 * Get largest font size such that the text still fits in the given 
	 * bounding box
	 */
	private double getBestFontScale(String txt, int width, int height) {
		double scale = 0.0;
		for (double incr = 1.0; incr > 0.011; incr /= 10.0) {
			Size size = new Size();
			int i = 0;
			do {
				scale += incr;
				size = Core.getTextSize(txt,
						Core.FONT_HERSHEY_SIMPLEX, scale, 1, null);
			} while (size.width <= width && size.height <= height
					&& i++ < 10);
			scale -= incr;
		}
		return Math.max(0.1, scale);
	}
}
