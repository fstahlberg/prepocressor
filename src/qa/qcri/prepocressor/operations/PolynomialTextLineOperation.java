package qa.qcri.prepocressor.operations;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoint;
import org.opencv.core.Core;
import org.opencv.core.Point;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the polynomialTextLine operation.
 * 
 * @author Felix Stahlberg
 */
public class PolynomialTextLineOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public PolynomialTextLineOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("dataPoints", "minima",
					"Strategy for data point retrival.\n"
					+ "'bbq': Use all bottom foregound points (see bbq op)\n"
					+ "'minima': Use only minima of bottom foreground points");
				addParameter("threshold", 0.5f,
					"Threshold for detecting the lowest point");
				addParameter("order", 4,
					"Order of the polynom");
				addParameter("flatBorders", 1,
					"If this is set to 1, the baseline left and right from all "
					+ "data points is set to a straight horizontal line at "
					+ "the height of the curve at the last data point. This "
					+ "usually reduces artefacts due to large slopes in the "
					+ "curves outside the data point range.");
				addParameter("minDataPoints", 2,
					"Minimum number of data points.");
				addParameter("outlierFactor", -1.0f,
					"Remove data points this factor times stdDev from median. Set"
					+ " to negative value to disable outlier detection");
				addParameter("minHeight", 1,
					"Minimum distance to top border for a polynom in order to "
					+ "be used for estimating the polynomial");
				addParameter("operation", "align",
					"This parameter decides what is passed through the pipeline."
					+ " Available values are:\n+\n"
					+ "* 'none': Leave the images as they are.\n"
					+ "* 'draw': Draw upper and lower baselines.\n"
					+ "* 'align': Create a new image where the baseline is "
					+ "horizontal and at image height/2\n-");
			}
			@Override
			protected String getDescription() {
				return "Fits a polynom to the data points in order to guess "
					+ "the text line.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		double eps = conf.getFloat("threshold");
		int minHeight = conf.getInteger("minHeight");
		int order = conf.getInteger("order");
		boolean onlyMins = conf.getString("dataPoints").equals("minima");
		List<WeightedObservedPoint> initPoints = 
				new ArrayList<WeightedObservedPoint>();
		initPoints.add(new WeightedObservedPoint(1.0, -1, 0));
		for (int col = 0; col < content.cols(); col++) {
			for (int row = content.rows() - 1; row >= minHeight; row--) {
				if (content.get(row, col)[0] >= eps) {
					initPoints.add(new WeightedObservedPoint(1.0, col, row));
					break;
				}
			}
		}
		initPoints.add(new WeightedObservedPoint(1.0, content.cols(), 0));
		
		List<WeightedObservedPoint> estPoints = 
				new LinkedList<WeightedObservedPoint>();
		int n = initPoints.size() - 1;
		for (int idx = 1; idx < n; idx++) {
			double y = initPoints.get(idx).getY();
			if (!onlyMins || (initPoints.get(idx-1).getY() <= y 
					&& initPoints.get(idx+1).getY() <= y)) {
				estPoints.add(initPoints.get(idx));
			}
		}
		estPoints = removeOutliers(conf.getFloat("outlierFactor"), estPoints);
		List<Individual> children = new LinkedList<Individual>();
		if (estPoints.size() < conf.getInteger("minDataPoints")) {
			Logger.getSingleton().error("Not enough estimation points! Pass thru "
				+ "original");
			children.add(indy);
			return children;
		}
		if (estPoints.size() <= order) {
			order = estPoints.size() - 1;
			Logger.getSingleton().warn("Decreasing order of polynomial to "
				+ order);
		}
		PolynomialCurveFitter fitter = PolynomialCurveFitter.create(order);
		final double[] coeff = fitter.fit(estPoints);
		String op = conf.getString("operation");
		if (op.equals("draw")) {
			draw(content, coeff, estPoints);
			children.add(indy);
		} else {
			int minCol = 0;
			int maxCol = content.cols();
			if (conf.getInteger("flatBorders") > 0) {
				minCol = (int) estPoints.get(0).getX();
				maxCol = (int) estPoints.get(estPoints.size() - 1).getX();
			}
			Mat dst = align(content, coeff, minCol, maxCol);
			content.release();
			Individual child = new Individual();
			child.setContent(dst);
			children.add(child);
		}
		return children;
	}
	
	/**
	 * Simple outlier detection with mean and stdDev.
	 * 
	 * @param tolerance outlier tolerance
	 * @param points data points with outliers
	 * @return data points without outliers
	 */
	static public List<WeightedObservedPoint> removeOutliers(double tolerance,
			List<WeightedObservedPoint> points) {
		double n = points.size();
		if (tolerance <= 0.0 || n == 0) {
			return points;
		}
		double sum = 0.0;
		for (WeightedObservedPoint p : points) {
			sum += p.getY();
		}
		double mean = sum/n;
		sum = 0.0;
		for (WeightedObservedPoint p : points) {
			double d = mean - p.getY();
			sum += d*d;
		}
		double stdDev = Math.sqrt(sum/n);
		List<WeightedObservedPoint> filtPoints = 
			new LinkedList<WeightedObservedPoint>();
		double maxDist = stdDev * tolerance;
		for (WeightedObservedPoint p : points) {
			if (Math.abs(mean - p.getY()) <= maxDist) {
				filtPoints.add(p);
			}
		}
		return filtPoints;
	}
	
	/**
	 * Draws a polynomial baseline into the image. The baseline is
	 * drawn in-place.
	 * 
	 * @param content image
	 * @param coeff polynomial coefficients
	 * @param estPoints data points
	 */
	public static void draw(Mat content, double[] coeff,
			List<WeightedObservedPoint> estPoints) {
		for (int col = 0; col < content.cols(); col++) {
			int row = (int) eval(coeff, col);
			if (row >= 0 && row < content.rows()) {
				content.put(row, col, new byte[]{100});
			}
		}
		for (WeightedObservedPoint p : estPoints) {
			Core.circle(content, new Point(p.getX(), p.getY()), 5,
				Scalar.all(100));
		}
	}
	
	/**
	 * Align picture for rectifying the polynomial baseline.
	 * 
	 * @param content image
	 * @param coeff polynomial coefficients
	 * @param minCol assume horizontal baseline left of minCol
	 * @param maxCol assume horizontal baseline right of maxCol
	 * @return new image with rectified baseline
	 */
	public static Mat align(Mat content, double[] coeff, 
			int minCol, int maxCol) {
		// Get dst dimensions
		int h = content.rows();
		int w = content.cols();
		int blHeight = h/2;
		for (int col = minCol; col < maxCol; col++) {
			int row =  (int) eval(coeff, col);
			blHeight = Math.max(Math.max(blHeight, row), h - row);
		}
		blHeight = Math.min(blHeight, h);
		Mat dst = Mat.zeros(blHeight*2, w, content.type());
		int minColShift = blHeight - ((int) eval(coeff, minCol));
		int maxColShift = blHeight - ((int) eval(coeff, maxCol));
		for (int col = 0; col < w; col++) {
			int shift = col < minCol ? minColShift
				: col > maxCol ? maxColShift : (blHeight - ((int) eval(coeff, col)));
			int shiftHeight = Math.min(h, blHeight*2-shift);
			if (shiftHeight > 0 && shift >= 0) {
				content.submat(0, shiftHeight, col, col+1).copyTo(
						dst.submat(shift, shift+shiftHeight, col, col+1));
			}
		}
		return dst;
	}
	
	/**
	 * Evaluate polynom
	 * 
	 * @param coeff coefficients
	 * @param x value
	 * @return polynom(x)
	 */
	public static double eval(double[] coeff, double x) {
		double acc = 0.0;
		double fact = 1.0;
		for (double c : coeff) {
			acc += fact*c;
			fact *= x;
		}
		return acc;
	}
}
