package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Size;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.Geometry;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the reducedAlcmTransformOperation operation.
 * 
 * @author Felix Stahlberg
 */
public class ReducedAlcmTransformOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public ReducedAlcmTransformOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("resolution", 2,
					"See operation description.");
				addParameter("kWidth", 30,
						"Width of the ellipse kernel.");
				addParameter("kHeight", 6,
						"Height of the ellipse kernel.");
			}
			@Override
			protected String getDescription() {
				return "Applies a steerable ellipsoid filter to create an "
					+ "adaptive local connectivity map. The ALCM of each "
					+ "direction is reduced horizontally to a single col. The "
					+ "i-th col of the resulting image corresponds to the "
					+ "direction i. i encodes the angle 180*i/resolution. If "
					+ "resolution=2, i=0 is the horizontal, i=1 the vertical "
					+ "ALCM. The resulting image has the width -resolution";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		Individual child = new Individual();
		/*Mat redALCM = Geometry.reducedALCM(content,
			new Size(conf.getInteger("kWidth"), conf.getInteger("kHeight")),
			conf.getInteger("resolution"));*/
		Mat redALCM = Geometry.reducedRunlength(content,
				conf.getInteger("resolution"));
		Mat zeros = Mat.zeros(new Size(140, redALCM.height()), redALCM.type());
		zeros.copyTo(redALCM.submat(0, redALCM.rows(), 20, 160));
		child.setContent(redALCM);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		content.release();
		return children;
	}
}
