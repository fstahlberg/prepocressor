package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the extend operation.
 * 
 * @author Felix Stahlberg
 */
public class ExtendOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public ExtendOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("top", 20,
					"Extend image at top border (in pixels).");
				addParameter("left", 20,
					"Extend image at left border (in pixels).");
				addParameter("right", 20,
					"Extend image at right border (in pixels).");
				addParameter("bottom", 20,
					"Extend image at bottom border (in pixels).");
			}
			@Override
			protected String getDescription() {
				return "Extend image canvas.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		int top = conf.getInteger("top");
		int left = conf.getInteger("left");
		int right = conf.getInteger("right");
		int bottom = conf.getInteger("bottom");
		Mat withBorder = Mat.zeros(
			content.rows()+top+bottom, content.cols()+left+right, content.type());
		Imgproc.copyMakeBorder(content, withBorder,
			top, bottom, left, right, Imgproc.BORDER_CONSTANT);
		Individual child = new Individual();
		child.setContent(withBorder);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		content.release();
		return children;
	}
}
