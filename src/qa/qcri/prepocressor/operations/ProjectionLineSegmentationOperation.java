package qa.qcri.prepocressor.operations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the projectionLineSegmentation operation.
 * 
 * @author Felix Stahlberg
 */
public class ProjectionLineSegmentationOperation extends Operation {

	final float EPS = 2.0f;
	
	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public ProjectionLineSegmentationOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("minLineCount", 10000,
					"Minimal line count in one segment. This can be used for "
					+ "outlier detection. Set to a high value to disable this "
					+ "feature.");
				addParameter("minLineHeight", 10,
						"Minimal line height in pixel.");
				addParameter("maxLineHeight", 130,
						"Maximum line height in pixel.");
				addParameter("maxLineHeightVariance", 2.0f,
						"If a line is taller than this parameter times the "
						+ "estimated average line height -> outlier.");
				addParameter("maxProjectionRatio", 0.8f,
						"If the minimum next to a maximum is larger than this "
						+ "parameter times the maximum in the projection, "
						+ "ignore this maximum.");
				addParameter("sinusExp", 1,
						"Sinus exponent used for line height estimation. "
						+ "Should be uneven.");
				addParameter("analysisMode", 0,
						"Set to 1 to output an image explaining the line "
						+ "segmentation by showing the smoothed vertical "
						+ "projection over the image plus the found boundaries");
			}
			@Override
			protected String getDescription() {
				return "Line segmentation using vertical projection. This "
					+ "operation first fits a sinus function to the profile. The "
					+ "frequency of the best fit is then used to determine the "
					+ "kernel size for a blur operation on the projection profile."
					+ "Lines are extracted from valley to valley in the smoothed "
					+ "profile.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		// TODO: Refactor!
		float outlierLineHeightThreshold = conf.getFloat("maxLineHeightVariance");
		Mat content = indy.getContent();
		Mat reduced = new Mat(content.rows(), 1, CvType.CV_32FC1);
		Mat reducedBlur = new Mat(content.rows(), 1, CvType.CV_32FC1);
		Core.reduce(content, reduced, 1, Core.REDUCE_SUM, CvType.CV_32FC1);
		normalizeProfile(reduced);
		int bestFreq = getBestFrequency(reduced);
		Imgproc.blur(reduced, reducedBlur, new Size(1, bestFreq/2),
				new Point(0, bestFreq/4), Imgproc.BORDER_CONSTANT);
		
		List<Integer> maxRows = new ArrayList<Integer>();
		List<Float> maxVals = new ArrayList<Float>();
		List<Float> minVals = new ArrayList<Float>();
		float[] vals = new float[reduced.rows()];
		reduced.get(0, 0, vals);
		buildMinMaxList(reduced, reducedBlur, maxRows, maxVals, minVals);
		removeFlatMaxima(maxRows, maxVals, minVals);
		List<Individual> children = new LinkedList<Individual>();
		maxRows.add(reduced.rows()); // Guardian element
		int fromRow = 0;
		int lastLastMinRow = -1;
		int lastFirstMinRow = -1;
		for (Integer toRow : maxRows) { // Extract lines
			float minVal = Float.MAX_VALUE;
			int firstMinRow = fromRow;
			int lastMinRow = fromRow;
			for (int row = fromRow; row < toRow; row++) {
				if (vals[row] < minVal - EPS) {
					minVal = vals[row];
					firstMinRow = row;
					lastMinRow = row;
				} else if (vals[row] < minVal + EPS) {
					minVal = Math.min(minVal, vals[row]);
					lastMinRow = row;
				}
			}
			if (lastLastMinRow >= 0) {
				int childFromRow = (lastFirstMinRow + lastLastMinRow) / 2;
				int childToRow = (firstMinRow + lastMinRow) / 2;
				if (childToRow - childFromRow > outlierLineHeightThreshold*bestFreq) {
					int add = Math.max(0, (bestFreq - (firstMinRow-lastLastMinRow))/2);
					childFromRow = Math.max(lastFirstMinRow, lastLastMinRow-add);
					childToRow = Math.min(lastMinRow, firstMinRow+add);
				}
				children.add(indy.produceChild(new Rect(
					new Point(0, childFromRow),
					new Point(content.cols(), childToRow))));
			}
			lastFirstMinRow = firstMinRow;
			lastLastMinRow = lastMinRow;
			fromRow = toRow;
		}
		if (conf.getInteger("analysisMode") > 0) {
			MinMaxLocResult minMax = Core.minMaxLoc(reduced);
			double max = minMax.maxVal;
			int width = content.cols()/4;
			int height = content.rows();
			// Draw projection profile
			for (int row = 0; row < height; row++) {
				Core.line(content, new Point(0, row), new Point(
					reduced.get(row, 0)[0]/max*width, row), Scalar.all(180), 1);
			}
			// Draw segments
			for (Individual child : children) {
				Rect rect = child.getParentRect();
				Core.rectangle(content, new Point(rect.x, rect.y),
						new Point(rect.x + rect.width, rect.y + rect.height), Scalar.all(255));
				Core.rectangle(content, new Point(rect.x+1, rect.y+1),
						new Point(rect.x + rect.width-1, rect.y + rect.height-1), Scalar.all(125));
				Core.line(content, new Point(rect.x, rect.y),
						new Point(rect.x + rect.width, rect.y + rect.height), Scalar.all(125));
				Core.line(content, new Point(rect.x + rect.width, rect.y),
						new Point(rect.x, rect.y + rect.height), Scalar.all(125));
			}
			// Draw maxs
			for (Integer row : maxRows) {
				Core.line(content, new Point(0, row),
					new Point(width, row), Scalar.all(75));
			}
			List<Individual> anaChildren = new LinkedList<Individual>();
			Individual child = new Individual();
			child.setContent(content);
			anaChildren.add(child);
			return anaChildren;
		}
		return children;
	}
	
	/* (non-Javadoc)
	 * Get sinus frequency with least squares method
	 */
	private int getBestFrequency(Mat reduced) {
		int minLineHeight = conf.getInteger("minLineHeight");
		int maxLineHeight = conf.getInteger("maxLineHeight");;
		int exp = conf.getInteger("sinusExp");
		Mat norm = new Mat(reduced.rows(), reduced.cols(), CvType.CV_32FC1);
		Core.normalize(reduced, norm, -1.0, 1.0, Core.NORM_MINMAX);
		int nData = reduced.rows();
		double data[] = new double[nData];
		for (int i = 0; i < data.length; i++) {
			data[i] = norm.get(i, 0)[0];
		}
		int bestFreq = 0;
		int bestShift = 0;
		double bestErr = Double.POSITIVE_INFINITY;
		for (int freq = minLineHeight; freq < maxLineHeight; freq++) {
			for (int shift = 0; shift < freq; shift++) {
				double err = 0.0;
				double sinFact = 2*Math.PI/freq;
				for (int k = 0; k < nData; k++) {
					double tmp = Math.pow(Math.sin((k-shift)*sinFact), exp) - data[k];
					err += tmp*tmp;
				}
				if (err < bestErr) {
					bestErr = err;
					bestFreq = freq;
					bestShift = shift;
				}
			}
		}
		Logger.getSingleton().debug("best frequence: " + bestFreq);
		Logger.getSingleton().debug("best shift: " + bestShift);
		norm.release();
		return bestFreq;
	}
	
	/* (non-Javadoc)
	 * Renormalize if we have too many maxima
	 */
	private void normalizeProfile(Mat reduced) {
		int minLineCount = conf.getInteger("minLineCount");
		float[] vals = new float[reduced.rows()];
		reduced.get(0, 0, vals);
		ArrayList<Float> maxList = new ArrayList<Float>();
		for (int i = 1; i < vals.length - 1; i++) {
			if (vals[i-1] < vals[i] && vals[i+1] < vals[i]) {
				maxList.add(vals[i]);
			}
		}
		Collections.sort(maxList);
		if (maxList.size() > minLineCount) {
			double maxVal = maxList.get(maxList.size() - minLineCount);
			for (int row = 0; row < reduced.rows(); row++) {
				if (reduced.get(row, 0)[0] > maxVal) {
					reduced.put(row, 0, new double[]{maxVal});
				}
			}
		}
	}
	
	/* (non-Javadoc)
	 * Builds three lists describing the blurred projection profile:
	 * maxRows is the row number of the i-th maximum, maxVal the value at this
	 * position, and minVals the values at valleys
	 */
	void buildMinMaxList(Mat reduced, Mat reducedBlur,
			List<Integer> maxRows, List<Float> maxVals, List<Float> minVals) {
		float[] vals = new float[reduced.rows()];
		float[] valsBlur = new float[reduced.rows()];
		reducedBlur.get(0, 0, valsBlur);
		reduced.get(0, 0, vals);
		boolean collectMins = true;
		float curMin = vals[0], curMaxVal = 0.0f;
		int curMaxRow = 0;
		for (int row = 1; row < valsBlur.length - 1; row++) {
			if (valsBlur[row-1] > valsBlur[row] && valsBlur[row+1] >= valsBlur[row]) { // min
				if (collectMins) {
					curMin = Math.min(curMin, valsBlur[row]);
				} else { // Collect maxs
					maxRows.add(curMaxRow);
					maxVals.add(curMaxVal);
					curMin = valsBlur[row];
					collectMins = true; // Start collecting mins
				}
			} else if (valsBlur[row-1] < valsBlur[row] 
					&& valsBlur[row+1] <= valsBlur[row]) { // max
				if (!collectMins) {
					if (valsBlur[row] > curMaxVal) {
						curMaxVal = valsBlur[row];
						curMaxRow = row;
					}
				} else { // Collect mins
					minVals.add(curMin);
					curMaxVal = valsBlur[row];
					curMaxRow = row;
					collectMins = false; // start collecting maxs
				}
			}
		}
		if (!collectMins) {
			maxRows.add(curMaxRow);
			maxVals.add(curMaxVal);
			minVals.add(vals[vals.length-1]);
		}
	}
	
	/* (non-Javadoc)
	 * Remove flat maxima by identifying indexes with small difference in 
	 * a minVal - maxVal - minVal sequence. Removal is done iterativly, starting
	 * with the flattest maximum
	 */
	void removeFlatMaxima(List<Integer> maxRows, List<Float> maxVals,
			List<Float> minVals) {
		float maxRatio = conf.getFloat("maxProjectionRatio");
		float worstRatio = 0.0f;
		do {
			worstRatio = 0.0f;
			int worstIndex = -1;
			int limit = Math.min(minVals.size() - 1, maxVals.size());
			for (int i = 0; i < limit; i++) {
				float ratio = Math.max(minVals.get(i), minVals.get(i + 1))
						/ maxVals.get(i);
				if (ratio > worstRatio) {
					worstRatio = ratio;
					worstIndex = i;
				}
			}
			if (worstRatio > maxRatio) {
				maxRows.remove(worstIndex);
				maxVals.remove(worstIndex);
				minVals.set(
						worstIndex + 1,
						Math.min(minVals.get(worstIndex),
								minVals.get(worstIndex + 1)));
				minVals.remove(worstIndex);
			}
		} while (worstRatio > maxRatio);
	}
}
