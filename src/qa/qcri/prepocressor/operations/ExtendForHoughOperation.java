package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the extendForHough operation.
 * 
 * @author Felix Stahlberg
 */
public class ExtendForHoughOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public ExtendForHoughOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("maxAngle", 15.0f,
					"Maximum skew angle in degree.");
				addParameter("extendHorizontally", 1,
					"Set to 1 to extend in horizontal direction.");
				addParameter("extendVertically", 1,
					"Set to 1 to extend in vertical direction.");
			}
			@Override
			protected String getDescription() {
				return "Extend image canvas such that the Hough transform "
					+ "catches all borders";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		double maxSkew = Math.PI * conf.getFloat("maxAngle") / 180.0;
		int borderVert = conf.getInteger("extendVertically") == 0 ? 0
				: (int) (1.0 + Math.sin(maxSkew) * content.width());
		int borderHoriz = conf.getInteger("extendHorizontally") == 0 ? 0
				: (int) (1.0 + Math.sin(maxSkew) * content.height());
		Logger.getSingleton().debug("Extend " + indy.getPopulation()
			.getInputFileName() + " vert:" + borderVert
			+ " horiz:" + borderHoriz);
		Mat withBorder = Mat.zeros(content.rows()+2*borderVert,
				content.cols()+2*borderHoriz, content.type());
		Imgproc.copyMakeBorder(content, withBorder,
			borderVert, borderVert,
			borderHoriz, borderHoriz, Imgproc.BORDER_CONSTANT);
		Individual child = new Individual();
		child.setContent(withBorder);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		content.release();
		return children;
	}
}
