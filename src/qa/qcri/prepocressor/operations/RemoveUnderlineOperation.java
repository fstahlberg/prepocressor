package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the removeUnderline operation.
 * 
 * @author Felix Stahlberg
 */
public class RemoveUnderlineOperation extends Operation {
	
	private int maxThickness, maxVariation;
	private float thicknessFactor;

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public RemoveUnderlineOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("foregroundThreshold", 0.5f,
					"Threshold for foreground");
				addParameter("thicknessFactor", 1.5f,
					"Underlines are removed up to a thickness of "
					+ "thicknessFactor*median thickness of this segment.");
				addParameter("maxRelHeight", 0.6f,
					"Minimum height of the underline relative to the image height");
				addParameter("maxHeight", 10000,
					"Minimum distance from underline to image bottom in pixel");
				addParameter("maxThickness", 6,
					"Maximum thickness of underline in pixels.");
				addParameter("minWidth", 20,
					"Minimum width of connected underline in pixels.");
				addParameter("minRelWidth", 0.0f,
					"Minimum width of connected underline relative to image width.");
				addParameter("maxVariation", 6,
					"Minimum width of connected underline in pixels.");
			}
			@Override
			protected String getDescription() {
				return "Remove underlines in text line images based on "
					+ "bottom point analysis: Record lowest foreground point ,"
					+ "look for straight lines, estimate line thickness with "
					+ "median height from segments just above a straight line, "
					+ "override with black.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		maxThickness = conf.getInteger("maxThickness");
		int minWidth = Math.max(conf.getInteger("minWidth"),
			(int) (conf.getFloat("minRelWidth") * content.cols()));
		int maxHeight = Math.min(conf.getInteger("maxHeight"),
			(int) (conf.getFloat("maxRelHeight") * content.rows()));
		if (minWidth < 2) {
			Logger.getSingleton().warn("minWidth is too low. I set it to 5");
			minWidth = 5;
		}
		maxVariation = conf.getInteger("maxVariation");
		thicknessFactor = conf.getFloat("thicknessFactor");
		double eps = conf.getFloat("foregroundThreshold");
		int[] bottomPos = new int[content.cols()];
		int[] segHeight = new int[content.cols()];
		do {
			bottomPos = new int[content.cols()];
			segHeight = new int[content.cols()];
			for (int col = 0; col < content.cols(); col++) {
				for (int row = content.rows() - 1; row >= 0; row--) {
					if (content.get(row, col)[0] >= eps) {
						bottomPos[col] = row;
						do {
							row--;
						} while (row >= 0 && content.get(row, col)[0] >= eps);
						segHeight[col] = bottomPos[col] - row; 
						break;
					}
				}
			}
		} while(removeSingleUnderline(
				content, bottomPos, segHeight, minWidth, maxHeight));
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		return children;
	}
	
	/* (non-Javadoc)
	 * Remove a single connected underline in the image.
	 * 
	 * @return true if a underline was detected and removed, false otherwise
	 */
	private boolean removeSingleUnderline(Mat content, int[] bottomPos, 
			int[] segHeight, int minWidth, int maxHeight) {
		int bestStart = 0, bestEnd = 0;
		double bestThickness = 0;
		int bottomPosMin = content.rows() - maxHeight;
		for (int start = 0; start < bottomPos.length - minWidth; start++) {
			if (bottomPos[start] < bottomPosMin) {
				continue;
			}
			int end = start + 1;
			int min = bottomPos[start];
			int max = min;
			int thickness = segHeight[start];
			if (thickness > maxThickness) {
				continue;
			}
			for (; 
					end < bottomPos.length 
					&& max - min <= maxVariation
					&& segHeight[end] <= thickness*thicknessFactor; end++) {
				min = Math.min(min, bottomPos[end]);
				max = Math.max(max, bottomPos[end]);
			}
			if (bestEnd - bestStart < end - start) {
				bestStart = start;
				bestEnd = end;
				bestThickness = thickness;
			}
		}
		if (bestEnd - bestStart < minWidth) {
			return false;
		}
		Logger.getSingleton().debug("Underline detected from " + bestStart 
			+ " to " + bestEnd + " (thickness: " + bestThickness + ")");
		int maxSegHeight = (int) (thicknessFactor * bestThickness);
		for (int x = bestStart; x < bestEnd; x++) {
			int height = segHeight[x];
			if (height <= maxSegHeight) {
				for (int yOffset = 0; yOffset < height; yOffset++) {
					content.put(bottomPos[x] - yOffset, x, 0);
				}
			}
		}
		return true;
	}
}
