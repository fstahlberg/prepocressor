package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the threshold operation.
 * 
 * @author Felix Stahlberg
 */
public class ThresholdOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public ThresholdOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("threshold", 127f,
					"Binarization threshold (passed through to OpenCV).");
				addParameter("maxVal", 255,
					"Positive value after binarization (passed through to OpenCV).");
				addParameter("type", "BINARY,OTSU",
					"Thresholding type. See OpenCVs documentation for the "
					+ "threshold function. Connect options with ','. Available"
					+ " options are: BINARY, BINARY_INV, TRUNC, TOZERO, "
					+ "TOZERO_INV, OTSU");
			}
			@Override
			protected String getDescription() {
				return "The threshold command creates binary images. It is "
					+ "based on the OpenCV function threshold() and thus "
					+ "supports simple, adaptive, and otsu's thresholding.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		Imgproc.threshold(content, content, conf.getFloat("threshold"), 
			conf.getInteger("maxVal"), ThresholdOperation.getType(conf));
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		return children;
	}
	
	/**
	 * Get the OpenCV descriptor for thresholding given the type parameter
	 * in the given configuration.
	 * 
	 * @param conf configuration to fetch the type parameter from
	 * @return
	 */
	public static int getType(Configuration conf) {
		int type = 0;
		for (String option : conf.getString("type").split(",")) {
			if (option.equals("BINARY")) {
				type += Imgproc.THRESH_BINARY;
			} else if (option.equals("BINARY_INV")) {
				type += Imgproc.THRESH_BINARY_INV;
			} else if (option.equals("TRUNC")) {
				type += Imgproc.THRESH_TRUNC;
			} else if (option.equals("TOZERO")) {
				type += Imgproc.THRESH_TOZERO;
			} else if (option.equals("TOZERO_INV")) {
				type += Imgproc.THRESH_TOZERO_INV;
			} else if (option.equals("OTSU")) {
				type += Imgproc.THRESH_OTSU;
			}
		}
		return type;
	}
}
