package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.Geometry;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the rectSum operation.
 * 
 * @author Felix Stahlberg
 */
public class RectSumOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public RectSumOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("fixX", 0,
					"x coordinate of fix point");
				addParameter("fixY", 0,
						"y coordinate of fix point");
				addParameter("maxWidth", 0,
						"Largest rectangle width. Can also be negative. If it"
						+ " is set to 0, use imageWidth-fixX");
				addParameter("maxHeight", 0,
						"Largest rectangle height. Can also be negative. If it"
						+ " is set to 0, use imageHeight-fixY");
			}
			@Override
			protected String getDescription() {
				return "This operation calculates the sum of elements within "
					+ "rectangles in the images. The rectangles have a common "
					+ "edge point (fix point) but their width and height vary."
					+ " Produces a maxWidth times maxHeight matrix storing the"
					+ " sum within corresponding rectangles.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		int maxHeight = conf.getInteger("maxHeight");
		int maxWidth = conf.getInteger("maxWidth");
		int fixX = conf.getInteger("fixX");
		int fixY = conf.getInteger("fixY");
		if (maxHeight == 0) {
			maxHeight = content.rows() - fixY;
		}
		if (maxWidth == 0) {
			maxWidth = content.cols() - fixX;
		}
		Mat dst = Geometry.rectSum(content, fixY, fixX, maxHeight, maxWidth);
		Individual child = new Individual();
		child.setContent(dst);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		return children;
	}
}
