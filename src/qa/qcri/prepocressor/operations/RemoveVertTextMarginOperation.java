package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the removeVertTextMargin operation.
 * 
 * @author Felix Stahlberg
 */
public class RemoveVertTextMarginOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public RemoveVertTextMarginOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("minRowSum", 3f,
					"Rows with less than minPixelCount white pixels are marked"
					+ " as black.");
				addParameter("minTextHeight", 15,
						"Minimal text height in pixel.");
			}
			@Override
			protected String getDescription() {
				return "Removes black space on top and bottom of the image, "
					+ "assuming that it contains only one single text line.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		float minRowSum = conf.getFloat("minRowSum");
		Mat reduced = Mat.zeros(content.rows(), content.cols(), CvType.CV_32FC1);
		Core.reduce(content, reduced, 1, Core.REDUCE_SUM, CvType.CV_32FC1);
		// Find largest connected component
		float[] vals = new float[reduced.rows()];
		reduced.get(0, 0, vals);
		int bestFromRow = -1, bestToRow = -1;
		for (int fromRow = 0; fromRow < vals.length; fromRow++) {
			if (vals[fromRow] > minRowSum) {
				int toRow = fromRow + 1;
				for (; toRow < vals.length - 1; toRow++) {
					if (vals[toRow] < minRowSum) {
						break;
					}
				}
				if (toRow - fromRow > bestToRow - bestFromRow) {
					bestFromRow = fromRow;
					bestToRow = toRow;
				}
				fromRow = toRow + 1;
			}
		}
		List<Individual> children = new LinkedList<Individual>();
		if (bestToRow - bestFromRow < conf.getInteger("minTextHeight")) {
			Logger.getSingleton().debug("RemoveVertTextMargin could not "
				+ "detect text.");
		} else {
			Individual child = new Individual();
			child.setContent(content.submat(bestFromRow, bestToRow,
					0, content.cols()));
			children.add(child);
		}
		return children;
	}
}
