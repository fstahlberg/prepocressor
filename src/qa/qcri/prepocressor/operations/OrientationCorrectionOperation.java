package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.ExtentedImgproc;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the orientationCorrection operation.
 * 
 * @author Felix Stahlberg
 */
public class OrientationCorrectionOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public OrientationCorrectionOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("reloadOriginal", 0,
						"Reload the original image and rotate it. Otherwise "
						+ "use image in the pipeline.");
				addParameter("noCorrection", 0,
						"Pass thru image without modification.");
				addParameter("resolution", 90,
						"Resolution of the Hough transform.");
				addParameter("sobelKSize", 3,
						"Size of the sobel kernel.");
				addParameter("maxAngle", 45f,
						"Maximum skew angle.");
			}
			@Override
			protected String getDescription() {
				return "Brings rotated text documents in an upright position. "
					+ "This is done by finding the maximum squared variance "
					+ "angle in the Hough transformed image.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		int resolution = conf.getInteger("resolution");
		float maxAngle = conf.getFloat("maxAngle");
		double maxFraction = maxAngle / 180.0;
		Mat content = indy.getContent();
		Mat hough = ExtentedImgproc.axisAlignedHoughTransform(content.t(),
			Math.PI/2.0 - maxFraction*Math.PI,
			Math.PI/2.0 + maxFraction*Math.PI, resolution);
		Mat sobel = new Mat(content.rows(), content.cols(), content.type());
		Mat reduced = new Mat(content.rows(), content.cols(), content.type());
		Imgproc.Sobel(hough, sobel, -1, 1, 0,
				conf.getInteger("sobelKSize"), 1, 0, Imgproc.BORDER_REPLICATE);
		Mat sobel2 = sobel.mul(sobel);
		Core.reduce(sobel2, reduced, 1, Core.REDUCE_SUM);
		MinMaxLocResult minMax = Core.minMaxLoc(reduced);
		double degree = 
				360*(0.5-maxFraction + maxFraction*2 * minMax.maxLoc.y / (resolution-1))/2.0
				-90;
		Logger.getSingleton().debug("Skew for " 
			+ indy.getPopulation().getInputFileName() + " detected: " + degree);
		sobel.release();
		sobel2.release();
		hough.release();
		reduced.release();
		
		Individual child = new Individual();
		if (conf.getInteger("noCorrection") == 0) {
			Mat dst = new Mat(content.rows(), content.cols(), content.type());
			Mat r = Imgproc.getRotationMatrix2D(new Point(content.cols()/2,
					content.rows()/2), -degree, 1.0);
			Imgproc.warpAffine(
				conf.getInteger("reloadOriginal") == 0 ? content 
						: Highgui.imread(indy.getPopulation().getInputFileName()),
					dst, r, content.size());
			child.setContent(dst);
			content.release();
		} else {
			child.setContent(content);
		}
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		return children;
	}
}
