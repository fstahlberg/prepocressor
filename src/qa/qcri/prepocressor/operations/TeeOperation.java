package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the tee operation.
 * 
 * @author Felix Stahlberg
 */
public class TeeOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public TeeOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("outputPath", "tee-%base%idx%ext",
						"Path to the output files. See the -outputPath parameter of "
						+ "prepocressor for more information.");
				addParameter("normalize", 1,
						"Normalize between 0 and 255 before storing.");
			}
			@Override
			protected String getDescription() {
				return "tee writes the current set of images to the file system"
					+ " similarly to the linux command tee. The images in the "
					+ "pipeline remain unchanged.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		List<Individual> children = new LinkedList<Individual>();
		Individual storeIndy = new Individual();
		Mat storeMat = new Mat(content.size(), content.type());
		content.copyTo(storeMat);
		storeIndy.setContent(storeMat);
		if (conf.getInteger("normalize") > 0) {
			Operation norm = new NormalizeOperation("normalize");
			storeIndy = norm.processIndividual(storeIndy).get(0);
		}
		storeIndy.setPopulation(indy.getPopulation());
		storeIndy.setIdxPrefix(indy.getIdxPrefix());
		storeIndy.store(conf.getString("outputPath"));
		children.add(indy);
		storeMat.release();
		return children;
	}
}
