package qa.qcri.prepocressor.operations;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.ExtentedImgproc;
import qa.qcri.prepocressor.imageprocessing.Geometry;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the houghTextLine operation. This is the implementation
 * of (Stahlberg and Vogel, 2015). Note also that this operation expects a Hough 
 * transformed image as input
 * 
 * @author Felix Stahlberg
 */
public class HoughTextLineOperation extends Operation {

	private int resolution = 90;
	
	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public HoughTextLineOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("blurMode", "none",
					"Blur hough space. Available values are:\n"
					+ "'none', 'median', 'mean'.");
				addParameter("truPath", "",
					"If .tru files are available (as for the IFN/ENIT database"
					+ ") the baseline error can be computed automatically. Use"
					+ " placeholders as in the global -outputPath parameter. "
					+ "The evaluation is written to stdout. Format:\n"
					+ "EvalBaseline <inputFileName> <refStartY> <refSlope> "
					+ "<hypoStartY> <hypoSlope> <Error> <StringError>");
				addParameter("useTruIfAvailable", 1,
					"If this is set to 1, we take the reference baseline from "
					+ "the tru file if exists.");
				addParameter("blurRho", 3,
					"Kernel size of blur operation in rho direction");
				addParameter("blurTheta", 1,
					"Kernel size of blur operation in theta direction");
				addParameter("maxDegree", 20f,
					"Maximum text rotation in degree. If this rotation is "
					+ "exceeded, the image is discarded. Note: We only check"
					+ " the range betweeen +-45 degree.");
				addParameter("borderFactor", 0.0f,
					"Before Hough transform, the image is extended on top and "
					+ "bottom by borderFactor*height");
				addParameter("maxValArea", 0.5f,
					"The maximum within the band must be in the lower part of "
					+ "the band area. This restriction addresses the common "
					+ "assumption that the baseline is represented by a "
					+ "maximum in the Hough space. Set to negative value to"
					+ "disable this check. This is not used if criterion=max");
				addParameter("deltaLambda", 0.05f,
					"Lambda increment. Set to 0 to use only startLambda*.");
				addParameter("startLambdaBandWidth", 0.2f,
					"Required fraction of white pixels between both base lines"
					+ " for the bandWidth criterion.");
				addParameter("endLambdaBandWidth", 0.4f,
					"Required fraction of white pixels between both base lines"
					+ " for the bandWidth criterion.");
				addParameter("startLambdaBandMin", 0.2f,
					"Required fraction of white pixels between both base lines"
					+ " for the bandMin criterion.");
				addParameter("endLambdaBandMin", 0.4f,
					"Required fraction of white pixels between both base lines"
					+ " for the bandMin criterion.");
				addParameter("resolution", 130,
					"Number of steps between -45 and +45 degree in Hough "
					+ "transform.");
				addParameter("combination", "lowest",
					"How the different baselines should be combined. Available:\n"
					+ "'none': Do not combine baselines - Pass them separately\n"
					+ "'lowest': Select the baseline with the lowest slope.");
				addParameter("houghMax", 0,
					"Set to 1 to include the hough space maximum.");
				addParameter("deleteAboveAscenders", 0,
					"Set to 1 to remove pixels above the highest ascender. An "
					+ "ascender is a connected component which crosses the "
					+ "upper baseline. Ignored if the operation parameter is "
					+ "not set to 'align'");
				addParameter("deleteBelowDescenders", 0,
					"Set to 1 to remove pixels below the lowest descender. A "
					+ "descender is a connected component which crosses the "
					+ "lower baseline. Ignored if the operation parameter is "
					+ "not set to 'align'");
				addParameter("operation", "align",
					"This parameter decides what is passed through the pipeline."
					+ " Available values are:\n+\n"
					+ "* 'none': Leave the images as they are.\n"
					+ "* 'draw': Draw upper and lower baselines.\n"
					+ "* 'align': Create a new image where the baseline is "
						+ "horizontal and at image height/2\n-");
				addParameter("noTextLineOperation", "original",
					"This parameter decides what is passed through the pipeline "
					+ "if no text line within range has been detected:\n+\n"
					+ "* 'original': Pass through original image.\n"
					+ "* 'bottom': Place baseline at bottom border of image\n-");
			}
			@Override
			protected String getDescription() {
				return "Calculates the base line from a "
					+ "Hough transformed image. If -criterion=max, the base "
					+ "line is detected at the maximum in the Hough space. "
					+ "Otherwise, we find a rotated rectangle which includes at least "
					+ "minBetweenBaseline white pixels and optimizes the target"
					+ " function (-criterion parameter) and meets the -maxValArea"
					+ " restriction. The base line is detected at the bottom "
					+ "of the rectangle. See (Stahlberg and Vogel, 2015) for "
					+ "a detailed discussion.";
			}
		};
	}

	/* (non-Javadoc)
	 * Represents a strip through the image
	 */
	private class HoughBand {
		public int upperX, lowerX, y;
		public String description;
		public HoughBand(String description, int upperX, int lowerX, int y) {
			this.description = description;
			this.upperX = upperX;
			this.lowerX = lowerX;
			this.y = y;
		}
	}
	
	/* (non-Javadoc)
	 * Represents line in the image
	 */
	private class Line {
		public int startHeight;
		public double slope;
		public Line(int startHeight, double slope) {
			this.startHeight = startHeight;
			this.slope = slope;
		}
	}
	
	@Override
	protected List<Individual> processIndividual(Individual indy) {
		resolution = conf.getInteger("resolution");
		Mat contentDep = indy.getContent();
		Mat content = new Mat(
				contentDep.rows(), contentDep.cols(), contentDep.type());
		contentDep.copyTo(content);
		List<Individual> children = new LinkedList<Individual>();
		float borderFactor = conf.getFloat("borderFactor");
		int border = (int) (content.rows() * borderFactor);
		Rect allRect = new Rect(0, 0, content.cols(), content.rows());
		List<HoughBand> validBands = getValidBandsByTru(indy);
		if (validBands == null) {
			validBands = getValidBandsByHough(content);
		}
		if (validBands.isEmpty() 
				&& conf.getString("noTextLineOperation").equals("bottom")) {
			int defaultHeight = getLowestForegroundHeight(content);
			validBands.add(new HoughBand("bottom", defaultHeight, defaultHeight,
					(resolution-1)/2)); // degree=0
			Logger.getSingleton().warn("No text line within range found for "
				+ indy.getPopulation().getInputFileName() 
				+ " -> Place add bottom border.");
		}
		for (HoughBand band : validBands) {
			double degree = houghYCoord2Degree(band.y);
			eval(indy, band.lowerX - border, degree, band.description);
			Mat childContent = content;
			if (conf.getString("operation").equals("draw")) {
				childContent = draw(content, band.upperX - border,
						band.lowerX - border, degree);
			} else if (conf.getString("operation").equals("align")) {
				childContent = align(content, band.lowerX - border, degree);
				if (conf.getInteger("deleteAboveAscenders") > 0) {
					deleteAboveAscenders(childContent, band.lowerX - band.upperX);
				}
				if (conf.getInteger("deleteBelowDescenders") > 0) {
					deleteBelowDescenders(childContent);
				}
			}
			Individual child = indy.produceChild(allRect);
			child.setContent(childContent);
			children.add(child);
		}
		if (children.isEmpty()) { // Add at least original
			Logger.getSingleton().warn("No text line within range found for "
				+ indy.getPopulation().getInputFileName() + " -> pass through original.");
			Individual child = indy.produceChild(allRect);
			child.setContent(content);
			children.add(child);
		} else {
			if (!conf.getString("operation").equals("none")) {
				content.release();
				
			}
			contentDep.release();
		}
		return children;
	}
	
	private double houghYCoord2Degree(int bandY) {
		return 360*(0.25 + 0.5 * bandY / (resolution-1))/2.0 - 90;
	}
	
	private int degree2houghYCoord(double degree) {
		return (int) (2.0*(resolution-1)*(2.0*(degree+90.0)/360.0 -0.25));
	}

	/* (non-Javadoc)
	 * Get valid band corresponding to reference baseline in tru file
	 * or null
	 */
	private List<HoughBand> getValidBandsByTru(Individual indy) {
		Line refLine = getReferenceLine(indy);
		if (refLine == null) {
			return null;
		}
		List<HoughBand> validBands = new LinkedList<HoughBand>();
		double degree = 180.0*Math.atan(refLine.slope)/Math.PI;
		validBands.add(new HoughBand("Reference", refLine.startHeight,
			refLine.startHeight, degree2houghYCoord(degree)));
		return validBands;
	}
	
	/* (non-Javadoc)
	 * Get list of valid bands using Hough transform method
	 */
	private List<HoughBand> getValidBandsByHough(Mat content) {
		float borderFactor = conf.getFloat("borderFactor");
		int border = (int) (content.rows() * borderFactor);
		Mat withBorder = Mat.zeros(
			content.rows()+2*border, content.cols(), content.type());
		Imgproc.copyMakeBorder(content, withBorder,
			border, border, 0, 0, Imgproc.BORDER_CONSTANT);
		Mat withBorderTrans = withBorder.t();
		Mat hough = ExtentedImgproc.axisAlignedHoughTransform(withBorderTrans,
				Math.PI/4, 3*Math.PI/4, resolution);
		hough = blurHough(hough);
		LinkedList<HoughBand> bands = new LinkedList<HoughBand>();
		if (conf.getInteger("houghMax") > 0) {
			bands.add(getHoughMax(hough));
		}
		getBestBands(bands, content, hough);
		if (conf.getString("combination").equals("lowest")) {
			HoughBand lowestSlope = getLowestSlope(bands);
			bands.clear();
			bands.add(lowestSlope);
		}
		LinkedList<HoughBand> validBands = new LinkedList<HoughBand>();
		for (HoughBand band : bands) {
			double degree = 
				360*(0.25 + 0.5 * band.y / (resolution-1))/2.0
				-90;
			if (Math.abs(degree) <= conf.getFloat("maxDegree")) {
				validBands.add(band);
			}
		}
		withBorder.release();
		withBorderTrans.release();
		hough.release();
		return validBands;
	}
	
	/* (non-Javadoc)
	 * Get height of the lowest foreground pixel
	 */
	private int getLowestForegroundHeight(Mat img) {
		Mat reduced = Mat.zeros(img.rows(), 1, CvType.CV_32FC1);
		Core.reduce(img, reduced, 1, Core.REDUCE_SUM, CvType.CV_32FC1);
		int lowestHeight = img.rows() - 1;
		while (lowestHeight > 0 && reduced.get(lowestHeight, 0)[0] < 0.001) {
			lowestHeight--;
		}
		reduced.release();
		return lowestHeight;
	}
	
	/* (non-Javadoc)
	 * Delete all white pixels above highest ascender
	 */
	private void deleteAboveAscenders(Mat img, int bandWidth) {
		int h = img.rows()/2 - bandWidth;
		if (h < 1) {
			return;
		}
		Mat aboveBL = img.submat(0, h, 0, img.cols());
		List<MatOfPoint> contours = new LinkedList<MatOfPoint>();
		Mat working = new Mat(aboveBL.rows(), aboveBL.cols(), aboveBL.type());
		aboveBL.copyTo(working);
		Mat hierarchy = new Mat(aboveBL.rows(), aboveBL.cols(), aboveBL.type());
		Imgproc.findContours(working, contours, hierarchy, 
				Imgproc.RETR_EXTERNAL,
				Imgproc.CHAIN_APPROX_NONE);
		int highest = h;
		for (MatOfPoint contour : contours) {
			Rect rect = Imgproc.boundingRect(contour);
			if (rect.y + rect.height >= h-1) {
				highest = Math.min(highest, rect.y);
			}
		}
		if (highest > 0) {
			Core.rectangle(img, new Point(0, 0), new Point(img.cols(), highest),
				Scalar.all(0), Core.FILLED);
		}
		working.release();
		hierarchy.release();
	}
	
	/* (non-Javadoc)
	 * Delete all white pixels below lowest descender
	 */
	private void deleteBelowDescenders(Mat img) {
		Mat belowBL = img.submat(img.rows()/2, img.rows(), 0, img.cols());
		List<MatOfPoint> contours = new LinkedList<MatOfPoint>();
		Mat working = new Mat(belowBL.rows(), belowBL.cols(), belowBL.type());
		belowBL.copyTo(working);
		Mat hierarchy = new Mat(belowBL.rows(), belowBL.cols(), belowBL.type());
		Imgproc.findContours(working, contours, hierarchy, 
				Imgproc.RETR_EXTERNAL,
				Imgproc.CHAIN_APPROX_NONE);
		int lowest = 0;
		for (MatOfPoint contour : contours) {
			Rect rect = Imgproc.boundingRect(contour);
			if (rect.y <= 1) {
				lowest = Math.max(lowest, rect.y + rect.height);
			}
		}
		if (img.rows() - lowest > 0) {
			Core.rectangle(belowBL, new Point(0, lowest),
				new Point(img.cols(), belowBL.rows()), Scalar.all(0), Core.FILLED);
		}
		working.release();
		hierarchy.release();
	}

	/* (non-Javadoc)
	 * Get band with lowest slope
	 */
	private HoughBand getLowestSlope(List<HoughBand> bands) {
		HoughBand best = null;
		double bestDegree = 500.0;
		for (HoughBand band : bands) {
			double degree = Math.abs(houghYCoord2Degree(band.y));
			if (degree < bestDegree) {
				best = band;
				bestDegree = degree;
			}
		}
		return best;
	}
	
	/* (non-Javadoc)
	 * Get the reference line if any specified or return NULL
	 */
	private Line getReferenceLine(Individual indy) {
		int width = indy.getContent().cols();
		String truPath = conf.getString("truPath");
		if (truPath.isEmpty()) {
			return null;
		}
		File file = new File(indy.getPopulation().getFullIndividualFileName(
			truPath, indy.getIdxPrefix()));
		if (!file.exists()) {
			return null;
		}
		Logger log = Logger.getSingleton();
		int refStartHeight = -10000;
		double refSlope = -1;
		try {
			FileInputStream fstream = new FileInputStream(file);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			// Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				String[] split = strLine.trim().split(":");
				if (split.length == 2
						&& split[0].trim().equals("BLN")) {
					String[] nums = split[1].split(",");
					refStartHeight = Integer.parseInt(nums[0].trim());
					double refEndHeight = Double.parseDouble(nums[1].trim());
					refSlope = (refStartHeight - refEndHeight)/(width-1);
					break;
				}
			}
			in.close();
		} catch (Exception e) {// Catch exception if any
			log.warn("TRU file '" + file.getAbsolutePath() + "' error: "
					+ e.getMessage());
			return null;
		}
		if (refStartHeight == -10000) {
			return null;
		}
		return new Line(refStartHeight, refSlope);
	}
	
	/* (non-Javadoc)
	 * Evaluate the lower baseline. The ground truth is fetched from the *.tru
	 * files according -truPath parameter. For the tru format see the
	 * IFN/ENIT database.
	 */
	private void eval(Individual indy, int startHeight, double degree,
			String desc) {
		Line refLine = getReferenceLine(indy);
		if (refLine == null) {
			return;
		}
		int width = indy.getContent().cols();
		double slope = Math.sin(2*Math.PI*degree/360.0);
		Logger log = Logger.getSingleton();
		/*Mat content = indy.getContent();
		Point p1 = new Point(0, refStartHeight);
		Point p2 = new Point(content.cols(),
				refStartHeight - content.cols() * refSlope);
		Core.line(content, p1, p2, Scalar.all(170), 1);*/
		// Everything is alright, start accumulating baseline error
		double totalError = 0;
		for (int x = 0; x < width; x++) {
			totalError += Math.abs(
				(startHeight - slope * x)
				- (refLine.startHeight - refLine.slope * x));
		}
		double bErr = ((double) totalError)/width;
		log.notice("EvalBaseline:" + desc + " "
			+ indy.getPopulation().getInputFileName()
			+ " " + width + " " + refLine.startHeight + " " + refLine.slope
			+ " " + startHeight + " " + slope
			+ " " + bErr + " "
			+ (bErr<= 5.0 ? "excellent"
					: bErr <= 7.0 ? "acceptable" : "insufficient"));
	}
	
	/* (non-Javadoc)
	 * align operation.
	 */
	private Mat align(Mat content, int bestLowerBaselineX, double degree) {
		Point center = new Point(content.cols()/2, bestLowerBaselineX 
				- content.cols()/2 * Math.sin(2*Math.PI*degree/360.0));
		Rect rotationBounding = new RotatedRect(
			center,
			content.size(), degree).boundingRect();
		int newBaselineHeight = Math.max(bestLowerBaselineX,
			content.rows() - bestLowerBaselineX);
		newBaselineHeight += Math.abs(rotationBounding.y); // for top border
		if (rotationBounding.height > newBaselineHeight*2) { // for lower border
			newBaselineHeight = rotationBounding.height/2;
		}
		int xOffset = Math.abs(rotationBounding.x);
		Mat dst = Mat.zeros(newBaselineHeight*2,
				content.cols() + 2*xOffset, content.type());
		Mat r = Imgproc.getRotationMatrix2D(center, -degree, 1.0);
		// Translation along y axis into center
		r.put(1, 2, new double[]{
			r.get(1, 2)[0] +
			newBaselineHeight - center.y});
		// Add offset to avoid loosing white pixels at left or right border
		r.put(0, 2, new double[]{
			r.get(0, 2)[0] + xOffset});
		Imgproc.warpAffine(content, dst, r, dst.size());
		return dst;
	}
	
	/* (non-Javadoc)
	 * draw operation.
	 */
	private Mat draw(Mat content, int bestUpperBaselineX, int bestLowerBaselineX,
			double degree) {
		Mat dst = new Mat(content.rows(), content.cols(), content.type());
		content.copyTo(dst);
		Point p1 = new Point(0, bestUpperBaselineX);
		Point p2 = new Point(content.cols(),
				bestUpperBaselineX - content.cols() * Math.sin(2*Math.PI*degree/360.0));
		Core.line(dst, p1, p2, Scalar.all(120));
		
		p1 = new Point(0, bestLowerBaselineX);
		p2 = new Point(content.cols(),
				bestLowerBaselineX - content.cols() * Math.sin(2*Math.PI*degree/360.0));
		Core.line(dst, p1, p2, Scalar.all(210), 2);
		return dst;
	}
	
	/* (non-Javadoc)
	 * Blurs Hough space according the -blur parameter.
	 */
	private Mat blurHough(Mat hough) {
		int xSize = conf.getInteger("blurRho");
		int ySize = conf.getInteger("blurTheta");
		if (conf.getString("blurMode").equals("mean")) {
			Imgproc.blur(hough, hough, new Size(xSize, ySize));
		} else if (conf.getString("blurMode").equals("median")) {
			hough = Geometry.myMedianBlur(hough, xSize, ySize);
		}
		return hough;
	}
	
	/* (non-Javadoc)
	 * Get the maximum in hough space
	 */
	private HoughBand getHoughMax(Mat hough) {
		MinMaxLocResult minMax = Core.minMaxLoc(hough);
		return new HoughBand("max",
				(int) minMax.maxLoc.x, 
				(int) minMax.maxLoc.x,
				(int) minMax.maxLoc.y);
	}
	
	/* (non-Javadoc)
	 * Get the best band from the hough transform.
	 */
	private void getBestBands(List<HoughBand> bands, Mat content, Mat hough) {
		double deltaLambda = conf.getFloat("deltaLambda");
		double fromLambdaMin = conf.getFloat("startLambdaBandMin");
		double toLambdaMin = conf.getFloat("endLambdaBandMin");
		double fromLambdaWidth = conf.getFloat("startLambdaBandWidth");
		double toLambdaWidth = conf.getFloat("endLambdaBandWidth");
		double fromLambda = Math.min(fromLambdaMin, fromLambdaWidth);
		double toLambda = Math.max(toLambdaMin, toLambdaWidth);
		int nLambdaVals = (int) ((0.0001 + toLambda - fromLambda) / deltaLambda);
		if (nLambdaVals <= 0) {
			return;
		}
		int[] bestMinValToX = new int[nLambdaVals],
			bestMinValFromX = new int[nLambdaVals],
			bestMinValY = new int[nLambdaVals];
		int[] bestBandWidthToX = new int[nLambdaVals],
			bestBandWidthFromX = new int[nLambdaVals],
			bestBandWidthY = new int[nLambdaVals];
		double[] bestMinVal = new double[nLambdaVals],
				bestBandWidth = new double[nLambdaVals];
		Arrays.fill(bestMinVal, -1.0);
		Arrays.fill(bestBandWidth, Double.POSITIVE_INFINITY);
		
		int houghWidth = hough.cols();
		double sum = Core.sumElems(content).val[0];
		double maxValArea = conf.getFloat("maxValArea");
		// Search for best band for each theta
		for (int y = 0; y < resolution; y++) {
			double lambda = fromLambda;
			double required = sum * lambda;
			int lambdaIdx = 0;
			if (Core.sumElems(hough.row(y)).val[0] < required) {
				continue;
			}
			for (int fromX = 0; fromX < houghWidth; fromX++) {
				lambda = fromLambda;
				required = sum * lambda;
				lambdaIdx = 0;
				double acc = 0.0;
				double minVal = hough.get(y, fromX)[0];
				if (minVal <= 0.00001) {
					continue;
				}
				double maxVal = minVal;
				double maxX = fromX;
				for (int toX = fromX; toX < houghWidth; toX++) {
					double val = hough.get(y, toX)[0];
					acc += val;
					minVal = Math.min(minVal, val);
					if (val > maxVal) {
						maxVal = val;
						maxX = toX;
					}
					// If required pixel collected and maximum is in lower half
					if (acc >= required 
							&& maxX-fromX >= maxValArea*(toX-fromX)) {
						do {
							if (minVal > bestMinVal[lambdaIdx]) {
								bestMinValToX[lambdaIdx] = toX;
								bestMinValFromX[lambdaIdx] = fromX;
								bestMinValY[lambdaIdx] = y;
								bestMinVal[lambdaIdx] = minVal;
							}
							if (toX-fromX < bestBandWidth[lambdaIdx]) {
								bestBandWidthToX[lambdaIdx] = toX;
								bestBandWidthFromX[lambdaIdx] = fromX;
								bestBandWidthY[lambdaIdx] = y;
								bestBandWidth[lambdaIdx] = toX - fromX;
							}
							lambda += deltaLambda;
							lambdaIdx++;
							if (lambdaIdx >= nLambdaVals) {
								toX = houghWidth;
								break;
							}
							required = sum * lambda;
						} while (acc >= required);
					}
				}
			}
		}
		// Add best band to {@code bands}
		double lambda = fromLambda;
		for (int lambdaIdx = 0; lambdaIdx < nLambdaVals; lambdaIdx++) {
			if (lambda >= fromLambdaMin - 0.0001 &&
					lambda <= toLambdaMin + 0.0001) {
				bands.add(new HoughBand("min-" + Math.round(lambda*100),
						bestMinValFromX[lambdaIdx],
						bestMinValToX[lambdaIdx], bestMinValY[lambdaIdx]));
			}
			if (lambda >= fromLambdaWidth - 0.0001 &&
					lambda <= toLambdaWidth + 0.0001) {
				bands.add(new HoughBand("width-" + Math.round(lambda*100),
						bestBandWidthFromX[lambdaIdx],
						bestBandWidthToX[lambdaIdx], bestBandWidthY[lambdaIdx]));
			}
			lambda += deltaLambda;
		}
	}
}
