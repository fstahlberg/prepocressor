package qa.qcri.prepocressor.operations;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the drawKaldiAlignment operation.
 * 
 * @author Felix Stahlberg
 */
public class DrawKaldiAlignmentOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public DrawKaldiAlignmentOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("alignmentFile", "kaldi.al",
					"Path to the Kaldi alignment file in text format");
				addParameter("kaldiId", "%base",
					"This string specifies how the kaldi ID is generated. You"
					+ " can use the same placeholders as in outputPath.");
				addParameter("borderHeight", 30,
					"Height of the border for annotations.");
				addParameter("offset", 0,
					"Offset from the right image border.");
			}
			@Override
			protected String getDescription() {
				return "Reads a Kaldi alignment file and draws the forced "
					+ "alignment into the image. Assumes left-to-right topology"
					+ " for nonsilence phones and whatever for silence.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		String[] lines = new String[]{null, null};
		Logger log = Logger.getSingleton();
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		String id = indy.getPopulation().getFullIndividualFileName(
				conf.getString("kaldiId"),
				indy.getIdxPrefix());
		try {
			FileInputStream fstream = new FileInputStream(conf.getString(
				"alignmentFile"));
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			int nr = 0;
			while ((strLine = br.readLine()) != null) {
				String[] split = strLine.split(" ", 2);
				if (split.length == 2 && split[0].equals(id)) {
					lines[nr++] = split[1].trim();
					if (nr >= 2) {
						break;
					}
				}
			}
			in.close();
		} catch (Exception e) {// Catch exception if any
			log.warn("Aignment file reading error: "
					+ e.getMessage());
			return children;
		}
		if (lines[1] == null) {
			log.warn("ID " + id + " not found in alignment file.");
			return children;
		}
		children.clear();
		Individual child = new Individual();
		child.setContent(draw(content,
				lines[0].trim(), lines[1].trim()));
		children.add(child);
		content.release();
		return children;
	}
	
	/* (non-Javadoc)
	 * Draws alignment stuff into the image
	 */
	private Mat draw(Mat content, String alString, String charString) {
		String[] chars = charString.split(" +");
		String[] states = alString.substring(1, alString.length() - 1)
				.split("\\] \\[");
		int nFeats = 0;
		for (String chStates : states) {
			nFeats += chStates.trim().split(" ").length;
		}
		int border = conf.getInteger("borderHeight");
		Mat dst = Mat.zeros(content.rows() + 2*border, content.cols(), content.type());
		// ceil is more correct afaik, but with quantization errors...?
		int winWidth = (int) Math.round(((double) content.cols()) / nFeats);
		if (chars.length != states.length || winWidth < 1) {
			Logger.getSingleton().warn("Malformed alignment file");
			return dst;
		}
		Logger.getSingleton().notice("Set window width to " + winWidth);
		Imgproc.copyMakeBorder(content, dst, border, border, 0, 0, Imgproc.BORDER_CONSTANT,
			Scalar.all(255));
		int winStart = content.cols() - winWidth - conf.getInteger("offset");
		Scalar[] fillColors = {new Scalar(0,0,125), new Scalar(0,125,0), new Scalar(125,0,0)};
		Scalar silCol = new Scalar(0, 125, 125);
		for (int idx = 0; idx < chars.length; idx++) {
			String ch = chars[idx];
			boolean silPhone = ch.toLowerCase().equals("sil") || ch.toLowerCase().equals("conn");
			String[] chStates = states[idx].trim().split(" ");
			int idOffset = Integer.parseInt(chStates[0])-1;
			for (String state : chStates) {
				int colIdx = (Integer.parseInt(state) - idOffset) / 2;
				drawFilledRect(dst, winStart, winStart+winWidth,
						silPhone || colIdx < 0 || colIdx >= fillColors.length ? 
								silCol : fillColors[colIdx]);
				winStart -= winWidth;
			}
			Core.line(dst, new Point(winStart+winWidth, 0),
				new Point(winStart+winWidth, dst.rows()), new Scalar(0,0,0), 2);
			Core.putText(dst, ch, 
					new Point(winStart+winWidth, 0.75*border),
					Core.FONT_HERSHEY_SIMPLEX, border / 45.0,
					new Scalar(0,0,0));
		}
		return dst;
	}
	
	private void drawFilledRect(Mat dst, int fromX, int toX, Scalar color) {
		int border = conf.getInteger("borderHeight");
		fromX = Math.max(0, fromX);
		if (toX - fromX <= 0) {
			return;
		}
		Rect colorRect = new Rect(fromX, border,
				toX - fromX, dst.rows() - 2*border);
		Mat add = Mat.ones(colorRect.size(), dst.type());
		Core.rectangle(add, new Point(0,0),
			new Point(colorRect.width, colorRect.height),
			color, Core.FILLED);
		Mat subDst = dst.submat(colorRect);
		Core.add(subDst, add, subDst);
		add.release();
	}
}
