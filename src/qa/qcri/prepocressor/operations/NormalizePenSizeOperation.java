package qa.qcri.prepocressor.operations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.Geometry;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the normalizePenSize operation.
 * 
 * @author Felix Stahlberg
 */
public class NormalizePenSizeOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public NormalizePenSizeOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("squarePen", 0,
					"Set this to 1 if the pen has the same size in "
					+ "horizontal and vertical direction. If this is set to 0 "
					+ "the horizontal and vertical pen sizes are estimated "
					+ "separately.");
				addParameter("mode", "erode",
					"Pen normalization technique:\n+\n"
					+ "* 'erode': Apply erode operation with a kernel half the "
						+ "size of the pen. This technique is defensive: It "
						+ "usually does not result in skeletons but is more "
						+ "likely to preserve all important information.\n"
					+ "* 'thinning': This is a two step procedure. First, the "
						+ "image is processed with the line thinning algorithm "
						+ "(see thinning operation). Additionally, the original "
						+ "image is convoluted with a pen sized box filter and "
						+ "binarized."
						+ "Both the blurred and thinned image are combined with "
						+ "bitwise or. This results into a skeletonized script "
						+ "image with larger white areas on loops without "
						+ "centered background pixel.\n-");
				addParameter("openHoles", 0,
					"Set to 1 to look for holes and open them.");
				addParameter("normPenSize", 2,
					"Size of the pen after normalization. Only used if mode=erode."
					+ "Should be a multiplier of 2. Keep in mind that this is not "
					+ "an exat value as the erode mode by itself is not exact.");
			}
			@Override
			protected String getDescription() {
				return "This operation can be used instead of standard "
					+ "line thinning algorithms for pen size normalization "
					+ "for OCR. It first estimates the horizontal and vertical "
					+ "pen size. Then, it convolutes the image with a kernel "
					+ "with the estimated dimensions. We filter all points "
					+ "which are no maxima in either horizontal or vertical "
					+ "direction.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		Size kSize = getKernelSize(content);
		
		String mode = conf.getString("mode");
		if (mode.equals("thinning")) {
			Mat thin = new Mat(content.size(), content.type());
			content.copyTo(thin);
			thin = Geometry.thinning(thin);
			Imgproc.blur(content, content, kSize);
			Imgproc.threshold(content, content, 254, 1, Imgproc.THRESH_BINARY);
			Core.bitwise_or(content, thin, content);
			thin.release();
		} else { // Mode is erode
			int sub = conf.getInteger("normPenSize") / 2;
			int erodeWidth = (int) kSize.width/2 - sub;
			int erodeHeight = (int) kSize.height/2 - sub;
			if (erodeWidth > 0 && erodeHeight > 0) {
				Mat kernel = Imgproc.getStructuringElement(
						Imgproc.MORPH_ELLIPSE,
						new Size(kSize.width/2-1,kSize.height/2-1));
				Imgproc.morphologyEx(content, content, Imgproc.MORPH_ERODE, kernel);
				kernel.release();
			}
		}
		
		if (conf.getInteger("openHoles") > 0) { // Search for holes
			Imgproc.threshold(content, content, 0.5, 255, Imgproc.THRESH_BINARY);
			Mat holes = new Mat(content.size(), content.type());
			int bSize = (int) Math.min(kSize.width, kSize.height);
			Imgproc.blur(content, holes, new Size(bSize, bSize));
			Imgproc.threshold(holes, holes, 254, 255, Imgproc.THRESH_BINARY);
			Core.subtract(content, holes, content);
			content = Geometry.thinning(content);
			holes.release();
		}
		indy.setContent(content);
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		return children;
	}
	
	/* (non-Javadoc)
	 * Get the kernel size for the blurring operation. This is basically
	 * the pen size estimation. We take the median of all foreground
	 * segment lengths in both directions.
	 */
	private Size getKernelSize(Mat content) {
		boolean square = conf.getInteger("squarePen") > 0;
		List<Integer> accHoriz = new ArrayList<Integer>();
		List<Integer> accVert = square ? accHoriz
				: new ArrayList<Integer>();
		int h = content.rows();
		int w = content.cols();
		for (int i = 0; i < h; i++) {
			addSegmentLengths(accHoriz, content.row(i));
		}
		for (int i = 0; i < w; i++) {
			addSegmentLengths(accVert, content.col(i));
		}
		int penWidth = getMedian(accHoriz);
		int penHeight = square ? penWidth : getMedian(accVert);
		Logger.getSingleton().debug("Estimated pen size: "
			+ penWidth + "x" + penHeight);
		return new Size(penWidth, penHeight);
	}
	
	/* (non-Javadoc)
	 * Add all segment lengths in the array to the list
	 */
	private void addSegmentLengths(List<Integer> l, Mat srcMat) {
		int n = srcMat.cols() * srcMat.rows();
		byte[] src = new byte[n];
		srcMat.get(0, 0, src);
		for (int start = 0; start < n; start++) {
			if (src[start] != 0) {
				int end = start + 1;
				while (end < n && src[end] != 0) {
					end++;
				}
				l.add(end - start);
				start = end;
			}
		}
	}
	
	/* (non-Javadoc)
	 * Standard median calculation
	 */
	private int getMedian(List<Integer> a) {
		if (a.isEmpty()) {
			Logger.getSingleton().warn("Pen size estimation: no foreground");
			return 1;
		}
		Collections.sort(a);
		int n = a.size();
		int mid = n / 2;
		if (n % 2 == 1) {
			return a.get(mid);
		}
		return (a.get(mid - 1) + a.get(mid)) / 2;
	}
}
