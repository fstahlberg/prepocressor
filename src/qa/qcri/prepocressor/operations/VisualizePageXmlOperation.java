package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.primaresearch.dla.page.Page;
import org.primaresearch.dla.page.io.xml.PageXmlInputOutput;
import org.primaresearch.dla.page.layout.PageLayout;
import org.primaresearch.dla.page.layout.physical.Region;
import org.primaresearch.dla.page.layout.physical.text.LowLevelTextObject;
import org.primaresearch.dla.page.layout.physical.text.impl.TextRegion;
import org.primaresearch.io.UnsupportedFormatVersionException;
import org.primaresearch.maths.geometry.Polygon;
import org.primaresearch.maths.geometry.Rect;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the visualizePageXml operation.
 * 
 * @author Felix Stahlberg
 */
public class VisualizePageXmlOperation extends Operation {
	
	private class DrawableRect {
		public final org.opencv.core.Rect rect;
		public final Scalar color;
		public final String id;
		
		public DrawableRect(org.opencv.core.Rect rect, Scalar color, String id) {
			this.rect = rect;
			this.color = color;
			this.id = id;
		}
	}
	
	private enum LabelMode {NONE, ID, CONSECUTIVE};
	private LabelMode labelMode = LabelMode.NONE;
	private int fontThickness = 4;
	
	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public VisualizePageXmlOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("xmlPath", "%base%idx.xml",
					"Path to the xml files in PAGE format. The same place"
					+ "holders as in the global outputPath can be used.");
				addParameter("minLevel", 0,
					"Minimum level in layout of region to be extracted.");
				addParameter("extractRegions", 1,
					"Extract regions.");
				addParameter("extractTextObjects", 0,
					"Extract text lines.");
				addParameter("showConfidences", 0,
					"If this is set to 1, the border color around lines corresponds "
					+ "to the OCR confidence as defined in the page xml file. If "
					+ "this is set to 0 the border is always red. NOTE: This "
					+ "function is NOT implemented yet!");
				addParameter("lineLabels", "none",
					"This defines the way how line labels are displayed:\n+\n"
					+ "* 'none': Do not show any line labels.\n"
					+ "* 'page-ids': Use line ids defined in the page xml file.\n"
					+ "* 'consecutive': Number lines starting from 1.\n-");
				addParameter("fontThickness", 4,
					"Thickness of label font. Only used if -lineLabels is set "
					+ "to 'page-ids' or 'consecutive'.");
			}
			@Override
			protected String getDescription() {
				return "Visualizes line segmentations and confidences in page XML files."
					+ "Note: The used PAGE library may break with multiple "
					+ "threads!";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Logger log = Logger.getSingleton();
		String lineLabels = conf.getString("lineLabels");
		fontThickness = conf.getInteger("fontThickness");
		if (lineLabels.equals("page-ids")) {
			labelMode = LabelMode.ID;
		} else if (lineLabels.equals("consecutive")) {
			labelMode = LabelMode.CONSECUTIVE;
		} else {
			labelMode = LabelMode.NONE;
		}
		Mat content = indy.getContent();
		String fileName = indy.getPopulation().getFullIndividualFileName(
			conf.getString("xmlPath"),
			indy.getIdxPrefix());
		List<DrawableRect> rects = new LinkedList<DrawableRect>();
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		try {
			Page page = PageXmlInputOutput.readPage(fileName);
			PageLayout layout = page.getLayout();
			for (Region region : layout.getRegionsSorted()) {
				addRegion(rects, indy, region, 0, indy.getIdxPrefix());
			}
		} catch (UnsupportedFormatVersionException e) {
			log.error("PAGE format error in " + fileName + ": " + e.getMessage());
			return children;
		}
		addOverlay(content, rects);
		drawDecorations(content, rects);
		return children;
	}
	
	private void addOverlay(Mat content, List<DrawableRect> rects) {
		Mat org = new Mat(content.size(), content.type());
		content.copyTo(org);
		Core.addWeighted(content, 0.0, content, 0.5, 0.0, content);
		for (DrawableRect rect : rects) {
			org.submat(rect.rect).copyTo(content.submat(rect.rect));
		}
		org.release();
	}
	
	private void drawDecorations(Mat content, List<DrawableRect> rects) {
		int thickness = 2;
		int i = 1;
		for (DrawableRect rect : rects) {
			Point p1 = new Point(rect.rect.x - thickness,
					rect.rect.y - thickness);
			Point p2 = new Point(p1.x + rect.rect.width + 2*thickness,
					p1.y + rect.rect.height + 2*thickness);
			Core.rectangle(content, p1, p2, rect.color, thickness);
			
			String txt = "";
			if (labelMode == LabelMode.CONSECUTIVE) {
				txt = "" + (i++);
			} else if (labelMode == LabelMode.ID) {
				txt = rect.id;
			}
			int fontHeight = rect.rect.height / 2;
			if (fontHeight < 6 || txt.isEmpty()) {
				continue;
			}
			double fontScale = getBestFontScale(txt,
				Math.max(fontHeight, content.cols() - rect.rect.x - rect.rect.width),
				fontHeight);
			Size txtSize = Core.getTextSize(txt,
				Core.FONT_HERSHEY_SIMPLEX, fontScale, fontThickness, null);
			Point txtP1 = new Point(
				Math.min(rect.rect.x + rect.rect.width,
					content.cols() - txtSize.width - 2*thickness - 1),
				rect.rect.y - thickness);
			Point txtP2  = new Point(
				txtP1.x + txtSize.width + 2*thickness,
				txtP1.y + txtSize.height + 2*thickness);
			Core.rectangle(content, txtP1, txtP2, rect.color, Core.FILLED);
			Core.putText(content, txt,
				new Point(txtP1.x + thickness, txtP2.y - thickness),
				Core.FONT_HERSHEY_SIMPLEX, fontScale, Scalar.all(255),
				fontThickness);
		}
	}
	
	/* (non-Javadoc)
	 * Get largest font size such that the text still fits in the given 
	 * bounding box
	 * 
	 * TODO: Refactor.. copied from renderPageXmlTranscription
	 */
	private double getBestFontScale(String txt, int width, int height) {
		double scale = 0.0;
		for (double incr = 1.0; incr > 0.011; incr /= 10.0) {
			Size size = new Size();
			int i = 0;
			do {
				scale += incr;
				size = Core.getTextSize(txt,
						Core.FONT_HERSHEY_SIMPLEX, scale, fontThickness, null);
			} while (size.width <= width && size.height <= height
					&& i++ < 10);
			scale -= incr;
		}
		return Math.max(0.1, scale);
	}
	
	/* (non-Javadoc)
	 * Recursive helper function for adding regions to children
	 */
	private void addRegion(List<DrawableRect> rects, Individual indy,
			Region region, int level, String idxPrefix) {
		String idx = idxPrefix + "_" + region.getId().toString();
		if (region.hasRegions()) { // recursive descend
			int n = region.getRegionCount();
			for (int i = 0; i < n; i++) {
				addRegion(rects, indy, region.getRegion(i), level+1, idx);
			}
		}
		if (conf.getInteger("minLevel") > level) {
			return;
		}
		if (conf.getInteger("extractTextObjects") > 0
				&& region.getType().getName().equals("TextRegion")) {
			TextRegion txtRegion = (TextRegion) region;
			for (LowLevelTextObject txtObj : txtRegion.getTextObjectsSorted()) {
				rects.add(createRect(indy, txtObj.getCoords(),
						txtObj.getId().toString(), txtObj.getConfidence()));
			}
		}
		if (conf.getInteger("extractRegions") > 0) {
			rects.add(createRect(indy, region.getCoords(),
					region.getId().toString(), 0.0));
		}
	}
	
	/* (non-Javadoc)
	 * Creates a new instance of DrawableRect from a page xml region
	 */
	private DrawableRect createRect(Individual indy, Polygon poly, 
			String id, double confidence){
		Mat content = indy.getContent();
		Rect bBox = poly.getBoundingBox();
		org.opencv.core.Rect rect = new org.opencv.core.Rect(
				bBox.left, bBox.top, 
				Math.min(content.cols() - bBox.left, bBox.getWidth()),
				Math.min(content.rows() - bBox.top, bBox.getHeight()));
		Scalar color = new Scalar(0, 0, 255);
		if (conf.getInteger("showConfidences") > 0) {
			color = new Scalar(0, 0, 255);//TODO
		}
		return new DrawableRect(rect, color, id);
	}
}
