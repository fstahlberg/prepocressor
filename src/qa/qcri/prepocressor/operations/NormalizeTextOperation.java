package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Range;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the normalizeText operation.
 * 
 * @author Felix Stahlberg
 */
public class NormalizeTextOperation extends Operation {
	
	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public NormalizeTextOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("aboveBaseline", 32,
						"Vertical distance in the output image from baseline"
						+ " to upper border.");
				addParameter("belowBaseline", 16,
					"Vertical distance in the output image from baseline"
					+ " to lower border.");
				addParameter("maxCut", 1.0f,
					"Maximum sum at cropped border");
				addParameter("minCroppedAboveRatio", 0.1f,
					"Distance of cropped border above baseline (relative "
					+ "to image border.");
				addParameter("minCroppedAboveAbsolute", 20,
					"Distance of cropped border above baseline.");
				addParameter("minCroppedBelowRatio", 0.02f,
					"Distance of cropped border above baseline (relative "
					+ "to image border.");
				addParameter("maxBelowStretch", 1.5f,
					"If scale below and above baseline differ by this factor "
					+ "(relative) use above bl scale for below bl.");
				addParameter("maxBelowShrink", 3.0f,
					"If scale below and above baseline differ by this factor "
					+ "(relative) use above bl scale for below bl.");
			}
			@Override
			protected String getDescription() {
				return "Assumes that images contain text lines with "
					+ "horizontal baseline at image center. Scales images such"
					+ "that the baseline is repositioned as defined by "
					+ "-below/aboveBaseline and scaled such that the "
					+ "first/last row is the nearest row to the baseline which "
					+ "sums up to less than -maxCut pixels. The resulting "
					+ "images will have the height belowBaseline+aboveBaseline.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		Mat reduced = Mat.zeros(content.rows(), content.cols(), CvType.CV_32FC1);
		Core.reduce(content, reduced, 1, Core.REDUCE_SUM, CvType.CV_32FC1);
		int aboveBL = conf.getInteger("aboveBaseline");
		int belowBL = conf.getInteger("belowBaseline");
		int oldBL = content.rows()/2;
		float maxCut = conf.getFloat("maxCut");
		float minAboveRatio = conf.getFloat("minCroppedAboveRatio");
		int minAboveAbsolute = conf.getInteger("minCroppedAboveAbsolute");
		float minBelowRatio = conf.getFloat("minCroppedBelowRatio");
		int height = aboveBL + belowBL;
		int fromRow = (int) ((1.0-minAboveRatio) * oldBL);
		if (oldBL - fromRow < minAboveAbsolute) {
			fromRow = Math.max(0, oldBL - minAboveAbsolute);
		}
		int toRow = (int) Math.min(content.rows(),
				oldBL + 1 + minBelowRatio * (content.rows() - oldBL));
		while (fromRow > 0 && reduced.get(fromRow, 0)[0] > maxCut) {
			fromRow--;
		}
		while (toRow < content.rows() && reduced.get(toRow, 0)[0] > maxCut) {
			toRow++;
		}
		double scale = (0.0 + aboveBL) / (0.0 + oldBL - fromRow);
		Mat dst = Mat.zeros(height, (int) (content.cols()*scale), content.type());
		Mat upper = content.rowRange(fromRow, oldBL);
		Mat dstUpper = new Mat(dst, new Range(0, aboveBL));
		Imgproc.resize(upper, dstUpper, dstUpper.size(), 0, 0, Imgproc.INTER_AREA);
		
		double lowerVertScale = (0.0 + belowBL) / (0.0 + toRow - oldBL);
		Mat dstLower = dst.rowRange(aboveBL, height);
		if (lowerVertScale / scale > conf.getFloat("maxBelowStretch")
				|| scale / lowerVertScale > conf.getFloat("maxBelowShrink")) {
			Logger.getSingleton().debug("Discard lower baseline scale for "
				+ indy.getPopulation().getInputFileName());
			toRow = content.rows();
			if ((toRow - oldBL) * scale > belowBL) {
				toRow = (int) (belowBL/scale) + oldBL;
			}
			dstLower = dst.rowRange(aboveBL,
				(int) (aboveBL + (toRow - oldBL) * scale));
		}
		Mat lower = content.rowRange(oldBL, toRow);
		Imgproc.resize(lower, dstLower, dstLower.size(), 0, 0, Imgproc.INTER_AREA);
		Individual child = new Individual();
		child.setContent(dst);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		content.release();
		reduced.release();
		return children;
	}
}
