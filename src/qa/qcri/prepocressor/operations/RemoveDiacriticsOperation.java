package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the removeDiacritics operation.
 * 
 * @author Felix Stahlberg
 */
public class RemoveDiacriticsOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public RemoveDiacriticsOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("maxHeight", 0.2f,
					"Maximum height of a diacritic relative to image height");
				addParameter("maxWidth", 0.3f,
						"Maximum width of a diacritic relative to image height");
			}
			@Override
			protected String getDescription() {
				return "Remove diacritics (small and quadractic connected "
					+ "components) in the image";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		int maxHeight = (int) (conf.getFloat("maxHeight") * content.rows());
		int maxWidth = (int) (conf.getFloat("maxWidth") * content.rows());
		Mat working = new Mat(content.rows(), content.cols(), content.type());
		content.copyTo(working);
		List<MatOfPoint> contours = new LinkedList<MatOfPoint>();
		Mat mask = Mat.ones(new Size(content.cols()+2,content.rows()+2), CvType.CV_8UC1);
		Mat hierarchy = new Mat(content.rows(), content.cols(), content.type());
		Imgproc.copyMakeBorder(content, mask, 1, 1, 1, 1, Imgproc.BORDER_DEFAULT);
		Imgproc.findContours(working, contours, hierarchy, 
				Imgproc.RETR_EXTERNAL,
				Imgproc.CHAIN_APPROX_NONE);
		Core.bitwise_not(mask, mask, mask);
		for (MatOfPoint contour : contours) {
			Rect rect = Imgproc.boundingRect(contour);
			if (rect.height <= maxHeight && rect.width <= maxWidth) {
				Point p = new Point(contour.get(0, 0));
				p.x -= rect.x;
				p.y -= rect.y;
				// Restrict flooding to bounding rect, otherwise larger components
				// might be flooded (floodFill and findContours sometimes do not
				// agree about connected components)
				Imgproc.floodFill(content.submat(rect), mask.submat(new Rect(
						rect.x-1, rect.y-1,
						rect.width+2, rect.height+2)),
					p, Scalar.all(0));
			}
			contour.release();
		}
		hierarchy.release();
		mask.release();
		working.release();
		List<Individual> children = new LinkedList<Individual>();
		Individual child = new Individual();
		child.setContent(content);
		children.add(child);
		return children;
	}
}
