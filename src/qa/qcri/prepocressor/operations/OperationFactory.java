package qa.qcri.prepocressor.operations;

import java.lang.reflect.Constructor;

import qa.qcri.prepocressor.io.Logger;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Abstract factory producing operations.
 * 
 * @author Felix Stahlberg
 */
final public class OperationFactory {

	/*
	 * Private constructor - do not instantiate.
	 */
	private OperationFactory() {
		;
	}

	/**
	 * Get a fully configured operation instance.
	 * 
	 * @param cmd the full command string for this operation, i.e. the string
	 * 		between to '|'
	 * @return a operation instance or null if an error occurred
	 */
	public static Operation createOperation(String cmd) {
		String trimmed = cmd.trim();
		if (trimmed.isEmpty()) {
			return null;
		}
		String name = getNameFromCommand(trimmed);
		Operation op = null;
		try {
			Class<?> c = Class.forName("qa.qcri.prepocressor.operations."
					+ Character.toUpperCase(name.charAt(0)) + name.substring(1)
					+ "Operation");
			Constructor<?> cons = c.getConstructor(String.class);
			op = (Operation) cons.newInstance(name);
			op.configure(trimmed.substring(name.length()).trim().split(" +"));
		} catch (ClassNotFoundException e) {
			Logger.getSingleton().fatalError("Operation '" + name 
				+ "' is not implemented");
		} catch (Exception e) {
			Logger.getSingleton().fatalError("Implementation of '" + name 
				+ "' cannot be used because of coding errors.");
		}
		return op;
	}

	private static String getNameFromCommand(String trimmed) {
		int pos = trimmed.indexOf(" ");
		if (pos < 0) {
			return trimmed;
		}
		return trimmed.substring(0, pos);
	}
}
