package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the grayscale operation.
 * 
 * @author Felix Stahlberg
 */
public class ReduceOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public ReduceOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("mode", "sum",
					"Accumulation mode. Available modes: sum, avg, max, min, "
					+ "sqrSum");
				addParameter("dim", 1,
					"Dimension along which the reduction is done. E.g. 1"
					+ " reduces the image to a single column.");
			}
			@Override
			protected String getDescription() {
				return "Calculates the projections of "
					+ "the images in the pipeline. The images are reduced "
					+ "to a single column or row (see dim parameter).";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		String mode = conf.getString("mode");
		Mat content = indy.getContent();
		Mat dst = new Mat(content.rows(), content.cols(), CvType.CV_32FC1);
		int type = 0;
		if (mode.equals("sum")) {
			type = Core.REDUCE_SUM;
		} else if (mode.equals("avg")) {
			type = Core.REDUCE_AVG;
		} else if (mode.equals("max")) {
			type = Core.REDUCE_MAX;
		} else if (mode.equals("min")) {
			type = Core.REDUCE_MIN;
		}
		Core.reduce(content, dst, conf.getInteger("dim"), type, CvType.CV_32FC1);
		Individual child = new Individual();
		child.setContent(dst);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		return children;
	}
}
