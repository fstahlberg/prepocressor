package qa.qcri.prepocressor.operations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the outlierRemove operation.
 * 
 * @author Felix Stahlberg
 */
public class OutlierRemoveOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public OutlierRemoveOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("tolerance", 3.0f,
					"Tolerance parameter for outlier detection.");
			}
			@Override
			protected String getDescription() {
				return "Removes outlier. Outlier are identified by differing "
					+ "by -tolerance times standard derivation from the mean."
					+ " Input needs to be 1 channel float.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		double eps = conf.getFloat("tolerance");
		double maxVal = 9999999;
		Mat content = indy.getContent();
		int minLineCount = 10;
		float[] vals = new float[content.rows() * content.cols()];
		content.get(0, 0, vals);
		
		ArrayList<Float> maxList = new ArrayList<Float>();
		for (int i = 1; i < vals.length - 1; i++) {
			if (vals[i-1] < vals[i] && vals[i+1] < vals[i]) {
				maxList.add(vals[i]);
			}
		}
		Collections.sort(maxList);
		if (maxList.size() > minLineCount) {
			maxVal = maxList.get(maxList.size() - minLineCount);
		}
		
		
		Arrays.sort(vals);
		int cnt = 0;
		float lastVal = -1;
		for (int i = 0; i < vals.length; i++) {
			if (lastVal != vals[i]) {
				//System.out.println(cnt + "x " + lastVal);
				cnt = 1;
				lastVal = vals[i];
			} else {
				cnt++;
			}
		}
		double median = vals[vals.length/2];
		
		MatOfDouble meanMat = new MatOfDouble();
		MatOfDouble stddevMat = new MatOfDouble();
		Core.meanStdDev(content, meanMat, stddevMat);
		double mean = meanMat.get(0, 0)[0];
		double stddev = stddevMat.get(0, 0)[0];
		double tolerance = stddev*eps;
		for (int row = 0; row < content.rows(); row++) {
			for (int col = 0; col < content.cols(); col++) {
				if (content.get(row, col)[0] > maxVal) {
					content.put(row, col, new double[]{maxVal});
				}
			}
		}
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		return children;
	}
}
