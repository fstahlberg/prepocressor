package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the normalizeUpperBaseline operation.
 * 
 * @author Felix Stahlberg
 */
public class NormalizeUpperBaselineOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public NormalizeUpperBaselineOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("upperBaselineHighest", 0.8f,
					"Highest possible ratio between distance between baselines"
					+ " and highest ascender - lower baseline distance");
				addParameter("upperBaselineLowest", 0.2f,
					"Lowest possible ratio between distance between baselines"
					+ " and highest ascender - lower baseline distance");
				addParameter("newUpperBaseline", 0.4f,
					"New ratio between distance between baselines"
					+ " and highest ascender - lower baseline distance. This is"
					+ "only applicable in combination with -operation=align");
				addParameter("maxCut", 1.0f,
					"Maximum sum at cropped border");
				addParameter("keepCoreZoneAspectRatio", 0,
					"Set to 1 to keep the aspect ratio in the core zone "
					+ "between upper and lower baseline. Otherwise, the core "
					+ "zone is stretched/shrinked in order to reposition the "
					+ "upper baseline. Only applicable if operation=align");
				addParameter("maxStretchFactor", 4.0f,
					"Works with keepCoreZoneAspectRatio=1. Maximum horizontal"
					+ " stretching factor");
				addParameter("minStretchFactor", 0.25f,
					"Works with keepCoreZoneAspectRatio=1. Minimum horizontal"
					+ " stretching factor");
				addParameter("minCroppedAboveRatio", 0.5f,
					"Distance of cropped border above baseline (relative "
					+ "to image border.");
				addParameter("minCroppedAboveAbsolute", 20,
					"Distance of cropped border above baseline.");
				addParameter("operation", "align",
					"What should be done after the upper baseline is found\n"
					+ "'align': Reposition the baseline to a predefined height\n"
					+ "'draw': Draw a line indicating the upper baseline");
			}
			@Override
			protected String getDescription() {
				return "This operation normalizes the position of the upper "
					+ "baseline. The input image should be an aligned image "
					+ "with lower baseline in image center (see houghTextLine "
					+ "operation) The upper baseline is estimated at the "
					+ "maximum in the derivative of the horizontal projection "
					+ "profile above the lower baseline. The image is modified "
					+ "s.t. the upper baseline is at a predefined height. Note: "
					+ "If you apply the normalizeText operation after this, the "
					+ "maxCut, minCroppedAboveRatio, and minCroppedAboveAbsolute"
					+ "parameters should be equal.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		Mat reduced = Mat.zeros(content.rows(), content.cols(), CvType.CV_32FC1);
		Core.reduce(content, reduced, 1, Core.REDUCE_SUM, CvType.CV_32FC1);
		float maxCut = conf.getFloat("maxCut");
		float minAboveRatio = conf.getFloat("minCroppedAboveRatio");
		int minAboveAbsolute = conf.getInteger("minCroppedAboveAbsolute");
		int lowerBL = content.rows()/2;
		int fromRow = (int) ((1.0-minAboveRatio) * lowerBL);
		if (lowerBL - fromRow < minAboveAbsolute) {
			fromRow = Math.max(0, lowerBL - minAboveAbsolute);
		}
		while (fromRow > 0 && reduced.get(fromRow, 0)[0] > maxCut) {
			fromRow--;
		}
		int searchRangeFrom = (int) (lowerBL - 
				(lowerBL - fromRow) * conf.getFloat("upperBaselineHighest"));
		int searchRangeTo = (int) (lowerBL - 
				(lowerBL - fromRow) * conf.getFloat("upperBaselineLowest"));
		if (searchRangeTo - searchRangeFrom > 0) {
			Mat sobel = new Mat(reduced.size(), reduced.type());
			Imgproc.Sobel(reduced.submat(searchRangeFrom, searchRangeTo, 0, 1),
				sobel, -1, 0, 1, 5, 1.0, 0.0,
				Imgproc.BORDER_REPLICATE);
			MinMaxLocResult minMax = Core.minMaxLoc(sobel);
			int upperBL = (int) minMax.maxLoc.y + searchRangeFrom;
			sobel.release();
			if (conf.getString("operation").equals("draw")) {
				Core.line(content, new Point(0, upperBL),
					new Point(content.cols(), upperBL), Scalar.all(200), 3);
			} else { // Align
				content = align(content, lowerBL, upperBL, fromRow);
			}
		} else {
			Logger.getSingleton().warn("Range for upper baseline is empty");
		}
		reduced.release();
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		return children;
	}
	
	/* (non-Javadoc)
	 * Align operation
	 */
	private Mat align(Mat content, int lowerBL, int upperBL, int fromRow) {
		if (lowerBL - upperBL <= 0 || upperBL - fromRow <= 0) {
			Logger.getSingleton().warn("Upper baseline is at border."
				+ "could not align");
			return content;
		}
		int newUpperBL = (int) (lowerBL - 
			(lowerBL - fromRow) * conf.getFloat("newUpperBaseline"));
		if (conf.getInteger("keepCoreZoneAspectRatio") > 0) {
			double factor = Math.min(conf.getFloat("maxStretchFactor"),
				Math.max(conf.getFloat("minStretchFactor"),
					(0.0 + lowerBL - newUpperBL) / (0.0 + lowerBL - upperBL)));
			Imgproc.resize(content, content, new Size(), factor, 1.0,
				Imgproc.INTER_CUBIC);
		}
		int w = content.cols();
		Mat src = new Mat(content.size(), content.type());
		content.copyTo(src);
		Mat dst = content.submat(fromRow, newUpperBL, 0, w);
		Imgproc.resize(src.submat(fromRow, upperBL, 0, w), dst,
			dst.size(), 0, 0, Imgproc.INTER_AREA);
		dst = content.submat(newUpperBL, lowerBL, 0, w);
		Imgproc.resize(src.submat(upperBL, lowerBL, 0, w), dst,
			dst.size(), 0, 0, Imgproc.INTER_AREA);
		src.release();
		return content;
	}
}
