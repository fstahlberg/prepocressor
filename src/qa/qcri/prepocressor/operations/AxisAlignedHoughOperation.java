package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.ExtentedImgproc;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the axisAlignedHough operation.
 * 
 * @author Felix Stahlberg
 */
public class AxisAlignedHoughOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public AxisAlignedHoughOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("fromTheta", 45f,
					"Minimum value for theta.");
				addParameter("toTheta", 135f,
						"Maximum value for theta.");
				addParameter("thetaResolution", 90,
						"Number of theta quantization steps.");
			}
			@Override
			protected String getDescription() {
				return "This is a specialized and modified version of the "
					+ "Hough transformation. In contrast to the Hough space, "
					+ "rho is always on the x-axis. Rho ranges from 0 to image"
					+ " width. The range and resolution for theta can be "
					+ "specified. The advantage of this implementation is that"
					+ " there are no quantization errors for rho since the "
					+ "resolution is exactly one pixel. The disadvantage is "
					+ "that only lines crossing the x axis between 0 and image"
					+ " width are considered. The returned image contains the "
					+ "counts where the y coordinate represents theta.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat dst = ExtentedImgproc.axisAlignedHoughTransform(indy.getContent(),
			conf.getFloat("fromTheta") / 180 * Math.PI,
			conf.getFloat("toTheta") / 180 * Math.PI,
			conf.getInteger("thetaResolution"));
		
		Individual child = new Individual();
		child.setContent(dst);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		indy.getContent().release();
		return children;
	}
}
