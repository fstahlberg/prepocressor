package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.ExtentedImgproc;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the textSkewCorrection operation.
 * 
 * @author Felix Stahlberg
 */
public class TextSkewCorrectionOperation extends Operation {
	
	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public TextSkewCorrectionOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("sobelKSize", 3,
					"Size of the sobel kernel.");
				addParameter("resolution", 90,
					"Resolution of Hough transform.");
				addParameter("maxDegree", 15f,
					"If the detected text skew exceeds this parameter (in "
					+ "degrees), the detection is assumed to be incorrect "
					+ "and no correction is applied.");
				addParameter("fromDegree", -50f,
					"Minimal degree considered by Hough transform.");
				addParameter("toDegree", 50f,
					"Maximal degree considered by Hough transform.");
			}
			@Override
			protected String getDescription() {
				return "Corrects the text skew resulting from italic writing "
					+ "styles. Assumes that the base line is centered, i.e. "
					+ "pixels on the middle horizontal lines are not "
					+ "affected by this transformation.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		int resolution = conf.getInteger("resolution");
		Mat content = indy.getContent();
		double fromRad = Math.PI/2.0 + conf.getFloat("fromDegree")/180.0*Math.PI;
		double toRad = Math.PI/2.0 + conf.getFloat("toDegree")/180.0*Math.PI;
		Mat hough = ExtentedImgproc.axisAlignedHoughTransform(content,
				fromRad, toRad, resolution);
		Mat sobel = Mat.zeros(content.rows(), content.cols(), content.type());
		Imgproc.Sobel(hough, sobel, -1, 1, 0,
				conf.getInteger("sobelKSize"), 1, 0);
		hough.release();
		Mat sobel2 = sobel.mul(sobel);
		sobel.release();
		Mat reduced = Mat.zeros(content.rows(), content.cols(), CvType.CV_32FC1);
		Core.reduce(sobel2, reduced, 1, Core.REDUCE_SUM);
		MinMaxLocResult minMaxReduced = Core.minMaxLoc(reduced);
		float textTheta = (float) (fromRad-Math.PI/2.0 
			+ minMaxReduced.maxLoc.y / resolution * (toRad - fromRad));
		sobel2.release();
		reduced.release();
		List<Individual> children = new LinkedList<Individual>();
		if (Math.abs(360.0*textTheta/(2*Math.PI)) > conf.getFloat("maxDegree")) {
			Logger.getSingleton().debug("Text skew detection for "
				+ indy.getPopulation().getInputFileName() + " failed because "
				+ "detected skew exceeds -maxDegree parameter");
			children.add(indy);
			return children;
		}
		double textSkew = (float) -Math.sin(textTheta);
		
		int baselineHeight = content.rows()/2;
		float offset = (float) (baselineHeight*textSkew);
		float absOffset = Math.abs((float) (baselineHeight*textSkew));
		
		Mat fromPoints = Mat.zeros(4, 1, CvType.CV_32FC2);
		Mat toPoints = Mat.zeros(4, 1, CvType.CV_32FC2);
		
		fromPoints.put(0, 0, new float[]{absOffset-offset, 0.0f});
		toPoints.put(0, 0, new float[]{absOffset, 0.0f});
		
		fromPoints.put(1, 0, new float[]{content.cols()-offset, 0.0f});
		toPoints.put(1, 0, new float[]{content.cols()+absOffset, 0.0f});
		
		fromPoints.put(2, 0, new float[]{absOffset+offset, content.rows()});
		toPoints.put(2, 0, new float[]{absOffset, content.rows()});
		
		fromPoints.put(3, 0, new float[]{content.cols()+offset, content.rows()});
		toPoints.put(3, 0, new float[]{content.cols()+absOffset, content.rows()});
		
		Mat dst = Mat.zeros(content.rows(), (int) (content.cols()+2*absOffset), content.type());
		Mat p = Imgproc.getPerspectiveTransform(fromPoints, toPoints);
		Imgproc.warpPerspective(content, dst, p, dst.size());
		
		Individual child = new Individual();
		child.setContent(dst);
		children.add(child);
		content.release();
		p.release();
		return children;
	}
}
