package qa.qcri.prepocressor.operations;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.datastructures.Population;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the extractConstantRegions operation. In contrast to the most
 * operations, this class needs to override the {@link #processPopulation(Population)}
 * method and breaks with the GoF template method pattern.
 * 
 * @see #processPopulation(Population)
 * @see #processIndividual(Individual)
 * @author Felix Stahlberg
 */
public class ExtractConstantRegionsOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public ExtractConstantRegionsOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("minLength", 5,
					"Minimal time period for a region in number of frames");
				addParameter("threshold", 20.0f,
					"Maximal sum of differences in channels for a pixel to "
					+ "be considered as constant.");
				addParameter("closeSize", 30,
					"Kernel size of closing operation");
				addParameter("openSize", 150,
					"Kernel size of open operation");
			}
			@Override
			protected String getDescription() {
				return "This operations assumes that the images in the pipeline "
					+ "are frames of a video. Note that the fps video filter "
					+ "in ffmpeg can be used to extract an image every X seconds "
					+ "of the video. The operation searches for regions which do "
					+ "not change during a certain time period. In news videos, "
					+ "these regions usually correspond to overlays embedded in "
					+ "the video displaying additional information. In the "
					+ "documentation for this operation we refer a single image "
					+ "in the pipeline as 'frame'.";
			}
		};
	}
	
	

	/**
	 * Finds temporal constant regions in the images in the pipeline.
	 * 
	 * @param pop population
	 * @throws NullPointerException if pop or pop.inputFile is null
	 * @return single element population with the augmented input image
	 */
	@Override
	public Population processPopulation(Population pop) {
		Population newPop = new Population(pop.getInputFileName());
		int nFrames = pop.size(); 
		if (nFrames < 2) {
			Logger.getSingleton().error("Not enough frames");
			return newPop;
		}
		Mat[] diffImages = getDiffImages(pop);
		diffImages = enforceMinLength(diffImages);
		int idx = 0;
		for (Individual indy : pop) {
			if (idx < diffImages.length) {
				List<MatOfPoint> contours = getContours(diffImages[idx]);
				int subIdx = 0;
				String idxStart = "_" + Operation.generateId(idx) + "_";
				for (MatOfPoint contour : contours) {
					Rect rect = Imgproc.boundingRect(contour);
					Individual childIndy = indy.produceChild(rect);
					childIndy.setIdxPrefix(idxStart 
							+ Operation.generateId(subIdx++));
					newPop.add(childIndy);
				}
				diffImages[idx++].release();
			}
			indy.getContent().release();
		}
		return newPop;
	}
	
	/* (non-Javadoc)
	 * Get connected components
	 */
	private List<MatOfPoint> getContours(Mat input) {
		Mat working = new Mat(input.size(), input.type());
		input.copyTo(working);
		List<MatOfPoint> contours = new LinkedList<MatOfPoint>();
		Mat hierarchy = new Mat(input.size(), input.type());
		Imgproc.findContours(working, contours, hierarchy, 
				Imgproc.RETR_EXTERNAL,
				Imgproc.CHAIN_APPROX_NONE);
		working.release();
		hierarchy.release();
		return contours;
	}
	
	/* (non-Javadoc)
	 * Only keep regions which are constant for at least minLength frames
	 */
	private Mat[] enforceMinLength(Mat[] diffImages) {
		int idx = diffImages.length - 1;
		int minLength = conf.getInteger("minLength");
		for (; idx >= minLength; idx--) {
			for (int j = idx - minLength; j < idx; j++) {
				Core.bitwise_and(diffImages[j], diffImages[idx],
						diffImages[idx]);
			}
		}
		Size size = diffImages[idx].size();
		int type = diffImages[idx].type();
		// First images are completely black
		for (; idx >= 0; idx--) {
			diffImages[idx].release();
			diffImages[idx] = Mat.zeros(size, type);
		}
		return diffImages;
	}
	
	/* (non-Javadoc)
	 * Get difference images
	 */
	private Mat[] getDiffImages(Population pop) {
		int closeSize = conf.getInteger("closeSize");
		int openSize = conf.getInteger("openSize");
		Mat closeKernel = Imgproc.getStructuringElement(
			Imgproc.MORPH_RECT, new Size(closeSize, closeSize));
		Mat openKernel = Imgproc.getStructuringElement(
			Imgproc.MORPH_RECT, new Size(openSize, openSize));
		int nFrames = pop.size(); 
		Mat[] diffImages = new Mat[nFrames - 1];
		Iterator<Individual> it = pop.iterator();
		Mat prevContent = it.next().getContent();
		for (int i = 0; i < diffImages.length; i++) {
			Mat content = it.next().getContent();
			Mat diffImage = new Mat(content.size(), content.type());
			Core.absdiff(content, prevContent, diffImage);
			List<Mat> channels = new LinkedList<Mat>();
			Core.split(diffImage, channels);
			Mat threshold = Mat.zeros(content.size(), channels.get(0).type());
			for (Mat channel : channels) {
				Core.add(threshold, channel, threshold);
			}
			Imgproc.threshold(threshold, threshold, conf.getFloat("threshold"),
				1.0, Imgproc.THRESH_BINARY_INV);
			Imgproc.morphologyEx(threshold, threshold,
					Imgproc.MORPH_CLOSE, closeKernel);
			Imgproc.morphologyEx(threshold, threshold,
					Imgproc.MORPH_OPEN, openKernel);
			diffImages[i] = threshold;
			prevContent = content;
		}
		return diffImages;
	}

	/**
	 * @throws UnsupportedOperationException unconditionally
	 * @see #processPopulation(Population)
	 */
	@Override
	protected List<Individual> processIndividual(Individual indy) {
		throw new UnsupportedOperationException();
	}
}
