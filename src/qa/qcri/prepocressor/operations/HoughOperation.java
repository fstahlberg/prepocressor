package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the hough operation.
 * 
 * @author Felix Stahlberg
 */
public class HoughOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public HoughOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("angleResolution", 360,
					"Angle resolution (number of different values for theta)");
				addParameter("angleSamplingFactor", 200,
					"HoughLines is called with resolution 1/angleSamplingFactor"
					+ ". A higher value reduces the noise in the Hough transform"
					+ ", but needs longer execution time.");
				addParameter("toMatrix", 0,
					"Transform the hough transformation to a matrix. The "
					+ "rho values are shifted so that the minimum value "
					+ "corresponds to x=0, and x=width/2 to rho=0. The theta "
					+ "values are scaled by angleResolution. If toMatrix is "
					+ "not set, the operation passes an array of 3 dimensional"
					+ " vectors storing [rho, theta, voteCount] ordered "
					+ "descending by voteCount.");
			}
			@Override
			protected String getDescription() {
				return "The hough command performs a Hough transformation "
					+ "on the input images. This is the original OpenCV "
					+ "implementation. Consider using axisAlignedHough if "
					+ "this is not the last operation in the pipeline or "
					+ "you want to obtain a meaningful image. This operation "
					+ "is useful for directly redirecting the output to a "
					+ "CSV file.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		int thetaRes = conf.getInteger("angleResolution");
		Mat content = indy.getContent();
		Mat lines = Mat.zeros(content.size(), CvType.CV_32FC3);
		// NOTE: Modified hough lines that preserves accumulator values
		Imgproc.HoughLines(content, lines, 1, 2*Math.PI/(400*thetaRes), 1);
		Individual child = new Individual();
		if (conf.getInteger("toMatrix") == 0) {
			child.setContent(lines);
		} else { // Transform to matrix
			List<Mat> channels = new LinkedList<Mat>();
			Core.split(lines, channels);
			Mat rhos = channels.get(0);
			Mat thetas = channels.get(1);
			Mat counts = channels.get(2);
			MinMaxLocResult minMax = Core.minMaxLoc(rhos);
			int shift = (int) Math.max(minMax.maxVal, -minMax.minVal);
			Mat hough = Mat.zeros(shift*2, thetaRes, CvType.CV_32FC1);
			int n = rhos.cols();
			for (int i = 0; i < n; i++) {
				hough.put(
					(int) rhos.get(0, i)[0] + shift,
					(int) (thetas.get(0, i)[0] * thetaRes / Math.PI), counts.get(0, i));
			}
			child.setContent(hough);
			lines.release();
		}
		content.release();
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		return children;
	}
}
