package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.primaresearch.dla.page.Page;
import org.primaresearch.dla.page.io.xml.PageXmlInputOutput;
import org.primaresearch.dla.page.layout.PageLayout;
import org.primaresearch.dla.page.layout.physical.Region;
import org.primaresearch.dla.page.layout.physical.text.LowLevelTextObject;
import org.primaresearch.dla.page.layout.physical.text.impl.TextRegion;
import org.primaresearch.io.UnsupportedFormatVersionException;
import org.primaresearch.maths.geometry.Polygon;
import org.primaresearch.maths.geometry.Rect;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the cutWithPageXml operation.
 * 
 * @author Felix Stahlberg
 */
public class CutWithPageXmlOperation extends Operation {

	private int border = 0;
	
	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public CutWithPageXmlOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("xmlPath", "%base%idx.xml",
					"Path to the xml files in PAGE format. The same place"
					+ "holders as in the global outputPath can be used.");
				addParameter("minLevel", 0,
					"Minimum level in layout of region to be extracted.");
				addParameter("extractRegions", 1,
					"Extract regions.");
				addParameter("extractTextObjects", 0,
					"Extract text lines.");
				addParameter("border", 0,
					"If -border equals 0 we cut the text regions accurately. "
					+ "Use a value greater than zero if the extracted regions"
					+ " are used for human inspections. It will add a padding "
					+ "to the image with decreased brightness and draw a red "
					+ "rectangle around the region.");
				addParameter("usePageIds", 0,
					"Use id attributes in xml file for naming. Otherwise, use "
					+ "consecutive numbering (see global idLength parameter)");
			}
			@Override
			protected String getDescription() {
				return "Cut a page image using an XML file in PAGE format."
					+ "Note: The used PAGE library may break with multiple "
					+ "threads!";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		border = conf.getInteger("border");
		Logger log = Logger.getSingleton();
		Mat content = indy.getContent();
		String fileName = indy.getPopulation().getFullIndividualFileName(
			conf.getString("xmlPath"),
			indy.getIdxPrefix());
		List<Individual> children = new LinkedList<Individual>();
		try {
			Page page = PageXmlInputOutput.readPage(fileName);
			PageLayout layout = page.getLayout();
			for (Region region : layout.getRegionsSorted()) {
				addRegion(children, indy, region, 0, indy.getIdxPrefix());
			}
			content.release();
		} catch (UnsupportedFormatVersionException e) {
			log.error("PAGE format error in " + fileName + ": " + e.getMessage());
			children.add(indy);
		}
		return children;
	}
	
	/* (non-Javadoc)
	 * Recursive helper function for adding regions to children
	 */
	private void addRegion(List<Individual> children, Individual indy,
			Region region, int level, String idxPrefix) {
		String idx = idxPrefix + "_" + region.getId().toString();
		if (region.hasRegions()) { // recursive descend
			int n = region.getRegionCount();
			for (int i = 0; i < n; i++) {
				addRegion(children, indy, region.getRegion(i), level+1, idx);
			}
		}
		if (conf.getInteger("minLevel") > level) {
			return;
		}
		String outputPath = Configuration.getGlobalConfiguration()
				.getString("outputPath");
		Logger log = Logger.getSingleton();
		if (conf.getInteger("extractTextObjects") > 0
				&& region.getType().getName().equals("TextRegion")) {
			TextRegion txtRegion = (TextRegion) region;
			for (LowLevelTextObject txtObj : txtRegion.getTextObjectsSorted()) {
				Individual child = extractChildFromCoords(children, indy,
					txtObj.getCoords(), idx + "_" + txtObj.getId().toString());
				log.debug("Transcription for "
					+ child.getTargetFileName(outputPath) + ": " + txtObj.getText());
			}
		}
		if (conf.getInteger("extractRegions") > 0) {
			extractChildFromCoords(children, indy, region.getCoords(), idx);
		}
	}
	
	/* (non-Javadoc)
	 * Creates a new child individual from a subregion in the image
	 */
	private Individual extractChildFromCoords(List<Individual> children,
			Individual indy, Polygon poly, String idx) {
		Mat content = indy.getContent();
		Rect bBox = poly.getBoundingBox();
		Mat child = border > 0 ? produceChildContentWithBorder(content, bBox)
				: produceChildContent(content, bBox, poly);
		Individual childIndy = indy.produceChild(new org.opencv.core.Rect(
				bBox.left, bBox.top, bBox.getWidth(), bBox.getHeight()), false);
		childIndy.setContent(child);
		childIndy.setIdxPrefix(conf.getInteger("usePageIds") > 0 
			? idx : "_" + Operation.generateId(children.size()));
		children.add(childIndy);
		
		return childIndy;
	}

	/* (non-Javadoc)
	 * Produce child content with -border > 0
	 */
	private Mat produceChildContentWithBorder(Mat content, Rect bBox) {
		org.opencv.core.Rect inner = new org.opencv.core.Rect(
			bBox.left, bBox.top, 
			Math.min(content.cols() - bBox.left, bBox.getWidth()),
			Math.min(content.rows() - bBox.top, bBox.getHeight()));
		org.opencv.core.Rect outer = new org.opencv.core.Rect(
			Math.max(0, bBox.left - border),
			Math.max(0, bBox.top - border),
			bBox.getWidth() + 2*border,
			bBox.getHeight() + 2*border);
		if (outer.width + outer.x > content.cols()) {
			System.out.println("reduce width");
			outer.width = content.cols() - outer.x;
		}
		if (outer.height + outer.y > content.rows()) {
			System.out.println("reduce height");
			outer.height = content.rows() - outer.y;
		}
		Mat child = Mat.zeros(new Size(outer.width, outer.height), content.type());
		Core.addWeighted(child, 0.0, content.submat(outer), 0.5, 0.0, child);
		content.submat(inner).copyTo(child.submat(new org.opencv.core.Rect(
			inner.x - outer.x, inner.y - outer.y, inner.width, inner.height)));
		int thickness = 2;
		Point p1 = new Point(inner.x - outer.x - thickness,
				inner.y - outer.y - thickness);
		Point p2 = new Point(p1.x + inner.width + 2*thickness,
				p1.y + inner.height + 2*thickness);
		Core.rectangle(child, p1, p2, new Scalar(0, 0, 255), thickness);
		return child;
	}
	
	/* (non-Javadoc)
	 * Produce child content with -border=0
	 */
	private Mat produceChildContent(Mat content, Rect bBox, Polygon poly) {
		int w = bBox.getWidth();
		int h = bBox.getHeight();
		int xOffset = bBox.left;
		int yOffset = bBox.top;
		Mat child = Mat.zeros(new Size(w, h), content.type());
		Mat mask = Mat.zeros(new Size(w, h), CvType.CV_8UC1);
		for (int x = 0; x < w; x++) { // TODO: not very efficient
			for (int y = 0; y < h; y++) {
				if (poly.isPointInside(x + xOffset, y + yOffset)) {
					mask.put(y, x, new byte[]{1});
				}
			}
		}
		Core.add(child, content.submat(
			yOffset, yOffset + h, xOffset, xOffset + w), child, mask);
		mask.release();
		return child;
	}
}
