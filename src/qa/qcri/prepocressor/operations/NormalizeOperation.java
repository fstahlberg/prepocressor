package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.ExtentedImgproc;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the normalize operation.
 * 
 * @author Felix Stahlberg
 */
public class NormalizeOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public NormalizeOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("newMin", 0f, 
					"Minimum value of the new interval.");
				addParameter("newMax", 255f, 
						"Maximum value of the new interval.");
			}
			@Override
			protected String getDescription() {
				return "The normalize operation rescales the values in the "
					+ "matrices to the given interval. The current value "
					+ "range is fetched from the first channel only.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		double newMin = conf.getFloat("newMin");
		double newMax = conf.getFloat("newMax");
		Mat content = indy.getContent();
		ExtentedImgproc.normalize(content, newMin, newMax);
		Individual child = new Individual();
		child.setContent(content);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		return children;
	}
}
