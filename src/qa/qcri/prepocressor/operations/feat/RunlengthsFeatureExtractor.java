package qa.qcri.prepocressor.operations.feat;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;

import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This is a slightly more general version of a raw pixel feature extractor.
 * It uses the mean of submatrices in the frame as features, where the sub
 * matrices have the same width as the frame, height according featRawCellHeight
 * and position according featRawCellShift (parameters of conf).
 * 
 * @author Felix Stahlberg
 */
public class RunlengthsFeatureExtractor extends FeatureExtractor {
	
	private int radius;
	private float foregroundThreshold;
	private boolean nonNegative;

	/* (non-Javadoc)
	 * @see FeatureExtractor#FeatureExtractor(Configuration)
	 */
	public RunlengthsFeatureExtractor(Configuration conf) {
		super(conf);
		radius = conf.getInteger("featRunlengthsRadius");
		foregroundThreshold = conf.getFloat("foregroundThreshold");
		nonNegative = conf.getInteger("featRunlengthsNonNegative") > 0;
	}

	/**
	 * Splits the input frame into cells with frame width and featRawCellHeight
	 * height. Extract features every featRawCellShift y position.
	 * {@inheritDoc}
	 */
	@Override
	public List<Double> extractFeatures(Mat whole, Mat frame) {
		List<Double> feat = new LinkedList<Double>();
		int height = frame.rows();
		int width = frame.cols();
		Size wholeSize = new Size();
		Point ofs = new Point();
		frame.locateROI(wholeSize, ofs);
		int[][] top = new int[height][width];
		int[][] left = new int[height][width];
		int[][] right = new int[height][width];
		int[][] bottom = new int[height][width];
		// Vertical features
		for (int colIdx = 0; colIdx < width; colIdx++) {
			Mat col = whole.col((int) ofs.x + colIdx);
			int lastSegEnd = 0;
			for (int segStart = 0; segStart < height; segStart++) {
				if (col.get(segStart, 0)[0] >= foregroundThreshold) {
					int segEnd = segStart + 1;
					while (segEnd < height 
							&& col.get(segEnd, 0)[0] >= foregroundThreshold) {
						segEnd++;
					}
					for (int segIdx = segStart; segIdx < segEnd; segIdx++) {
						top[segIdx][colIdx] = Math.min(radius, segIdx - segStart + 1);
						bottom[segIdx][colIdx] = Math.min(radius, segEnd - segIdx);
					}
					if (!nonNegative) {
						for (int segIdx = lastSegEnd; segIdx < segStart; segIdx++) {
							top[segIdx][colIdx] = Math.max(-radius, lastSegEnd - segIdx);
							bottom[segIdx][colIdx] = Math.max(-radius, segIdx - segStart);
						}
					}
					segStart = segEnd;
					lastSegEnd = segEnd;
				}
			}
			if (!nonNegative) {
				for (int segIdx = lastSegEnd; segIdx < height; segIdx++) {
					top[segIdx][colIdx] = Math.max(-radius, lastSegEnd - segIdx);
					bottom[segIdx][colIdx] = -radius;
				}
			}
		}
		int xMax = (int) Math.min(width + radius, wholeSize.width - ofs.x - 2);
		int xMin = (int) Math.max(-ofs.x, -radius);
		// Horizontal features
		for (int rowIdx = 0; rowIdx < height; rowIdx++) {
			Mat row = whole.row(rowIdx);
			int lastSegEnd = xMin;
			for (int segStart = xMin; segStart < xMax; segStart++) {
				if (row.get(0, (int) ofs.x + segStart)[0] >= foregroundThreshold) {
					int segEnd = segStart + 1;
					while (segEnd < xMax
							&& row.get(0, (int) ofs.x + segEnd)[0] >= foregroundThreshold) {
						segEnd++;
					}
					for (int segIdx = Math.max(0, segStart); 
							segIdx < Math.min(width, segEnd); segIdx++) {
						left[rowIdx][segIdx] = Math.min(radius, segIdx - segStart + 1);
						right[rowIdx][segIdx] = Math.min(radius, segEnd - segIdx);
					}
					if (!nonNegative) {
						for (int segIdx = Math.max(0, lastSegEnd); 
								segIdx < Math.min(width, segStart); segIdx++) {
							left[rowIdx][segIdx] = Math.max(-radius, lastSegEnd - segIdx);
							right[rowIdx][segIdx] = Math.max(-radius, segIdx - segStart);
						}
					}
					segStart = segEnd;
					lastSegEnd = segEnd;
				}
			}
			if (!nonNegative) {
				for (int segIdx = Math.max(0, lastSegEnd); 
						segIdx < width; segIdx++) {
					left[rowIdx][segIdx] = Math.max(-radius, lastSegEnd - segIdx);
					right[rowIdx][segIdx] = Math.max(-radius, segIdx - width);
				}
			}
		}
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				feat.add((double) top[y][x]);
				feat.add((double) bottom[y][x]);
				feat.add((double) left[y][x]);
				feat.add((double) right[y][x]);
			}
		}
		return feat;
	}

}
