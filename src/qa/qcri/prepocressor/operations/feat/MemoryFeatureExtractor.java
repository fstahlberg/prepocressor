package qa.qcri.prepocressor.operations.feat;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;

import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This abstract class implements a memory for feature extractors such that the
 * extraction can access features and frames in the past.
 * 
 * @author Felix Stahlberg
 */
abstract public class MemoryFeatureExtractor extends FeatureExtractor {
	
	/**
	 * Sole constructor. Initializes {@link #featHistory} and
	 * {@link #frameHistory} and stores the configuration.
	 * 
	 * @param conf configuration of the featExtract operation
	 */
	public MemoryFeatureExtractor(Configuration conf) {
		super(conf);
		featHistory = new LinkedList<List<Double>>();
		frameHistory = new LinkedList<Mat>();
	}
	
	/**
	 * Stores the features from the past calls of 
	 * {{@link #extractFeatures(Mat)} (newest first)
	 */
	protected LinkedList<List<Double>> featHistory;
	
	/**
	 * Stores the frames from the past calls of 
	 * {{@link #extractFeatures(Mat)} (newest first)
	 */
	protected LinkedList<Mat> frameHistory;

	/**
	 * Delegates feature extraction but maintain feature and frame history.
	 * 
	 * @see #featHistory
	 * @see #frameHistory
	 * @see #extractCurrentFeatures(Mat)
	 */
	@Override
	public List<Double> extractFeatures(Mat whole, Mat frame) {
		List<Double> feat = extractCurrentFeatures(frame);
		if (feat != null) {
			featHistory.addFirst(feat);
			frameHistory.addFirst(frame);
		}
		return feat;
	}

	/**
	 * Extract features for the current frame. Implementations can access the
	 * variables {@link #featHistory} and {@link #frameHistory} storing the 
	 * last features and frames from newest to last (excluding current frame).
	 * 
	 * @param frame current frame
	 * @return list of features
	 */
	abstract public List<Double> extractCurrentFeatures(Mat frame);
}
