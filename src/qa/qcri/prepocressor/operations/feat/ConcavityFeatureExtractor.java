package qa.qcri.prepocressor.operations.feat;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This feature extractor implements the concavity features described in
 * "Features for HMM-Based Arabic Handwritten Word Recognition Systems", 2012.
 * The baseline dependent features are reduced to the zones "above baseline"
 * and "below baseline".
 * 
 * @author Felix Stahlberg
 */
public class ConcavityFeatureExtractor extends FeatureExtractor {
	
	private boolean blnDependence;
	private int blnRow;
	private float foregroundThreshold;
	private static Mat[] kernels = null;
	private static Integer kernelSemaphore = 1;

	/* (non-Javadoc)
	 * @see FeatureExtractor#FeatureExtractor(Configuration)
	 */
	public ConcavityFeatureExtractor(Configuration conf) {
		super(conf);
		blnDependence = conf.getInteger("featConcavityBaselineDependence") > 0;
		blnRow = conf.getInteger("baselineHeight");
		foregroundThreshold = conf.getFloat("foregroundThreshold");
		initKernels();
	}
	
	/* (non-Javadoc)
	 * If not already done, initialize concavity kernels
	 */
	private static void initKernels() {
		synchronized(kernelSemaphore) {
			if (kernels == null) {
				kernels = new Mat[6];
				for (int i = 0; i < 6; i++) {
					kernels[i] = Mat.zeros(new Size(3,3), CvType.CV_32SC1);
					kernels[i].put(1, 1, new int[]{-2});
				}
				// Left-up
				kernels[0].put(0, 1, new int[]{3});
				kernels[0].put(1, 0, new int[]{3});
				// Up-right
				kernels[1].put(0, 1, new int[]{3});
				kernels[1].put(1, 2, new int[]{3});
				// Right-down
				kernels[2].put(1, 2, new int[]{3});
				kernels[2].put(2, 1, new int[]{3});
				// Down-left
				kernels[3].put(1, 0, new int[]{3});
				kernels[3].put(2, 1, new int[]{3});
				// Vertical
				kernels[4].put(0, 0, new int[]{2});
				kernels[4].put(1, 0, new int[]{2});
				kernels[4].put(2, 0, new int[]{2});
				// Horizontal
				kernels[5].put(2, 0, new int[]{2});
				kernels[5].put(2, 1, new int[]{2});
				kernels[5].put(2, 2, new int[]{2});
			}
		}
	}

	/**
	 * Extract concavity features based on 3x3 masks as defined by 
	 * (Likforman-Sulem et. al., 2012).
	 * {@inheritDoc}
	 */
	@Override
	public List<Double> extractFeatures(Mat whole, Mat frame) {
		List<Double> feat = new LinkedList<Double>();
		Mat thresholded = new Mat(frame.rows(), frame.cols(), CvType.CV_8UC1);
		Mat frameSC1 = new Mat(frame.rows(), frame.cols(), CvType.CV_8UC1);
		Imgproc.threshold(frame, thresholded, foregroundThreshold, 1, 
			Imgproc.THRESH_BINARY);
		thresholded.convertTo(frameSC1, CvType.CV_8UC1);
		addConcavityFeats(feat, frameSC1);
		if (blnDependence) {
			addConcavityFeats(feat, frameSC1.submat(0, blnRow,
					0, frame.cols()));
			addConcavityFeats(feat, frameSC1.submat(blnRow, frame.rows(),
					0, frame.cols()));
		}
		thresholded.release();
		frameSC1.release();
		return feat;
	}

	/* (non-Javadoc)
	 * Add concavity features for the given thresholded (sub)matrix
	 */
	private void addConcavityFeats(List<Double> feat, Mat frame) {
		Mat filtered = new Mat(frame.rows(), frame.cols(), CvType.CV_16SC1);
		double frameHeight = frame.rows();
		Mat threshold = new Mat(frame.rows(), frame.cols(), frame.type());
		for (Mat kernel : kernels) {
			Imgproc.filter2D(frame, filtered, CvType.CV_16SC1, kernel);
			Imgproc.threshold(filtered, threshold, 5, 1, Imgproc.THRESH_BINARY);
			feat.add(Core.sumElems(threshold).val[0]/frameHeight);
		}
		filtered.release();
		threshold.release();
	}
}
