package qa.qcri.prepocressor.operations.feat;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;

import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This method tries to fit in paths of snakes. This approach is described
 * as segment-based features in (Stahlberg and Vogel, 2015) ICIAP
 * 
 * @author Felix Stahlberg
 */
public class SnakeFeatureExtractor extends ANHDFFeatureExtractor {
	
	private int nSnakes;
	private double defaultHeight;
	private boolean doForeground, doBackground, addCenterDist, addRelative;

	/* (non-Javadoc)
	 * @see FeatureExtractor#FeatureExtractor(Configuration)
	 */
	public SnakeFeatureExtractor(Configuration conf) {
		super(conf);
		nSnakes = conf.getInteger("featSnakeNumber");
		defaultHeight = conf.getInteger("featSnakeDefaultHeight");
		doForeground = conf.getInteger("featSnakeForeground") > 0;
		doBackground = conf.getInteger("featSnakeBackground") > 0;
		addCenterDist = conf.getInteger("featSnakeAddCenterDistances") > 0;
		addRelative = conf.getInteger("featSnakeAddRelativeFeats") > 0;
	}

	/**
	 * Similar to ANHDF features
	 * {@inheritDoc}
	 */
	@Override
	public List<Double> extractFeatures(Mat whole, Mat frame) {
		List<Double> feat = new LinkedList<Double>();
		Mat col = reduce(frame);
		if (doForeground) {
			addFeatures(feat, col);
		}
		if (doBackground) {
			Mat ones = Mat.ones(col.size(), col.type());
			Core.subtract(ones, col, col);
			addFeatures(feat, col);
			ones.release();
		}
		col.release();
		return feat;
	}

	/* (non-Javadoc)
	 * Add features for given column.
	 */
	private void addFeatures(List<Double> feat, Mat col) {
		Slice curSlice = new Slice(nSnakes);
		curSlice.load(col);
		int nSegments = 0;
		while (nSegments < nSnakes && !curSlice.isEmpty(nSegments)) {
			nSegments++;
		}
		if (nSegments == 0) { // Silence representation
			for (int snake = 0; snake < nSnakes; snake++) {
				feat.add(defaultHeight);
				feat.add(0.0);
				if (addCenterDist && snake > 0) {
					feat.add(0.0);
				}
				if (addRelative) {
					feat.add(0.0);
					feat.add(0.0);
				}
			}
		} else {
			double lastCenter = 0.0;
			double sliceLength = curSlice.getLength();
			double sliceFrom = curSlice.getCenter() - sliceLength/2.0;
			for (int snake = 0; snake < nSnakes; snake++) {
				int seg = (int) ((0.0 + snake) / (0.0 + nSnakes) * (0.0 + nSegments));
				double center = curSlice.getCenter(seg); 
				feat.add(center);
				feat.add(curSlice.getLength(seg));
				if (addCenterDist && snake > 0) {
					feat.add(center - lastCenter);
				}
				if (addRelative) {
					feat.add((center-sliceFrom)/sliceLength);
					feat.add(curSlice.getLength(seg)/sliceLength);
				}
				lastCenter = center;
			}
		}
	}

}
