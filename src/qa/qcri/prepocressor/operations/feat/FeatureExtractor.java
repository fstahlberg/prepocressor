package qa.qcri.prepocressor.operations.feat;

import java.util.List;

import org.opencv.core.Mat;

import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This is the most general super class for feature extractors.
 * 
 * @see #extractFeatures()
 * @author Felix Stahlberg
 */
abstract public class FeatureExtractor {
	/**
	 * Configuration of the featExtract operation. Note that this configuration
	 * does not only contain parameters of this feature extractor but of all
	 * possible feature extractors.
	 */
	protected Configuration conf;
	
	/**
	 * Sole constructor.
	 * 
	 * @param conf featExtract configuration
	 */
	public FeatureExtractor(Configuration conf) {
		this.conf = conf;
	}
	
	/**
	 * Extract features from the given frame. The length of the returned array
	 * should be always the same as long as the height of the {@code frame}  
	 * matrix stays the same.
	 * 
	 * @param whole whole input frame
	 * @param frame current frame from which to extract features
	 * @return list of features or null if an error occurred
	 * @throws NullPointerException if frame is null
	 */
	public abstract List<Double> extractFeatures(Mat whole, Mat frame);
}
