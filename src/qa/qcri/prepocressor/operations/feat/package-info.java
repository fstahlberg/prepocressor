/**
 * The feat package contains feature extractors that work together with the
 * featExtract operation
 * 
 * @see FeatExtractOperation
 * @see FeatureExtractor
 */
/**
 * The feat package contains feature extractors that work together with the
 * featExtract operation
 * 
 * @see FeatExtractOperation
 * @see FeatureExtractor
 */
package qa.qcri.prepocressor.operations.feat;