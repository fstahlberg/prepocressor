package qa.qcri.prepocressor.operations.feat;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This feature extractor implements Autonomously Normalized Horizontal 
 * Differential Feature Extraction as presented by El-Mahallawy (2008) in his
 * PhD thesis.
 * 
 * @author Felix Stahlberg
 */
public class ANHDFFeatureExtractor extends FeatureExtractor {
	
	protected int connectEpsilon, segNum;
	protected float foregroundThreshold;
	protected int reductionMode;
	protected Slice lastNonemptySlice = null;
	protected boolean lastSliceWasEmpty = true;
	
	/**
	 * A slice represents a column in the input image, represented in a segment
	 * based manner. It stores positions of foreground regions by their center 
	 * and horizontal stretching.
	 */
	protected class Slice {
		private int[] segFrom;
		private int[] segTo;
		private double[] segCenter;
		private int globalFrom = 0, globalTo = 0;
		private double globalCenter = 0.0;
		
		/**
		 * Sole constructor. Initializes the internal arrays.
		 * 
		 * @param segNum number of segments
		 */
		public Slice(int segNum) {
			segFrom = new int[segNum];
			segTo = new int[segNum];
			segCenter = new double[segNum];
		}
		
		/**
		 * Is this slice empty?
		 */
		public boolean isEmpty() {
			return globalFrom == globalTo;
		}
		
		/**
		 * Is the segment at the given position empty?
		 */
		public boolean isEmpty(int idx) {
			return segFrom[idx] == segTo[idx];
		}
		
		// Getters
		public double getCenter(int idx) {
			return segCenter[idx];
		}
		public double getCenter() {
			return globalCenter;
		}
		public double getLength(int idx) {
			return segTo[idx] - segFrom[idx];
		}
		public double getLength() {
			return globalTo - globalFrom;
		}
		
		/**
		 * Transforms the given column into a representation of segments as
		 * needed for feature calculation
		 * 
		 * @param col Matrix with raw pixel values
		 */
		public void load(Mat col) {
			float vals[] = new float[col.rows()];
			col.get(0, 0, vals);
			int curSegIdx = 0;
			for (int curFrom = 0; curFrom < vals.length; curFrom++) {
				if (vals[curFrom] >= foregroundThreshold) {
					double acc = curFrom*vals[curFrom];
					double sum = vals[curFrom];
					int curTo = curFrom + 1;
					while (curTo < vals.length 
							&& vals[curTo] >= foregroundThreshold) {
						acc += curTo*vals[curTo];
						sum += vals[curTo];
						curTo++;
					}
					if (curSegIdx >= segFrom.length) { // Throw out smallest seg
						int worstL = curTo-curFrom;
						int worstIdx = -1;
						for (int idx = 0; idx < segFrom.length; idx++) {
							if (segTo[idx]-segFrom[idx] < worstL) {
								worstIdx = idx;
								worstL = segTo[idx]-segFrom[idx]; 
							}
						}
						if (worstIdx < 0) { // Current is worst
							curFrom = curTo;
							continue;
						}
						for (int idx = worstIdx+1; idx < segFrom.length; idx++) {
							segFrom[idx-1] = segFrom[idx];
							segTo[idx-1] = segTo[idx];
							segCenter[idx-1] = segCenter[idx];
						}
						curSegIdx--;
					}
					segFrom[curSegIdx] = curFrom;
					segTo[curSegIdx] = curTo;
					segCenter[curSegIdx] = acc/sum;
					curSegIdx++;
					curFrom = curTo;
				}
			}
			if (curSegIdx > 0) {
				globalFrom = segFrom[0];
				globalTo = segTo[curSegIdx-1];
				Moments mom = Imgproc.moments(col);
				globalCenter = mom.get_m01()/mom.get_m00();
			} else {
				globalFrom = globalTo = 0;
				globalCenter = 0.0;
			}
			for (int idx = curSegIdx; idx < segFrom.length; idx++) {
				segFrom[idx] = 0;
				segTo[idx] = 0;
				segCenter[idx] = 0.0;
			}
		}

		/**
		 * Add connection features for segment j given the last slice
		 * 
		 * @param feat feature accumulator (exactly 2 elements will be added)
		 * @param j segment index
		 * @param lastSlice last slice
		 */
		public void addConnectionFeatures(List<Double> feat, int j,
				Slice lastSlice) {
			int firstIdx = segFrom.length + 1;
			int lastIdx = -1;
			int from = segFrom[j];
			int to = segTo[j];
			for (int idx = 0; idx < segFrom.length; idx++) {
				if (Math.max(
						lastSlice.segFrom[idx] - to,
						from - lastSlice.segTo[idx]) <= connectEpsilon) {
					firstIdx = Math.min(firstIdx, idx);
					lastIdx = Math.max(lastIdx, idx);
				}
			}
			if (lastIdx == -1) { // Disconnected segment (Case 2)
				feat.add(-2.0);
				feat.add(-2.0);
			} else { // Case 5
				feat.add((double) firstIdx);
				feat.add((double) lastIdx);
			}
		}
	}

	/* (non-Javadoc)
	 * @see FeatureExtractor#FeatureExtractor(Configuration)
	 */
	public ANHDFFeatureExtractor(Configuration conf) {
		super(conf);
		connectEpsilon = conf.getInteger("featAnhdfConnecticityTolerance");
		segNum = conf.getInteger("featAnhdfSegmentNum");
		foregroundThreshold = conf.getFloat("foregroundThreshold");
		String redMode = conf.getString("featAnhdfReductionMode");
		if (redMode.equals("max")) {
			reductionMode = Core.REDUCE_MAX;
		} else if (redMode.equals("min")) {
			reductionMode = Core.REDUCE_MIN;
		} else if (redMode.equals("average")) {
			reductionMode = Core.REDUCE_AVG;
		} else if (redMode.equals("firstAndLast")) {
			reductionMode = -1;
		} else {
			reductionMode = Core.REDUCE_MAX;
			Logger.getSingleton().warn("Reduction mode '" + redMode + 
				"' not known. Falling back to 'max'.");
		}
		lastNonemptySlice = null;
	}
	
	

	/**
	 * Extracts ANHDF features (dimensionality: 4*featAnhdfSegmentNum.
	 * {@inheritDoc}
	 */
	@Override
	public List<Double> extractFeatures(Mat whole, Mat frame) {
		List<Double> feat = new LinkedList<Double>();
		Mat col = reduce(frame);
		Slice curSlice = new Slice(segNum);
		curSlice.load(col);
		// Implement cases on p. 50 of El-Mahallawy (2008) (PhD thesis)
		if (lastNonemptySlice == null) { // Case 1
			addFirstSliceFeats(feat, curSlice);
		} else {
			for (int j = 0; j < segNum; j++) {
				addFeats(feat, j, curSlice);
			}
		}
		Slice nextSlice = curSlice;
		if (reductionMode < 0 && frame.cols() > 1) {
			nextSlice = new Slice(segNum);
			nextSlice.load(frame.col(0));
		}
		lastSliceWasEmpty = nextSlice.isEmpty();
		if (!nextSlice.isEmpty()) {
			lastNonemptySlice = nextSlice;
		}
		return feat;
	}
	
	
	/* (non-Javadoc)
	 * Add features (Case 2-5)
	 * Assumes: non-empty slice, not first slice
	 */
	private void addFeats(List<Double> feat, int j, Slice curSlice) {
		if (curSlice.isEmpty(j)) { // Case 4
			addNullFeats(feat);
			return;
		}
		feat.add((curSlice.getCenter(j)-lastNonemptySlice.getCenter())
				/ lastNonemptySlice.getLength());
		feat.add(curSlice.getLength(j)
				/ lastNonemptySlice.getLength());
		if (lastSliceWasEmpty) { // Case 3
			feat.add(-3.0);
			feat.add(-3.0);
		} else { // Case 2 or 5
			curSlice.addConnectionFeatures(feat, j, lastNonemptySlice);
		}
	}


	/* (non-Javadoc)
	 * Add all features given that curSlice is the first slice (Case 1)
	 * Assumes: non-empty slice
	 */
	private void addFirstSliceFeats(List<Double> feat, Slice curSlice) {
		double firstLength = curSlice.getLength(0);
		double firstCenter = curSlice.getCenter(0);
		for (int j = 0; j < segNum; j++) {
			if (curSlice.isEmpty(j)) {
				addNullFeats(feat);
			} else {
				feat.add((curSlice.getCenter(j)-firstCenter) / firstLength);
				feat.add(curSlice.getLength(j) / firstLength);
				feat.add(-1.0);
				feat.add(-1.0);
			}
		}
	}

	/* (non-Javadoc)
	 * Add features for empty segments (Case 4)
	 */
	private void addNullFeats(List<Double> feat) {
		feat.add(0.0);
		feat.add(0.0);
		feat.add(0.0);
		feat.add(0.0);
	}



	/* (non-Javadoc)
	 * Get the column from which the slice features are to be extracted.
	 */
	protected Mat reduce(Mat frame) {
		if (reductionMode < 0 || frame.cols() == 1) {
			return frame.col(frame.cols()-1);
		}
		Mat col = new Mat(frame.rows(), 1, CvType.CV_32FC1);
		Core.reduce(frame, col, 1, reductionMode, CvType.CV_32FC1);
		return col;
	}
}
