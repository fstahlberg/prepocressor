package qa.qcri.prepocressor.operations.feat;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;

import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This is a slightly more general version of a raw pixel feature extractor.
 * It uses the mean of submatrices in the frame as features, where the sub
 * matrices have the same width as the frame, height according featRawCellHeight
 * and position according featRawCellShift (parameters of conf).
 * 
 * @author Felix Stahlberg
 */
public class DirectionalFeatureExtractor extends FeatureExtractor {
	
	private int radius;

	/* (non-Javadoc)
	 * @see FeatureExtractor#FeatureExtractor(Configuration)
	 */
	public DirectionalFeatureExtractor(Configuration conf) {
		super(conf);
		radius = conf.getInteger("featDirectionalRadius");
	}

	/**
	 * Splits the input frame into cells with frame width and featRawCellHeight
	 * height. Extract features every featRawCellShift y position.
	 * {@inheritDoc}
	 */
	@Override
	public List<Double> extractFeatures(Mat whole, Mat frame) {
		List<Double> feat = new LinkedList<Double>();
		int height = frame.rows();
		int width = frame.cols();
		Size wholeSize = new Size();
		Point ofs = new Point();
		frame.locateROI(wholeSize, ofs);
		int winX = (int) ofs.x;
		int wholeWidth = (int) wholeSize.width;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				// Top
				feat.add(Core.sumElems(whole.submat(
						Math.max(0, y - radius), y,
						winX + x, winX + x+1)).val[0]);
				// Bottom
				feat.add(Core.sumElems(whole.submat(
						y, Math.min(height, y + radius), 
						winX + x, winX + x+1)).val[0]);
				// Left
				feat.add(Core.sumElems(whole.submat(
						y, y+1,
						Math.max(0, winX + x - radius), winX + x)).val[0]);
				// Right
				feat.add(Core.sumElems(whole.submat(
						y, y+1,
						winX + x, Math.min(wholeWidth, winX + x + radius))).val[0]);
			}
		}
		return feat;
	}

}
