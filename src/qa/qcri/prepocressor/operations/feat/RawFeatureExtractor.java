package qa.qcri.prepocressor.operations.feat;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;

import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This is a slightly more general version of a raw pixel feature extractor.
 * It uses the mean of submatrices in the frame as features, where the sub
 * matrices have the same width as the frame, height according featRawCellHeight
 * and position according featRawCellShift (parameters of conf).
 * 
 * @author Felix Stahlberg
 */
public class RawFeatureExtractor extends FeatureExtractor {
	
	private int cellHeight, cellWidth, cellShift;

	/* (non-Javadoc)
	 * @see FeatureExtractor#FeatureExtractor(Configuration)
	 */
	public RawFeatureExtractor(Configuration conf) {
		super(conf);
		cellHeight = conf.getInteger("featRawCellHeight");
		cellWidth = conf.getInteger("featRawCellWidth");
		cellShift = conf.getInteger("featRawCellShift");
	}

	/**
	 * Splits the input frame into cells with frame width and featRawCellHeight
	 * height. Extract features every featRawCellShift y position.
	 * {@inheritDoc}
	 */
	@Override
	public List<Double> extractFeatures(Mat whole, Mat frame) {
		List<Double> feat = new LinkedList<Double>();
		int frameWidth = frame.cols();
		int frameHeight = frame.rows();
		int cellRow = 0, cellCol;
		do {
			cellCol = 0;
			do {
				feat.add(Core.mean(frame.submat(
					cellRow, Math.min(frameHeight, cellRow + cellHeight),
					cellCol, Math.min(frameWidth, cellCol + cellWidth))).val[0]);
				cellCol += cellShift;
			} while (frameWidth - cellCol > 0);
			cellRow += cellShift;
		} while (frameHeight - cellRow > 0);
		return feat;
	}

}
