package qa.qcri.prepocressor.operations.feat;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This feature extractor implements the distribution features described in
 * "Features for HMM-Based Arabic Handwritten Word Recognition Systems", 2012.
 * For f_1, see {@link RawFeatureExtractor}. Note that all feature indices are
 * shifted by 2 (1 (since it starts by 0) + 1 (f_1 is omitted)). WARNING: 
 * Assumes constant frame width
 * 
 * @author Felix Stahlberg
 */
public class DistributionFeatureExtractor extends FeatureExtractor {
	
	private int cellHeight, cellShift, nCellsAboveBln;
	private float foregroundThreshold;
	private int blnRow;

	/* (non-Javadoc)
	 * @see FeatureExtractor#FeatureExtractor(Configuration)
	 */
	public DistributionFeatureExtractor(Configuration conf) {
		super(conf);
		cellHeight = conf.getInteger("featRawCellHeight");
		cellShift = conf.getInteger("featRawCellShift");
		blnRow = conf.getInteger("baselineHeight");
		foregroundThreshold = conf.getFloat("foregroundThreshold");
	}

	/**
	 * Extract distributional features.
	 * {@inheritDoc}
	 */
	@Override
	public List<Double> extractFeatures(Mat whole, Mat frame) {
		List<Double> feat = new LinkedList<Double>();
		int frameWidth = frame.cols();
		int frameHeight = frame.rows();
		LinkedList<Mat> cells = new LinkedList<Mat>();
		nCellsAboveBln = 0;
		for (int cellStart = 0; cellStart < frameHeight - 1;
				cellStart += cellShift) {
			cells.add(frame.submat(
					cellStart, Math.min(frameHeight, cellStart + cellHeight),
					0, frameWidth));
			if (cellStart > blnRow) {
				nCellsAboveBln++;
			}
		}
		addTransitionFeats(feat, cells);
		addGravityFeats(feat, frame);
		addBaselineDensityFeats(feat, frame);
		addColumnDensityFeats(feat, frame);
		return feat;
	}
	
	/* (non-Javadoc)
	 * Adds the transition features f_2 and f_15
	 */
	protected void addTransitionFeats(List<Double> feat, List<Mat> cells) {
		int acc = 0;
		Iterator<Mat> it = cells.iterator();
		MinMaxLocResult lastMinMax = Core.minMaxLoc(it.next());
		int cnt = 1;
		while (it.hasNext()) {
			MinMaxLocResult thisMinMax = Core.minMaxLoc(it.next());
			acc += Math.abs(
				(lastMinMax.maxVal >= foregroundThreshold ? 1 : 0)
				- (thisMinMax.maxVal >= foregroundThreshold ? 1 : 0));
			lastMinMax = thisMinMax;
			cnt++;
			if (cnt == nCellsAboveBln) {
				feat.add((double) acc);
			}
		}
		if (nCellsAboveBln < 2) { // Special cases for f_15
			feat.add(0.0);
		} else if (cnt < nCellsAboveBln) {
			feat.add((double) acc);
		}
		feat.add((double) acc);
	}
	
	/* (non-Javadoc)
	 * Adds the derivative of center of gravity feature f_3 and the gravity
	 * center to baseline difference f_12, and the gravity zone f_16
	 */
	private double lastGravity = -1;
	private double lastValidGravity = -1;
	protected void addGravityFeats(List<Double> feat, Mat frame) {
		double frameHeight = frame.rows();
		Moments mom = Imgproc.moments(frame);
		double thisGravity = -1;
		if (mom.get_m00() > 0.001) {
			thisGravity = mom.get_m01()/mom.get_m00();
			feat.add(lastGravity >= 0.0 ? (thisGravity-lastGravity) : 0.0);
			feat.add(lastValidGravity >= 0.0 ? (thisGravity-lastValidGravity) : 0.0);
			feat.add((thisGravity-blnRow)/frameHeight);
			feat.add(// f_16
				thisGravity <= 0.6*blnRow ? 1.0
					: thisGravity <= blnRow ? 2.0 : 3.0);
			lastValidGravity = thisGravity;
		} else {
			feat.add(0.0);
			feat.add(0.0);
			feat.add(0.0);
			feat.add(0.0);
		}
		lastGravity = thisGravity;
	}
	
	/* (non-Javadoc)
	 * Adds baseline dependent density features (f_13 and f_14).
	 */
	protected void addBaselineDensityFeats(List<Double> feat, Mat frame) {
		feat.add(Core.mean(frame.submat(0, blnRow, 0, frame.cols())).val[0]);
		feat.add(Core.mean(frame.submat(blnRow, frame.rows(),
				0, frame.cols())).val[0]);
	}
	
	/* (non-Javadoc)
	 * Adds column density features (f_4 and f_11).
	 */
	protected void addColumnDensityFeats(List<Double> feat, Mat frame) {
		for (int col = 0; col < frame.cols(); col++) {
			feat.add(Core.mean(frame.col(col)).val[0]);
		}
	}
}
