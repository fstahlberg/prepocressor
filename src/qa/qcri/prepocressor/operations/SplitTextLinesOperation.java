package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the splitTextLines operation.
 * 
 * @author Felix Stahlberg
 */
public class SplitTextLinesOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public SplitTextLinesOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("minWidth", 4.0f,
					"Minimal width of a child relative to image height.");
				addParameter("maxCut", 2.0f,
					"Maximal sum at split borders");
			}
			@Override
			protected String getDescription() {
				return "Expects input images to have horizontal baselines in "
					+ "the center of the image.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		Mat reduced = Mat.zeros(1, content.cols(), CvType.CV_32FC1);
		Core.reduce(content, reduced, 0, Core.REDUCE_SUM, CvType.CV_32FC1);
		int minWidth = (int) (conf.getFloat("minWidth")*content.rows());
		float maxCut = conf.getFloat("maxCut");
		int fromCol = 0;
		List<Individual> children = new LinkedList<Individual>();
		while (fromCol < content.cols()) {
			int toCol = fromCol + minWidth;
			while (toCol < content.cols() && reduced.get(0, toCol)[0] > maxCut) {
				toCol++;
			}
			if (content.cols() - toCol < minWidth) {
				toCol = content.cols();
			}
			children.add(indy.produceChild(new Rect(
					fromCol, 0, toCol-fromCol, content.rows())));
			fromCol = toCol;
		}
		return children;
	}
}
