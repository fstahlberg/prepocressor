package qa.qcri.prepocressor.operations;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Rect;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the writeRects operation.
 * 
 * @author Felix Stahlberg
 */
public class WriteRectsOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public WriteRectsOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("outputPath", "%base%idx-rect.txt",
					"Path to the rectangle files to create. The same place"
					+ "holders as in the global outputPath can be used.");
			}
			@Override
			protected String getDescription() {
				return "Writes the rectangles for each child into a file.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		if (indy.hasParent()) {
			Rect rect = indy.getParentRect();
			Individual parent = indy.getParent();
			while (parent.hasParent()) {
				Rect parentRect = parent.getParentRect();
				rect.x += parentRect.x;
				rect.y += parentRect.y;
				parent = parent.getParent();
			}
			String fileName = indy.getPopulation().getFullIndividualFileName(
					conf.getString("outputPath"),
					indy.getIdxPrefix());
			try {
				FileWriter fs = new FileWriter(fileName);
				BufferedWriter out = new BufferedWriter(fs);
				out.write("x1 " + rect.x + "\n");
				out.write("y1 " + rect.y + "\n");
				out.write("x2 " + (rect.x+rect.width) + "\n");
				out.write("y2 " + (rect.y+rect.height) + "\n");
				out.write("width " + rect.width + "\n");
				out.write("height " + rect.height + "\n");
				out.close();
			} catch (IOException e) {
				Logger.getSingleton().warn("Error while writing "
						+ fileName + ": " + e.getMessage());
			}
		}
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		return children;
	}
}
