package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Core.MinMaxLocResult;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.ExtentedImgproc;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the exactOrientationCorrection operation.
 * 
 * @author Felix Stahlberg
 */
public class ExactOrientationCorrectionOperation extends Operation {
	private int mode;

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public ExactOrientationCorrectionOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("reloadOriginal", 0,
					"Reload the original image and rotate it. Otherwise "
					+ "use image in the pipeline.");
				addParameter("noCorrection", 0,
					"Pass thru image without modification.");
				addParameter("resolution", 90,
					"Resolution of the Hough transform.");
				addParameter("sobelKSize", 3,
					"Size of the sobel kernel.");
				addParameter("maxAngle", 45f,
					"Maximum skew angle.");
				addParameter("eps", 0.1f,
					"Accuracy in degree.");
				addParameter("refine", 0,
					"Set to 1 to enable refinement. If this parameter is set "
					+ "an additional search in degree +- 0.5 with resolution "
					+ "100 is added assuring that the tested values are "
					+ "multipliers of 0.01");
				addParameter("criterion", "horiz",
					"Maximization criterion. Available values:\n+\n"
					+ "* 'horiz': Horizontal estimation\n"
					+ "* 'vert': Vertical estimation\n"
					+ "* 'sum': Sum of horizontal and vertical estimation\n-");
				addParameter("horizWeight", 0.5f,
					"If the sum criterion is used, the horizontal profile "
					+ "is weighted with horizWeight and the vertical "
					+ "profile is weighted with (1-horizWeight)");
				addParameter("houghLineMode", "scaling",
					"Method for line definition in Hough space. Available "
					+ "values:\n+\n"
					+ "* 'scaling': Gradually increase scaling factor of line definition\n"
					+ "* 'bresenham': Use Bresenhams line drawing algorithm\n"
					+ "* 'exact': Take fractional counts for pixels into account\n-");
			}
			@Override
			protected String getDescription() {
				return "Brings rotated text documents in an upright position. "
					+ "This is done by finding the maximum squared variance "
					+ "angle in the Hough transformed image. An iterative "
					+ "algorithm is applied to increase the accuracy of the "
					+ "skew angle estimation.";
			}
		};
	}
	
	/* (non-Javadoc)
	 * Convert degrees to radians
	 */
	private double degreeToRadians(double degree) {
		return Math.PI * degree / 180.0;
	}
	
	/* (non-Javadoc)
	 * get reduced vector for document skew estimation (see last step in 
	 * corresponding publication (Stahlberg and Vogel, 2105))
	 */
	private void getReducedVector(Mat reduced, Mat content,
			double fromAngle, double toAngle, int resolution) {
		double orth = Math.PI/2.0;
		Mat hough = ExtentedImgproc.axisAlignedHoughTransform(content,
			orth + degreeToRadians(fromAngle), orth + degreeToRadians(toAngle),
			resolution, mode);
		Mat sobel = new Mat(hough.size(), CvType.CV_32FC1);
		Imgproc.Sobel(hough, sobel, -1, 1, 0,
			conf.getInteger("sobelKSize"), 1, 0, Imgproc.BORDER_REPLICATE);
		Mat sobel2 = sobel.mul(sobel);
		Mat reducedTmp = new Mat(resolution, 1, CvType.CV_32FC1);
		Core.reduce(sobel2, reducedTmp, 1, Core.REDUCE_SUM);
		Core.normalize(reducedTmp, reduced);
		hough.release();
		sobel.release();
		sobel2.release();
		reducedTmp.release();
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		updateMode();
		double horizWeight = conf.getFloat("horizWeight");
		double vertWeight = 1.0 - horizWeight;
		int resolution = conf.getInteger("resolution");
		float maxAngle = conf.getFloat("maxAngle");
		float eps = conf.getFloat("eps");
		double fromAngle = -maxAngle, toAngle = maxAngle;
		String criterion = conf.getString("criterion");
		boolean doHoriz = criterion.equals("horiz") || criterion.equals("sum");
		boolean doVert = criterion.equals("vert") || criterion.equals("sum");
		Mat content = indy.getContent();
		Mat contentTranspose = content.t();
		Mat contentFlip = new Mat(content.size(), content.type());
		Core.flip(content, contentFlip, 1);
		Mat reduced = Mat.zeros(resolution, 1, CvType.CV_32FC1);
		Mat reducedHoriz = Mat.zeros(resolution, 1, CvType.CV_32FC1);
		Mat reducedVert = Mat.zeros(resolution, 1, CvType.CV_32FC1);
		double degree = 0.0;
		double maxQuantErr = 0.0;
		do { // Iteratively refine upper and lower boundary for skew angle 
			if (doHoriz) {
				getReducedVector(
					reducedHoriz, contentTranspose, fromAngle, toAngle, resolution);
			}
			if (doVert) {
				getReducedVector(
					reducedVert, contentFlip, fromAngle, toAngle, resolution);
			}
			Core.addWeighted(reducedHoriz, horizWeight,
				reducedVert, vertWeight, 0.0, reduced);
			MinMaxLocResult minMax = Core.minMaxLoc(reduced);
			degree = fromAngle + 
				(toAngle - fromAngle) * minMax.maxLoc.y / (resolution-1);
			maxQuantErr = (toAngle - fromAngle) / resolution / 2.0;
			Logger.getSingleton().debug("Interim skew for " 
				+ indy.getPopulation().getInputFileName() + " in ["
				+ fromAngle + "," + toAngle + "]: " + degree);
			fromAngle = degree - 3.0 * maxQuantErr;
			toAngle = degree + 3.0 * maxQuantErr;
		} while (maxQuantErr >= eps);
		if (conf.getInteger("refine") > 0) { // Enforce multipliers of 0.01
			fromAngle = Math.round(100.0 * degree)/100.0 - 0.5;
			toAngle = fromAngle + 1.0;
			if (doHoriz) {
				getReducedVector(
					reducedHoriz, contentTranspose, fromAngle, toAngle, 101);
			}
			if (doVert) {
				getReducedVector(
					reducedVert, contentFlip, fromAngle, toAngle, 101);
			}
			Core.addWeighted(reducedHoriz, horizWeight,
					reducedVert, vertWeight, 0.0, reduced);
			MinMaxLocResult minMax = Core.minMaxLoc(reduced);
			degree = fromAngle + minMax.maxLoc.y / 100.0;
		}
		Logger.getSingleton().debug("Skew for " 
			+ indy.getPopulation().getInputFileName() + " detected: " + degree);
		reduced.release();
		reducedHoriz.release();
		reducedVert.release();
		contentTranspose.release();
		contentFlip.release();
		
		Individual child = new Individual();
		if (conf.getInteger("noCorrection") == 0) { // Rotate image
			Mat dst = new Mat(content.rows(), content.cols(), content.type());
			Mat r = Imgproc.getRotationMatrix2D(new Point(content.cols()/2,
					content.rows()/2), -degree, 1.0);
			Imgproc.warpAffine(conf.getInteger("reloadOriginal") == 0 ? content 
					: Highgui.imread(indy.getPopulation().getInputFileName()),
				dst, r, content.size());
			child.setContent(dst);
			content.release();
		} else {
			child.setContent(content);
		}
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		return children;
	}
	
	/* (non-Javadoc)
	 * Update the {@link #mode} class variable according configuration.
	 */
	private void updateMode() {
		String paramLineMode = conf.getString("houghLineMode");
		mode = ExtentedImgproc.SCALING;
		if (paramLineMode.equals("bresenham")) {
			mode = ExtentedImgproc.BRESENHAM;
		} else if (paramLineMode.equals("exact")) {
			mode = ExtentedImgproc.EXACT;
		} else if (paramLineMode.equals("scaling")) {
			mode = ExtentedImgproc.SCALING;
		} else {
			Logger.getSingleton().warn("Unknown houghLineMode. Fall back to SCALING");
		}
	}
}
