package qa.qcri.prepocressor.operations;

import java.util.List;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.datastructures.Population;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * Super class for all operations. Defines an interface between the pipeline
 * and the operations and handles basic command line argument parsing.
 * 
 * @author Felix Stahlberg
 */
public abstract class Operation {

	/**
	 * Name of the operation.
	 */
	protected String name;
	
	/**
	 * Configuration instance for this operation
	 */
	protected Configuration conf;
	
	/**
	 * Sole constructor.
	 * 
	 * @param name Name of the operation
	 */
	public Operation(String name) {
		this.name = name;
		conf = createConfiguration();
		conf.restore();
	}

	/**
	 * Get a {@link Configuration} instance for this operation. Child classes
	 * can specify their own parameter set by inheriting Configuration and 
	 * implementing {@link Configuration#addDefaultParameters()}. This method
	 * is called once in the constructor.
	 * 
	 * @return Configuration instance
	 */
	protected abstract Configuration createConfiguration();
	
	/**
	 * Parse the command line arguments.
	 * 
	 * @param argv string array with command line arguments
	 * @see Configuration#processCommandLineArguments(String[])
	 */
	public void configure(String[] argv) {
		conf.processCommandLineArguments(argv);
		conf.dumpToFilesystem();
	}
	
	/**
	 * Get the configuration instance for this operation.
	 * 
	 * @return the configuration of this operation
	 */
	public Configuration getConfiguration() {
		return conf;
	}

	/**
	 * Process the given population and create a new population with the
	 * processed individuals.
	 * 
	 * @param pop input population
	 * @return processed population
	 * @see #processIndividual(Individual)
	 */
	public Population processPopulation(Population pop) {
		Population trgtPopulation = new Population(pop.getInputFileName());
		for (Individual indy : pop) {
			String idxPrefix = indy.getIdxPrefix();
			List<Individual> children = processIndividual(indy);
			if (children.size() == 1) {
				Individual child = children.get(0);
				child.setIdxPrefix(idxPrefix);
				if (!child.hasParent()) {
					child.setParent(
						indy.getParent(), indy.getParentRect());
				}
			} else {
				int idx = 0;
				for (Individual child : children) {
					if (child.getIdxPrefix().endsWith("_child")) {
						child.setIdxPrefix(idxPrefix + "_" 
								+ generateId(idx++));
					}
					if (!child.hasParent()) {
						Logger.getSingleton().warn("Multiple children, but no"
							+ " parent reference.");
					}
				}
			}
			trgtPopulation.addAll(children);
		}
		return trgtPopulation;
	}
	
	/**
	 * Generate string representation of ID given the numerical value. This 
	 * means adding 0s if the length does not match the idLength parameter.
	 * 
	 * @param idx the numerical id
	 */
	public static String generateId(int idx) {
		String strIdx = "" + idx;
		int idLength = Configuration.getGlobalConfiguration().getInteger(
				"idLength");
		while (idLength > strIdx.length()) {
			strIdx = "0" + strIdx;
		}
		return strIdx;
	}
	
	/**
	 * Process the given individual and write the result(s) to 
	 * {@link #trgtPopulation}. GoF template method.
	 * 
	 * @param indy individual to process
	 * @return list of processed individuals (e.g. an individual can be split
	 * 	up into smaller pieces)
	 */
	protected abstract List<Individual> processIndividual(Individual indy);
}
