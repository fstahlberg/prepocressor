package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the drawTextLine operation. Note: this operation 
 * expect a set of text lines as input but no image. A set of text lines in
 * prepocressor is represented as one dimensional {@link CvType#CV_32FC4} 
 * matrix storing the text lines one after the other. the 4 elements in each
 * cell are:
 * rho (line in polar coordinates)
 * angle (line in polar coordinates)
 * dir (text skew direction (if no skew, this is orthogonal to angle))
 * to-x (line segment reaches from previous to-x to this value)
 * 
 * @author Felix Stahlberg
 */
public class DrawTextLinesOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public DrawTextLinesOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("drawSkewLine", 1,
					"Draw the skew line. Set to 0 to ignore the text skew "
					+ "information");
			}
			@Override
			protected String getDescription() {
				return "Reloads the original input images and draws text lines"
					+ " in it for visual verification. The text lines must be "
					+ "in the pipeline, e.g. produced by the houghTextLine "
					+ "operation.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat dst = Highgui.imread(indy.getPopulation().getInputFileName());
		Mat content = indy.getContent();
		int nSegments = content.cols();
		int fromX = 0;
		for (int i = 0; i < nSegments; i++) {
			double rho = content.get(0, i)[0];
			double angle = content.get(0, i)[1];
			double dir = content.get(0, i)[2];
			int toX = (int) ((i == nSegments - 1) ? dst.cols()
					: content.get(0, i)[3]);
			double a = Math.cos(angle), b = Math.sin(angle);
			double x0 = a * rho, y0 = b * rho;
			Point pt1 = new Point(fromX, y0 + a * (x0 - fromX) / b);
			Point pt2 = new Point(toX, y0 + a * (x0 - toX) / b);
			Core.line(dst, pt1, pt2, new Scalar(255, 0, 0), 2);
			int skewX = (toX + fromX) / 2;
			int skewY = (int) (y0 + a * (x0 - skewX) / b);
			double skewA = Math.cos(dir), skewB = Math.sin(dir);
			Point skewPt1 = new Point(skewX, skewY);
			Point skewPt2 = new Point(skewX + skewA * 100, skewY + skewB * 100);
			Core.line(dst, skewPt1, skewPt2, new Scalar(0, 255, 0), 2);
			fromX = toX;
		}
		Individual child = new Individual();
		child.setContent(dst);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		return children;
	}
}
