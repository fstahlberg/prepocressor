package qa.qcri.prepocressor.operations;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.imageprocessing.Geometry;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the vertTextSegmentation operation.
 * 
 * @author Felix Stahlberg
 */
public class VertTextSegmentationOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public VertTextSegmentationOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("minSlope", 0.25f,
					"Minimal gradient in the vertical projection of a text "
					+ "area. The values of the vertical projection range "
					+ "from 0 to 1, and the projection is resized to "
					+ "-resolution");
				addParameter("minWidth", 0.1f,
					"Minimal width of a text area relative to the page width.");
				addParameter("dilate", 0.05f,
					"Safety margin which is added to the found text segments, "
					+ "relative to the page width. Set to negative to disable "
					+ "dilation.");
				addParameter("outputSegmentation", 0,
					"Output an 1xresolution array indicating the classification"
					+ " of the columns in text and non-text columns. If this "
					+ "is set to 0, the input image is cutted according the "
					+ "text segmentations.");
				addParameter("minMargin", 0.05f,
					"Minimal vertical distance between two text areas "
					+ "relative to the page width.");
				addParameter("transpose", 0,
					"Set to 1 to transpose the image before applying segmentation "
					+ "algorithm. This results producing horizontal instead of "
					+ "vertical cuts.");
				addParameter("resolution", 100,
					"Resolution for the vertical projection. 100 is a good "
					+ "value even for documents with largely differing sizes.");
				addParameter("morph", "openFirst",
					"Morphology operations on the segmentation.\n+\n"
					+ "* openFirst: opening, then closing\n"
					+ "* closeFirst: closing, then opening\n"
					+ "* none: No morphology operation.\n-");
				addParameter("concatChildren", 0,
					"Set to 0 to pass each found text segment separately "
					+ "through the pipeline. Set to 1 to concat all children "
					+ "to a single image removing the non-text areas.");
				addParameter("symmetric", 0,
					"Set to 1 to ensure that the middle of the input image "
					+ "corresponds to the middle of the output image. Useful "
					+ "for preserving the baseline position. Only applies "
					+ "if the number of children equals one (e.g. enforced "
					+ "by -concatChildren)");
			}
			@Override
			protected String getDescription() {
				return "Extracts vertical cuts of text areas. The areas are "
					+ "identified by a constant horizontal gradient of the "
					+ "vertical projection.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		int res = conf.getInteger("resolution");
		boolean transpose = conf.getInteger("transpose") > 0;
		Mat work = transpose ? content.t() : content;
		boolean[] segmentation = getSegmentation(work);
		List<Individual> children = new LinkedList<Individual>();
		if (segmentation == null) {
			if (transpose) {
				work.release();
			}
			children.add(indy);
			return children;
		}
		// Cut image
		if (conf.getInteger("outputSegmentation") > 0) {
			Individual child = new Individual();
			child.setContent(generateSegmentationImage(segmentation));
			children.add(child);
			content.release();
			work.release();
			return children;
		}
		int lastA = 0;
		int oldMiddle = work.cols()/2;
		int newMiddle = oldMiddle;
		for (int from = 0; from < res; from++) {
			if (segmentation[from]) {
				int to = from+1;
				for (; to < res; to++) {
					if (!segmentation[to]) {
						break;
					}
				}
				int a = (int) (((double) from)/((double) res)*work.cols());
				int b = (int) (((double) to)/((double) res)*work.cols());
				if (a <= oldMiddle) {
					newMiddle -= (a - lastA);
				}
				if (transpose) {
					children.add(indy.produceChild(new Rect(
						new Point(0, a), new Point(work.rows(),	b))));
				} else {
					children.add(indy.produceChild(new Rect(
						new Point(a, 0), new Point(b, work.rows()))));
				}
				from = to;
			}
		}
		if (conf.getInteger("concatChildren") > 0 && children.size() > 1) {
			Individual child = new Individual();
			Mat childContent = transpose ? concatChildrenVertically(indy, children)
					: concatChildrenHorizontally(indy, children);
			child.setContent(childContent);
			children.clear();
			children.add(child);
			content.release();
		} else if (!transpose) {
			Collections.reverse(children); // Because of arabic right-to-left
		}
		if (transpose) {
			work.release();
		}
		if (conf.getInteger("symmetric") > 0 && children.size() == 1) {
			Individual child = children.get(0);
			child.setContent(symmetrize(
					child.getContent(), newMiddle, transpose));
		}
		// TODO: Release content?
		content.release();
		return children;
	}
	
	/* (non-Javadoc)
	 * Helper method for symmetrizing the image. Called if symmetric=1
	 */
	private Mat symmetrize(Mat content, int curMiddle, boolean transpose) {
		int top = 0;
		int left = 0;
		int right = 0;
		int bottom = 0;
		if (transpose) {
			int middle = content.rows()/2;
			if (curMiddle < middle) {
				top = content.rows() - 2*curMiddle;
			} else {
				bottom = 2*curMiddle - content.rows();
			}
		} else { // no transpose
			int middle = content.cols()/2;
			if (curMiddle < middle) {
				left = content.cols() - 2*curMiddle;
			} else {
				right = 2*curMiddle - content.cols();
			}
		}
		if (top + left + right  + bottom <= 0) {
			return content;
		}
		Mat withBorder = new Mat(content.size(), content.type());
		Imgproc.copyMakeBorder(content, withBorder, top, bottom, left, right,
				Imgproc.BORDER_CONSTANT);
		content.release();
		return withBorder;
	}
	
	/* (non-Javadoc)
	 * Translates bool array to OpenCV Mat
	 */
	private Mat generateSegmentationImage(boolean[] segmentation) {
		Mat img = Mat.zeros(new Size(segmentation.length, 1), CvType.CV_8UC1);
		for (int i = 0; i < segmentation.length; i++) {
			if (segmentation[i]) {
				img.put(0, i, 1);
			}
		}
		return img;
	}
	
	/* (non-Javadoc)
	 * Get bounding rectangle for all child regions
	 */
	private Rect getBoundaryBox(List<Individual> children) {
		Point p1 = new Point(1000000, 1000000);
		Point p2 = new Point(-1000000, -1000000);
		for (Individual child : children) {
			Rect childRect = child.getParentRect();
			p1.x = Math.min(p1.x, childRect.x);
			p1.y = Math.min(p1.y, childRect.y);
			p2.x = Math.max(p2.x, childRect.x + childRect.width);
			p2.y = Math.max(p2.y, childRect.y + childRect.height);
		}
		return new Rect(p1, p2);
	}
	
	/* (non-Javadoc)
	 * Combines children into a single image
	 */
	private Mat concatChildrenHorizontally(Individual indy,
			List<Individual> children) {
		Mat content = indy.getContent();
		Rect bBox = getBoundaryBox(children);
		int width = 0;
		for (Individual child : children) {
			width += child.getContent().cols();
		}
		Mat dst = Mat.zeros(content.rows(), width, content.type());
		int border = 0;
		for (Individual childIndy : children) {
			Mat child = childIndy.getContent();
			Mat tmp = new Mat(child.rows(), child.cols(), child.type());
			child.copyTo(tmp);
			Mat tmpBorder = new Mat(content.rows(), width, content.type());
			Imgproc.copyMakeBorder(tmp, tmpBorder, 0, 0,
					border, width - border - child.cols(),
					Imgproc.BORDER_CONSTANT);
			Core.add(dst, tmpBorder, dst);
			border += child.cols();
			tmp.release();
			tmpBorder.release();
			child.release();
		}
		Individual child = new Individual();
		child.setContent(dst);
		child.setParent(indy, bBox);
		return dst;
	}
	
	/* (non-Javadoc)
	 * Combines children into a single image
	 */
	private Mat concatChildrenVertically(Individual indy,
			List<Individual> children) {
		Mat content = indy.getContent();
		Rect bBox = getBoundaryBox(children);
		int height = 0;
		for (Individual child : children) {
			height += child.getContent().rows();
		}
		Mat dst = Mat.zeros(height, content.cols(), content.type());
		int border = 0;
		for (Individual childIndy : children) {
			Mat child = childIndy.getContent();
			Mat tmp = new Mat(child.rows(), child.cols(), child.type());
			child.copyTo(tmp);
			Mat tmpBorder = new Mat(height, content.cols(), content.type());
			Imgproc.copyMakeBorder(tmp, tmpBorder, border, height - border - child.rows(),
					0, 0,
					Imgproc.BORDER_CONSTANT);
			Core.add(dst, tmpBorder, dst);
			border += child.rows();
			tmp.release();
			tmpBorder.release();
			child.release();
		}
		Individual child = new Individual();
		child.setContent(dst);
		child.setParent(indy, bBox);
		return dst;
	}
	
	/* (non-Javadoc)
	 * get segmentation
	 */
	private boolean[] getSegmentation(Mat content) {
		Mat red = new Mat(content.rows(), content.cols(), CvType.CV_32FC1);
		Core.reduce(content, red, 0, Core.REDUCE_SUM, CvType.CV_32FC1);
		Mat rectSum = Geometry.rectSum(red, 0, 0, 1, red.cols());
		if (rectSum.cols() * rectSum.rows() == 0) {
			Logger.getSingleton().warn("Accumulation error in vertTextSegmentation"
				+ " (not enough foreground?)");
			rectSum.release();
			red.release();
			return null;
		}
		Mat threshold = new Mat(content.rows(), content.cols(), CvType.CV_8UC1);
		Mat accSobel = new Mat(content.rows(), content.cols(), CvType.CV_32FC1);
		Mat accNorm = new Mat(content.rows(), content.cols(), CvType.CV_32FC1);
		Mat acc100 = new Mat(content.rows(), content.cols(), CvType.CV_32FC1);
		int res = conf.getInteger("resolution");
		Imgproc.resize(rectSum,
			acc100, new Size(res, 1), 0, 0, Imgproc.INTER_LINEAR);
		Core.normalize(acc100, accNorm, 0.0, 1.0, Core.NORM_MINMAX);
		Imgproc.Sobel(accNorm, accSobel, -1,
				1, 0, 3, 1, 0, Imgproc.BORDER_REFLECT);
		Imgproc.threshold(accSobel, threshold, conf.getFloat("minSlope"), 255,
				Imgproc.THRESH_BINARY);
		boolean[] segmentation = new boolean[res];
		for (int i = 0; i < segmentation.length; i++) {
			segmentation[i] = threshold.get(0, i)[0] > 0.5;
		}
		red.release();
		acc100.release();
		accNorm.release();
		accSobel.release();
		rectSum.release();
		threshold.release();
		if (!conf.getString("morph").equals("none")) {
			int closeSize = (int) (conf.getFloat("minMargin")*res);
			int openSize = (int) (conf.getFloat("minWidth")*res);
			if (conf.getString("morph").equals("closeFirst")) {
				segmentation = removeShortSections(segmentation, false, closeSize);
				segmentation = removeShortSections(segmentation, true, openSize);
			} else {
				segmentation = removeShortSections(segmentation, true, openSize);
				segmentation = removeShortSections(segmentation, false, closeSize);
			}
		}
		int dilateSize = (int) (res*conf.getFloat("dilate"));
		if (dilateSize > 0) {
			for (int i = 0; i < res; i++) {
				if (segmentation[i]) {
					for (int j = Math.max(0, i - dilateSize);
							j < Math.min(res, i + dilateSize + 1); j++) {
						segmentation[j] = true;
					}
				}
			}
		}
		return segmentation;
	}
	
	/* (non-Javadoc)
	 * Remove consecutive sections in array which are val and less then
	 * minWidth wide
	 */
	private boolean[] removeShortSections(boolean[] a, boolean val, int minWidth) {
		for (int i = 0; i < a.length; i++) {
			if (a[i] == val) {
				int start = i;
				for (; i < a.length; i++) {
					if (a[i] != val) {
						break;
					}
				}
				if (!val) { // If removing 0 sections
					if (start == 0 || i == a.length) { // Skip border sections
						continue;
					}
				}
				if (i - start < minWidth) {
					for (int j = start; j < i; j++) {
						a[j] = !val;
					}
				}
			}
		}
		return a;
	}
}
