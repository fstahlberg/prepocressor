package qa.qcri.prepocressor.operations;

import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.datastructures.Population;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the drawChildren operation. In contrast to the most
 * operations, this class needs to override the {@link #processPopulation(Population)}
 * method and breaks with the GoF template method pattern.
 * 
 * @see #processPopulation(Population)
 * @see #processIndividual(Individual)
 * @author Felix Stahlberg
 */
public class DrawChildrenOperation extends Operation {
	
	final Scalar[] colors = new Scalar[]{
			new Scalar(255, 0, 0),
			new Scalar(0, 255, 0),
			new Scalar(0, 0, 255)};

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public DrawChildrenOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("thickness", 2,
					"Thickness of the rectangle. Negative for filled "
					+ "rectangles.");
				addParameter("transpose", 0,
						"Transpose original.");
			}
			@Override
			protected String getDescription() {
				return "Reloads the input image of the given population and "
					+ "draws all children in the population with rectangles.";
			}
		};
	}
	
	

	/**
	 * Reloads the input image of the given population and draws all children
	 * in the population with rectangles.
	 * 
	 * @param pop population
	 * @throws NullPointerException if pop or pop.inputFile is null
	 * @return single element population with the augmented input image
	 */
	@Override
	public Population processPopulation(Population pop) {
		int thickness = conf.getInteger("thickness");
		Individual indy = new Individual();
		indy.loadContent(pop.getInputFileName());
		Mat content = indy.getContent();
		if (conf.getInteger("transpose") > 0) {
			content = content.t();
		}
		for (Individual child : pop) {
			if (child.hasParent()) {
				Rect rect = child.getParentRect();
				int x = rect.x;
				int y = rect.y;
				Individual parent = child.getParent();
				int cnt = 0;
				while (parent.hasParent()) {
					Rect thisRect = parent.getParentRect();
					x += thisRect.x;
					y += thisRect.y;
					cnt++;
					parent = parent.getParent();
				}
				Core.rectangle(content,
					new Point(x, y),
					new Point(x+rect.width, y+rect.height),
					colors[cnt % colors.length],
					thickness);
			}
		}
		Population newPop = new Population(pop.getInputFileName());
		indy.setContent(content);
		newPop.add(indy);
		return newPop;
	}

	/**
	 * @throws UnsupportedOperationException unconditionally
	 * @see #processPopulation(Population)
	 */
	@Override
	protected List<Individual> processIndividual(Individual indy) {
		throw new UnsupportedOperationException();
	}
}
