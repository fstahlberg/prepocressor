package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the adaptiveThreshold operation.
 * 
 * @author Felix Stahlberg
 */
public class AdaptiveThresholdOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public AdaptiveThresholdOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("blockSize", 12,
					"Block size (passed through to OpenCV).");
				addParameter("C", 2.0f,
						"C constant (passed through to OpenCV).");
				addParameter("maxVal", 255,
					"Lowest possible value (passed through to OpenCV).");
				addParameter("adaptiveType", "MEAN_C",
					"Adaptive thresholding method. See OpenCVs documentation for "
					+ "the adaptiveThreshold function. Available values are "
					+ "MEAN_C or GAUSSIAN_C");
				addParameter("type", "BINARY",
					"Thresholding type. See OpenCVs documentation for the "
					+ "threshold function. Connect options with ','. Available"
					+ " options are: BINARY, BINARY_INV, TRUNC, TOZERO, "
					+ "TOZERO_INV, OTSU");
			}
			@Override
			protected String getDescription() {
				return "The adaptiveThreshold command creates binary images. It"
					+ "is based on the OpenCV function adaptiveThreshold().";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		Imgproc.adaptiveThreshold(content, content, conf.getInteger("maxValue"), 
			getAdaptiveType(), ThresholdOperation.getType(conf), 
			conf.getInteger("blockSize"), conf.getFloat("C"));
		Individual child = new Individual();
		child.setContent(content);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		return children;
	}
	
	private int getAdaptiveType() {
		if (conf.getString("adaptiveType").equals("MEAN_C")) {
			return Imgproc.ADAPTIVE_THRESH_MEAN_C;
		}
		return Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C;
	}
}
