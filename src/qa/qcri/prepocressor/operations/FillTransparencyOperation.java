package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.highgui.Highgui;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the fillTransparency operation.
 * 
 * @author Felix Stahlberg
 */
public class FillTransparencyOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public FillTransparencyOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				;
			}
			@Override
			protected String getDescription() {
				return "Loads the original image, fetches the alpha channel "
					+ "and fills transparent areas in the current image "
					+ "in the pipeline with zero.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		Mat original = Highgui.imread(indy.getPopulation().getInputFileName(), -1);
		Mat dst = Mat.zeros(content.size(), content.type());
		List<Mat> orgChannels = new LinkedList<Mat>();
		Core.split(original, orgChannels);
		if (orgChannels.size() < 4) {
			Logger.getSingleton().error("Original image does not have alpha "
				+ "channel! Pass thru image in pipeline unmodified");
			dst.release();
			dst = content;
		} else {
			Core.add(dst, content, dst, orgChannels.get(3));
			content.release();
		}
		for (Mat ch : orgChannels) {
			ch.release();
		}
		original.release();

		Individual child = new Individual();
		child.setContent(dst);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		return children;
	}
}
