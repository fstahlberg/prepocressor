package qa.qcri.prepocressor.operations;

import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Rect;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.datastructures.Population;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the concat operation. In contrast to the most
 * operations, this class needs to override the {@link #processPopulation(Population)}
 * method and breaks with the GoF template method pattern.
 * 
 * @see #processPopulation(Population)
 * @see #processIndividual(Individual)
 * @author Felix Stahlberg
 */
public class ConcatOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public ConcatOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("center", 1,
					"Set to 0 if the images should not be centered in case of "
					+ "different heights");
			}
			@Override
			protected String getDescription() {
				return "Concatinate all images of one population horizontally.";
			}
		};
	}
	
	

	/**
	 * Combine all images in the population in a single image by horizontally
	 * concatenating them
	 * 
	 * @param pop population
	 * @throws NullPointerException if pop or pop.inputFile is null
	 * @return single element population with the augmented input image
	 */
	@Override
	public Population processPopulation(Population pop) {
		if (pop.size() == 0) {
			return pop;
		}
		boolean center = (conf.getInteger("center") != 0);
		int width = 0;
		int height = 0;
		int type = 0;
		for (Individual child : pop) {
			Mat content = child.getContent();
			width += content.cols();
			height = Math.max(height, content.rows());
			type = content.type();
		}
		Mat dst = Mat.zeros(height, width, type);
		int fromX = 0;
		for (Individual child : pop) {
			Mat content = child.getContent();
			int childWidth = content.cols();
			int childHeight = content.rows();
			Rect roi = new Rect(fromX,
				center ? ((height-childHeight)/2) : 0,
				childWidth, childHeight);
			content.copyTo(dst.submat(roi));
			content.release();
			fromX += childWidth;
		}
		Population newPop = new Population(pop.getInputFileName());
		Individual indy = new Individual();
		indy.setContent(dst);
		newPop.add(indy);
		return newPop;
	}

	/**
	 * @throws UnsupportedOperationException unconditionally
	 * @see #processPopulation(Population)
	 */
	@Override
	protected List<Individual> processIndividual(Individual indy) {
		throw new UnsupportedOperationException();
	}
}
