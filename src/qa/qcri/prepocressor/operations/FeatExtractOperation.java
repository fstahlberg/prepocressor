package qa.qcri.prepocressor.operations;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.datastructures.Population;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.operations.feat.ANHDFFeatureExtractor;
import qa.qcri.prepocressor.operations.feat.ConcavityFeatureExtractor;
import qa.qcri.prepocressor.operations.feat.DirectionalFeatureExtractor;
import qa.qcri.prepocressor.operations.feat.DistributionFeatureExtractor;
import qa.qcri.prepocressor.operations.feat.FeatureExtractor;
import qa.qcri.prepocressor.operations.feat.RawFeatureExtractor;
import qa.qcri.prepocressor.operations.feat.RunlengthsFeatureExtractor;
import qa.qcri.prepocressor.operations.feat.SnakeFeatureExtractor;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the featExtract operation. The feature extractors 
 * themselves are implemented in the {@link qa.qcri.prepocressor.operations.feat}
 * package. Note that the kaldi file is generated incrementally: Each operation
 * call adds its individual to the file. Therefore, it is important that only
 * one instance of this operation is instantiated.
 * 
 * @author Felix Stahlberg
 */
public class FeatExtractOperation extends Operation {
	
	private static Object SEM = new Object();
	private int[] delays = new int[0];
	private boolean delayRaw, delayDelta, centerFrames;
	private int frameWidth, frameShift;
	
	
	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public FeatExtractOperation(String name) {
		super(name);
	}

	/**
	 * Deletes kaldiFile.
	 * {@inheritDoc}
	 */
	@Override
	public void configure(String[] argv) {
		super.configure(argv);
		String kaldiFile = conf.getString("kaldiFile");
		File file = new File(kaldiFile);
		if (file.exists()) {
			try {
			    file.delete();
			} catch (Exception e) {
			    Logger.getSingleton().warn("Error while cleaning kaldi file: "
			    	+ e.getMessage());
			}
		}
		delayRaw = conf.getInteger("delayRaw") > 0;
		delayDelta = conf.getInteger("delayDelta") > 0;
		String delayParam = conf.getString("delays");
		if (delayParam.isEmpty()) {
			return;
		}
		String[] strDelays = delayParam.split(",");
		delays = new int[strDelays.length];
		try {
			for (int i = 0; i < strDelays.length; i++) {
				delays[i] = Integer.parseInt(strDelays[i]);
				if (delays[i] <= 0) {
					Logger.getSingleton().warn("Delta delays must be positive");
					delays = new int[0];
					return;
				}
			}
		} catch (NumberFormatException e) {
			Logger.getSingleton().warn("Malformed deltas parameter. Must be comma "
				+ "separated list of integers or empty");
			delays = new int[0];
		}
		if (Configuration.getGlobalConfiguration().getInteger("nThreads") > 1) {
			Logger.getSingleton().warn("Using -nThreads > 1 can cause "
				+ "reorderings in the kaldi feature file. To make sure that "
				+ "the order in the feature files is the same as in -inputFile"
				+ " set nThreads=1.");
		}
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("kaldiId", "%base",
					"This string specifies how the kaldi ID is generated. You"
					+ " can use the same placeholders as in outputPath.");
				addParameter("kaldiFile", "kaldi.ark,t",
					"Path to the kaldi feature file to generate. This is a "
					+ "feature table in text format. See kaldis copy-feat "
					+ "tool with ark,t specifiers for more information.");
				addParameter("winWidth", 3,
					"Width of the sliding window.");
				addParameter("winShift", 2,
					"Shift of the sliding window.");
				addParameter("extractors", "raw",
					"Comma separated list of feature extractors. Available:\n+\n"
					+ "* 'raw': Raw pixel values.\n"
					+ "* 'directional': Directional features.\n"
					+ "* 'snake': Snake feature extraction method (also called "
						+ "segment-based method in (Stahlberg and Vogel, 2015))\n"
					+ "* 'runlengths': Use pixel-wise runlengths in 4 directions\n"
					+ "* 'anhdf': ANHDF features (El-Mahallawy, 2008)\n"
					+ "* 'distribution': Distribution features as defined by "
						+ " (Likforman-Sulem et. al., 2012)\n"
					+ "* 'concavity': Concavity features as defined by "
						+ " (Likforman-Sulem et. al., 2012)\n-\n");
				addParameter("centerFrames", 0,
					"Set this to 1 to enable vertically repositioned windows "
					+ "based on the center of gravity. This can help to better "
					+ "handle vertically translated letters. See (Doetsch et. "
					+ "al., 2012) for more information. Additionally, the "
					+ "vertical shift of each window is added as feature to "
					+ "the feature vectors if centerFrames=1.");
				addParameter("delays", "",
					"Comma-separated list of integers specifying the deltas to add. "
					+ "The integers are the delta distances, i.e. '1' stands for "
					+ "standard deltas, '2' calculates deltas to second last "
					+ "feature vector.");
				addParameter("delayRaw", 0,
					"Set to positive value to add raw feature vectors according "
					+ "-delays (feature staking)");
				addParameter("delayDelta", 1,
					"Set to positive value to add deltas of feature vectors according "
					+ "-delays");
				addParameter("baselineHeight", 32,
					"Height of the baseline for baseline dependent features.");
				addParameter("featRawCellHeight", 1,
					"Height of the cell for the raw feature extractor.");
				addParameter("featRawCellWidth", 1,
					"Height of the cell for the raw feature extractor.");
				addParameter("featRawCellShift", 1,
					"Vertical cell shift for the raw feature extractor.");
				addParameter("featAnhdfConnecticityTolerance", 4,
					"Tolerance for segments in the ANHDF feature to be "
					+ "connected. See (El-Mahallawy, 2008) PhD thesis "
					+ "for more information.");
				addParameter("featAnhdfSegmentNum", 4,
					"Number of segments in ANHDF features. 4 is also used "
					+ "by (El-Mahallawy, 2008) and reasonable for Arabic.");
				addParameter("foregroundThreshold", 0.5f,
					"Pixel values above this threshold are considered as "
					+ "foreground.");
				addParameter("featSnakeNumber", 6,
					"Number of snakes.");
				addParameter("featSnakeAddRelativeFeats", 0,
					"Add snake features divided by height of entire slice");
				addParameter("featSnakeAddCenterDistances", 0,
					"Add distance between consecutive segment centers as features.");
				addParameter("featSnakeForeground", 1,
					"Set to positive value to use foreground snakes.");
				addParameter("featSnakeBackground", 0,
					"Set to positive value to use background snakes.");
				addParameter("featSnakeDefaultHeight", 0,
					"Default value for height features which is used in silence.");
				addParameter("featRunlengthsRadius", 10,
					"Radius for runlength feature extractor (maximum feature "
					+ "value)");
				addParameter("featDirectionalRadius", 10,
					"Radius for directional feature extractor (maximum feature "
					+ "value)");
				addParameter("featRunlengthsNonNegative", 1,
					"Set to positive value to avoid using negative values for "
					+ "background pixel runlengths (use 0 instead)");
				addParameter("featAnhdfReductionMode", "max",
					"ANHDF features are defined for windows with 1 pixel width."
					+ " Wider windows a reduced according to this method:\n+\n"
					+ "* 'max': Take the maximum of each row.\n"
					+ "* 'min': Take the minimum of each row.\n"
					+ "* 'average': Take the average of each row.\n"
					+ "* 'firstAndLast': Use right most column for slice i and "
					+ "left most column of previous slice as i-1.\n-");
				addParameter("featConcavityBaselineDependence", 1,
					"Extract also concavity separately for above and below "
					+ "baseline.");
				
			}
			@Override
			protected String getDescription() {
				return "Feature extraction for Kaldi. The feat* parameters "
					+ "are extractor specific. NOTE: Feature extraction is "
					+ "based on a sliding window in right-to-left direction "
					+ "as this tool was initially developed for Arabic. If you "
					+ "wish to change direction, apply the flip operation first.";
			}
		};
	}

	/**
	 * Overrides {@link Operation#processPopulation(Population)} because it
	 * generates one file containing all individuals. The file is stored in
	 * kaldiFile. The ids are extracted according kaldiId.
	 * 
	 * @param pop population to 
	 */
	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		frameWidth = conf.getInteger("winWidth");
		frameShift = conf.getInteger("winShift");
		centerFrames = conf.getInteger("centerFrames") > 0;
		Mat input = content;
		int border = frameShift - (input.cols() - frameWidth) % frameShift;
		input = new Mat(content.rows(), content.cols() + border + frameWidth, content.type());
		Imgproc.copyMakeBorder(content, input, 0, 0, border, frameWidth,
				Imgproc.BORDER_CONSTANT);
		List<FeatureExtractor> extractors = getExtractors();
		List<List<Double>> featMatrix = new LinkedList<List<Double>>();
		int dim = -1;
		for (int frameStart = input.cols() - frameWidth; frameStart >= 0;
				frameStart -= frameShift) {
			List<Double> frameFeats = new LinkedList<Double>();
			Mat frame = getFrame(input, frameStart, frameFeats);
			for (FeatureExtractor extractor : extractors) {
				List<Double> extractorFeats = extractor.extractFeatures(input, frame);
				if (extractorFeats != null) {
					frameFeats.addAll(extractorFeats);
				}
			}
			frame.release();
			if (dim == -1) {
				dim = frameFeats.size();
			}
			if (dim != frameFeats.size()) {
				Logger.getSingleton().error("Frame differs in dimensionality ("
					+ "frameStart=" + frameStart + ") This frame is skipped, "
					+ "but recorded by feature extractors with memory.");
			} else {
				featMatrix.add(frameFeats);
			}
		}
		addDeltas(featMatrix);
		writeFeatureMatrix(indy, featMatrix);
		List<Individual> children = new LinkedList<Individual>();
		children.add(indy);
		return children;
	}
	
	/* (non-Javadoc)
	 * Get a frame starting from horizontal pixel position frameStart. Applies
	 * vertical repositioning and adds shift to feature vector if centerFrames
	 * is set to 1.
	 * 
	 * @param input line image
	 * @param frameStart frame position
	 * @param frameFeats feature vector
	 * @return frame
	 */
	private Mat getFrame(Mat input, int frameStart, List<Double> frameFeats) {
		int frameHeight = input.rows();
		Mat frame = input.submat(0, frameHeight, frameStart, 
				frameStart + frameWidth);
		if (!centerFrames) {
			return frame;
		}
		// Center frame
		Moments mom = Imgproc.moments(frame);
		if (mom.get_m00() < 0.0001) {
			frameFeats.add(0.0); // add shift=0
			return frame;
		}
		int center = (int) (mom.get_m01()/mom.get_m00());
		int shift = frameHeight/2 - center;
		if (center < 1 || center >= frameHeight || shift == 0) {
			frameFeats.add(0.0); // add shift=0
			return frame;
		}
		int copyHeight = frameHeight - Math.abs(shift);
		int fromRowStart = Math.max(0, -shift);
		int toRowStart = Math.max(0, shift);
		Mat centeredFrame = Mat.zeros(frame.size(), frame.type());
		frame.submat(fromRowStart, fromRowStart + copyHeight, 0, frameWidth)
			.copyTo(centeredFrame.submat(
					toRowStart, toRowStart + copyHeight, 0, frameWidth));
		frameFeats.add((0.0 + shift) / (0.0 + frameHeight)); // add shift
		return centeredFrame; // must be released by calling function
	}
	
	private void addDeltas(List<List<Double>> featMatrix) {
		int nDelays = delays.length;
		if (nDelays == 0 || featMatrix.isEmpty()) {
			return;
		}
		List<Iterator<List<Double>>> its = new ArrayList<Iterator<List<Double>>>();
		List<List<Double>> hist = new ArrayList<List<Double>>();
		List<Double> firstOrgFeat = new LinkedList<Double>();
		firstOrgFeat.addAll(featMatrix.get(0));
		for (int j = 0; j < nDelays; j++) {
			Iterator<List<Double>> it = featMatrix.iterator();
			it.next();
			hist.add(firstOrgFeat);
			its.add(it);
		}
		int i = 0;
		for (List<Double> feat : featMatrix) {
			List<Iterator<Double>> histIts = new LinkedList<Iterator<Double>>();
			// Update hist and iterators
			for (int j = 0; j < nDelays; j++) {
				if (i >= delays[j]) {
					hist.set(j, its.get(j).next());
				}
				histIts.add(hist.get(j).iterator());
			}
			i++;
			List<Double> orgFeat = new LinkedList<Double>();
			orgFeat.addAll(feat);
			// Add delay features
			for (double val : orgFeat) {
				for (Iterator<Double> histIt : histIts) {
					double histVal = histIt.next();
					if (delayRaw) {
						feat.add(histVal);
					}
					if (delayDelta) {
						feat.add(val - histVal);
					}
				}
			}
		}
	}

	/**
	 * Adds the features for the current individual to the feature file.
	 * 
	 * @param indy current individual
	 * @param featMatrix feature matrix
	 */
	protected void writeFeatureMatrix(Individual indy,
			List<List<Double>> featMatrix) {
		synchronized (SEM) {
			String kaldiFile = conf.getString("kaldiFile");
			String kaldiId = indy.getPopulation().getFullIndividualFileName(
					conf.getString("kaldiId"),
					indy.getIdxPrefix());
			PrintWriter out = null;
			try {
				BufferedWriter bf = new BufferedWriter(
						new FileWriter(kaldiFile, true));
			    out = new PrintWriter(bf);
			    out.println(kaldiId + " " + matrix2string(featMatrix));
			} catch (IOException e) {
			    Logger.getSingleton().warn("Could not write to " + kaldiFile + ": "
			    	+ e.getMessage());
			} finally {
				if (out != null) {
					out.close();
				}
			}
		}
	}
	
	/* (non-Javadoc)
	 * Returns the feature matrix in kaldi string representation. 
	 */
	private String matrix2string(List<List<Double>> featMatrix) {
		StringBuilder sb = new StringBuilder();
		sb.append(" [");
		for (List<Double> row : featMatrix) {
			sb.append("\n");
			for (Double val : row) {
				sb.append(" " + val);
			}
		}
		sb.append(" ]");
		return sb.toString();
	}
	
	/* (non-Javadoc)
	 * Get a list of all feature extractors according to the extractors 
	 * parameter.
	 */
	private List<FeatureExtractor> getExtractors() {
		List<FeatureExtractor> extractors = new LinkedList<FeatureExtractor>();
		String[] featNames = conf.getString("extractors").split(",");
		for (String featName : featNames) {
			if (featName.equals("raw")) {
				extractors.add(new RawFeatureExtractor(conf));
			} else if (featName.equals("snake")) {
				extractors.add(new SnakeFeatureExtractor(conf));
			} else if (featName.equals("runlengths")) {
				extractors.add(new RunlengthsFeatureExtractor(conf));
			} else if (featName.equals("anhdf")) {
				extractors.add(new ANHDFFeatureExtractor(conf));
			} else if (featName.equals("directional")) {
				extractors.add(new DirectionalFeatureExtractor(conf));
			} else if (featName.equals("concavity")) {
				extractors.add(new ConcavityFeatureExtractor(conf));
			} else if (featName.equals("distribution")) {
				extractors.add(new DistributionFeatureExtractor(conf));
			} else {
				Logger.getSingleton().warn("Feature extractor '" + featName
					+ "' is not implemented");
			}
		}
		return extractors;
	}
}
