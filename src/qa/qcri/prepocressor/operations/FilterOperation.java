package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the filter operation.
 * 
 * @author Felix Stahlberg
 */
public class FilterOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public FilterOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("minAspectRatio", 2.0f,
					"Images with width/height<minAspectRatio are removed.");
				addParameter("minHeight", 10,
						"Images smaller height (in pixel) are removed.");
			}
			@Override
			protected String getDescription() {
				return "Removes images that are likely to be no "
					+ "text lines.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		List<Individual> children = new LinkedList<Individual>();
		int height = content.rows();
		int width = content.cols();
		if (height < conf.getInteger("minHeight") || ((double) width)
				/((double) height) < conf.getFloat("minAspectRatio")) {
			content.release();
			return children;
		}
		if (indy.hasParent() && indy.getParentRect().height <= 1) {
			Logger.getSingleton().warn("Filtered because of small parent");
			content.release();
			return children;
		}
		children.add(indy);
		return children;
	}
}
