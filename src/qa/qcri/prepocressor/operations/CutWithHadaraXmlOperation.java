package qa.qcri.prepocressor.operations;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.primaresearch.maths.geometry.Polygon;
import org.primaresearch.maths.geometry.Rect;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the cutWithHadaraXml operation.
 * 
 * @author Felix Stahlberg
 */
public class CutWithHadaraXmlOperation extends Operation {
	
	private String zoneType;

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public CutWithHadaraXmlOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("xmlPath", "HADARA80P_v1.1.xml",
					"Path to the xml files in HADARA format. The same place"
					+ "holders as in the global outputPath can be used.");
				addParameter("id", "%base",
					"This is the id of the image tag corresponding to the "
					+ "current image. The same place"
					+ "holders as in the global outputPath can be used.");
				addParameter("cutLevel", "line",
					"Splitting level. Either 'line' or 'word'");
				addParameter("useZoneIds", 0,
					"Use id attributes in xml file for naming. Otherwise, use "
					+ "consecutive numbering (see global idLength parameter)");
				addParameter("useIndexAttributes", 0,
					"Set the prepocressor index to the value of the index "
					+ "attribute of the corresponding node in the xml file."
					+ "Note that this can lead to problems in combination "
					+ "with cutLevel=word because the same word level index "
					+ "might be used multiple times in a single xml file.");
			}
			@Override
			protected String getDescription() {
				return "This operation corresponds to cutWithPageXml but reads"
					+ " xml files in the format used by HADARA. See "
					+ "https://www.ifn.ing.tu-bs.de/research/data/HADARA80P "
					+ "for more information.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		String fileName = indy.getPopulation().getFullIndividualFileName(
				conf.getString("xmlPath"), indy.getIdxPrefix());
		String hadaraId = indy.getPopulation().getFullIndividualFileName(
				conf.getString("id"), indy.getIdxPrefix());
		zoneType = conf.getString("cutLevel").equals("word") ? "word" : "textline";
		Logger log = Logger.getSingleton();
		List<Individual> children = new LinkedList<Individual>();
		try {
			File fXmlFile = new File(fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			Node imgNode = getElementByTagNameAndAttribute(doc, 
				"image", "id", hadaraId);
			if (imgNode == null) {
				log.error("Image tag with id " + hadaraId + " not found");
				children.add(indy);
				return children;
			}
			Node contentNode = getElementByTagNameAndAttribute(doc,
				"content", "image_id", hadaraId);
			if (contentNode == null) {
				log.warn("No content node found for " + hadaraId);
			}
			addZones(hadaraId, indy, children, imgNode, contentNode);
			indy.getContent().release();
		} catch (ParserConfigurationException e) {
			log.error("Parser configuration exception for " + fileName
					+ ": " + e.getMessage());
			children.add(indy);
		} catch (SAXException e) {
			log.error("SAX exception for " + fileName
					+ ": " + e.getMessage());
			children.add(indy);
		} catch (IOException e) {
			log.error("File reading exception for " + fileName
					+ ": " + e.getMessage());
			children.add(indy);
		}
		return children;
	}
	
	/* (non-Javadoc)
	 * Get node with specific tag name and specific attribute
	 */
	private Node getElementByTagNameAndAttribute(Document doc, String tag,
			String attrKey, String attrVal) {
		Node node = null;
		NodeList nList = doc.getElementsByTagName(tag);
		for (int i = 0; i < nList.getLength(); i++) {
			Element el = (Element) nList.item(i);
			if (el.hasAttribute(attrKey) 
					&& el.getAttribute(attrKey).equals(attrVal)) {
				node = nList.item(i);
				break;
			}
		}
		return node;
	}
	
	
	/* (non-Javadoc)
	 * Add the zones in the xml as children to the pipeline
	 */
	private void addZones(String hadaraId, Individual indy, List<Individual> children,
			Node imgNode, Node contentNode) {
		String outputPath = Configuration.getGlobalConfiguration()
			.getString("outputPath");
		Mat content = indy.getContent();
		Map<String, String> transcriptions = loadTranscriptions(contentNode);
		NodeList zList = ((Element) imgNode).getElementsByTagName("zone");
		for (int i = 0; i < zList.getLength(); i++) {
			Element el = (Element) zList.item(i);
			if (!el.hasAttribute("type") || !el.hasAttribute("id")  
					|| !el.getAttribute("type").equals(zoneType)) {
				continue;
			}
			Polygon poly = new Polygon();
			NodeList pList = el.getElementsByTagName("point");
			for (int j = 0; j < pList.getLength(); j++) {
				Element point = (Element) pList.item(j);
				if (point.hasAttribute("x") && point.hasAttribute("y")) {
					poly.addPoint(Integer.parseInt(point.getAttribute("x")),
							Integer.parseInt(point.getAttribute("y")));
				}
			}
			Rect bBox = poly.getBoundingBox();
			if (Math.min(bBox.getHeight(), bBox.getWidth()) > 1) {
				org.opencv.core.Rect cvRect = new org.opencv.core.Rect(bBox.left,
					bBox.top, bBox.getWidth(), bBox.getHeight());
				Individual child = indy.produceChild(cvRect, false);
				Mat childContent = Mat.zeros(cvRect.size(), content.type());
				Mat mask = Mat.ones(cvRect.size(), CvType.CV_8UC1);
				for (int y = bBox.top; y <= bBox.bottom; y++) {
					for (int x = bBox.left; x <= bBox.right; x++) {
						if (!poly.isPointInside(x, y)) {
							mask.put(y - bBox.top, x - bBox.left, new byte[]{0});
						}
					}
				}
				content.submat(cvRect).copyTo(childContent, mask);
				child.setContent(childContent);
				children.add(child);
				mask.release();
				String zoneId = el.getAttribute("id");
				if (zoneId.startsWith(hadaraId)) {
					zoneId = zoneId.substring(hadaraId.length());
				}
				child.setIdxPrefix(conf.getInteger("useZoneIds") > 0 
					? zoneId : "_" + Operation.generateId(children.size()));
				String trans = transcriptions.get(el.getAttribute("id"));
				Logger.getSingleton().debug("Transcription for "
					+ child.getTargetFileName(outputPath) + ": " + 
					(trans == null ? "" : trans));
			}
		}
	}
	
	/* (non-Javadoc)
	 * Load all transcriptions in a content node associated with their ref_id.
	 */
	private Map<String, String> loadTranscriptions(Node contentNode) {
		HashMap<String, String> transcriptions = new HashMap<String, String>();
		if (contentNode == null) {
			return transcriptions;
		}
		NodeList sList = ((Element) contentNode).getElementsByTagName("segment");
		for (int i = 0; i < sList.getLength(); i++) {
			Element el = (Element) sList.item(i);
			if (!el.hasAttribute("ref_id")) {
				continue;
			}
			NodeList tList = el.getElementsByTagName("transcription");
			String trans = "";
			for (int j = 0; j < tList.getLength(); j++) {
				Element transElement = (Element) tList.item(j);
				trans += " " + transElement.getTextContent().trim();
			}
			transcriptions.put(el.getAttribute("ref_id"), trans.trim());
		}
		return transcriptions;
	}
}
