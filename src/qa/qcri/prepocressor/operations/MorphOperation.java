package qa.qcri.prepocressor.operations;

import java.util.LinkedList;
import java.util.List;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the morph operation.
 * 
 * @author Felix Stahlberg
 */
public class MorphOperation extends Operation {

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public MorphOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("kernelSize", 5,
					"Kernel size");
				addParameter("kernelShape", "rect",
						"Kernel shape. 'rect' or 'ellipse'");
				addParameter("operation", "close",
					"One of the following morphology operations: "
					+ "close, open, erode, dilate");
			}
			@Override
			protected String getDescription() {
				return "Performs morphological operations.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		Mat content = indy.getContent();
		int kSize = conf.getInteger("kernelSize");
		Mat kernel = Imgproc.getStructuringElement(
			conf.getString("kernelShape").equals("ellipse") 
				? Imgproc.MORPH_ELLIPSE : Imgproc.MORPH_RECT,
			new Size(kSize, kSize));
		int  type = Imgproc.MORPH_CLOSE;
		String op = conf.getString("operation");
		if (op.equals("open")) {
			type = Imgproc.MORPH_OPEN;
		} else if (op.equals("erode")) {
			type = Imgproc.MORPH_ERODE;
		} else if (op.equals("dilate")) {
			type = Imgproc.MORPH_DILATE;
		}
		Mat dst = Mat.zeros(content.rows(), content.cols(), content.type());
		Imgproc.morphologyEx(content, dst, type, kernel);
		Individual child = new Individual();
		child.setContent(dst);
		List<Individual> children = new LinkedList<Individual>();
		children.add(child);
		content.release();
		kernel.release();
		return children;
	}
}
