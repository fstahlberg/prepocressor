package qa.qcri.prepocressor.operations;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.opencv.core.Rect;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import qa.qcri.prepocressor.datastructures.Individual;
import qa.qcri.prepocressor.io.Logger;
import qa.qcri.prepocressor.ui.Configuration;

/*
This file is part of the prepocressor toolkit.

Copyright (c) 2015, QCRI a member of Qatar Foundation. All Rights Reserved
Felix Stahlberg

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/**
 * This class implements the cutWithAltecXml operation.
 * 
 * @author Felix Stahlberg
 */
public class CutWithAltecXmlOperation extends Operation {
	
	private boolean wordLevelSplitting = false;

	/* (non-Javadoc)
	 * @see Operation#Operation(String)
	 */
	public CutWithAltecXmlOperation(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see Operation#createConfiguration()
	 */
	@Override
	protected Configuration createConfiguration() {
		return new Configuration() {
			@Override
			protected void addDefaultParameters() {
				addParameter("xmlPath", "%base%idx.xml",
					"Path to the xml files in ALTEC format. The same place"
					+ "holders as in the global outputPath can be used.");
				addParameter("cutLevel", "line",
					"Splitting level. Either 'line' or 'word'");
				addParameter("useIndexAttributes", 0,
					"Set the prepocressor index to the value of the index "
					+ "attribute of the corresponding node in the xml file."
					+ "Note that this can lead to problems in combination "
					+ "with cutLevel=word because the same word level index "
					+ "might be used multiple times in a single xml file.");
			}
			@Override
			protected String getDescription() {
				return "This operation corresponds to cutWithPageXml but reads"
					+ " xml files in the format used by the ALTEC corpus. This "
					+ "format specifies line tags on the first level and "
					+ "word tags on the second level. See http://ALTEC-Center."
					+ "org/xsd/ocr-annotation-1-0.xsd for a specification.";
			}
		};
	}

	@Override
	protected List<Individual> processIndividual(Individual indy) {
		String fileName = indy.getPopulation().getFullIndividualFileName(
				conf.getString("xmlPath"),
				indy.getIdxPrefix());
		wordLevelSplitting = conf.getString("cutLevel").equals("word");
		Logger log = Logger.getSingleton();
		List<Individual> children = new LinkedList<Individual>();
		try {
			File fXmlFile = new File(fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName(wordLevelSplitting
				? "word" : "line");
			for (int i = 0; i < nList.getLength(); i++) {
				Node node = nList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					cutNode(children, indy, (Element) node);
				}
			}
			indy.getContent().release();
		} catch (ParserConfigurationException e) {
			log.error("Parser configuration exception for " + fileName
					+ ": " + e.getMessage());
			children.add(indy);
		} catch (SAXException e) {
			log.error("SAX exception for " + fileName
					+ ": " + e.getMessage());
			children.add(indy);
		} catch (IOException e) {
			log.error("File reading exception for " + fileName
					+ ": " + e.getMessage());
			children.add(indy);
		}
		return children;
	}
	
	/* (non-Javadoc)
	 * Add the subimage specified by the node to children
	 */
	private void cutNode(List<Individual> children, 
			Individual indy, Element el) {
		if (!el.hasAttribute("x") || !el.hasAttribute("y") ||
				!el.hasAttribute("width") || !el.hasAttribute("height")) {
			Logger.getSingleton().warn("Extraction level tag in xml lacks of "
				+ "one of the following attributes: x, y, width, height");
			return;
		}
		String outputPath = Configuration.getGlobalConfiguration()
				.getString("outputPath");
		try {
			Individual childIndy = indy.produceChild(new Rect(
				Integer.parseInt(el.getAttribute("x")), 
				Integer.parseInt(el.getAttribute("y")), 
				Integer.parseInt(el.getAttribute("width")),
				Integer.parseInt(el.getAttribute("height"))));
			childIndy.setIdxPrefix(conf.getInteger("useIndexAttributes") > 0 
				? el.getAttribute("index") 
				: "_" + Operation.generateId(children.size()));
			outputTranscription(el, childIndy.getTargetFileName(outputPath));
			children.add(childIndy);
		} catch (NumberFormatException e) {
			Logger.getSingleton().warn("x, y, width, or height attribute "
				+ "did not contain integer value");
		}
	}
	
	/* (non-Javadoc)
	 * Output transcription for elementl
	 */
	private void outputTranscription(Element el, String desc) {
		Logger log = Logger.getSingleton();
		if (el.hasAttribute("transcription")) {
			log.debug("Transcription for "
				+ desc + ": " + el.getAttribute("transcription"));
			return;
		}
		TreeMap<Integer, String> words = new TreeMap<Integer, String>();
		NodeList nList = el.getElementsByTagName("word");
		for (int i = 0; i < nList.getLength(); i++) {
			Node childNode = nList.item(i);
			if (childNode.getNodeType() == Node.ELEMENT_NODE) {
				Element childEl = (Element) childNode;
				if (childEl.hasAttribute("index") 
					 && childEl.hasAttribute("transcription")) {
					try {
						words.put(Integer.parseInt(
							childEl.getAttribute("index")),
							childEl.getAttribute("transcription"));
					} catch (NumberFormatException e) {
						;
					}
				}
			}
		}
		String trans = "";
		for (String word : words.values()) {
			trans += " " + word;
		}
		log.debug("Transcription for " + desc + ": " 
			+ (trans.isEmpty() ? "" : trans.substring(1)));
	}
}
